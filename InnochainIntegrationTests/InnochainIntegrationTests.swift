import XCTest
@testable import InnochainDemo
import IrohaCrypto
import RobinHood
import FearlessUtils

class InnochainIntegrationTests: XCTestCase {

    func testCreateUserTx() {
        (0..<10).forEach { _ in
            performAccountCreation()
        }
    }

    func testTxRetrieving() {
        do {
            let txHash = "0x48122cc7d7434003c1a38dcbe86bafc8f55f6cd0adbe5fad427647abc6e2a9cb"
            let operationFactory = NodeOperationFactory(url: PrivateUrl.main)
            let call = GetTransactionCall(hash: txHash)

            let operation: BaseOperation<GetTransactionResponse> = operationFactory.callMethodOperation(
                RPCMethod.getTransaction,
                params: call
            )

            OperationQueue().addOperations([operation], waitUntilFinished: true)

            let response = try operation.extractNoCancellableResultData()

            guard let transaction = response.transaction else {
                XCTFail("Missing transaction")
                return
            }

            let scaleEncoder = ScaleEncoder()
            try transaction.payload.encode(scaleEncoder: scaleEncoder)
            let actualHash = try (scaleEncoder.encode() as NSData).stribog().toHex(includePrefix: true)

            XCTAssertEqual(txHash, actualHash)
            Logger.shared.info("Tx sender: \(transaction.payload.sender.value.toHex(includePrefix: true))")
        } catch {
            XCTFail("Did receive error: \(error)")
        }
    }

    func testAccountRetrieving() {
        do {
            let accountId = AccountId(value: try Data(hexString: "0x72195988afd628520eaaf0535c89ce31"))
            let operationFactory = NodeOperationFactory(url: PrivateUrl.main)
            let call = GetAccountCall(accountId: accountId)

            let operation: BaseOperation<GetAccountResponse> = operationFactory.callMethodOperation(
                RPCMethod.getAccount,
                params: call
            )

            OperationQueue().addOperations([operation], waitUntilFinished: true)

            let response = try operation.extractNoCancellableResultData()

            XCTAssertEqual(accountId, response.account.accountId)

            Logger.shared.info("Did receive response: \(response)")
        } catch {
            XCTFail("Did receive error: \(error)")
        }
    }

    func testContractRetrieving() {
        do {
            let accountId = AccountId(value: try Data(hexString: "0x5bf300184fef70d704ce1c252c89a262"))
            let operationFactory = NodeOperationFactory(url: PrivateUrl.main)
            let call = GetAccountCall(accountId: accountId)

            let operation: BaseOperation<GetAccountResponse> = operationFactory.callMethodOperation(
                RPCMethod.getAccount,
                params: call
            )

            OperationQueue().addOperations([operation], waitUntilFinished: true)

            let response = try operation.extractNoCancellableResultData()

            XCTAssertEqual(accountId, response.account.accountId)

            Logger.shared.info("Did receive response: \(response)")

            guard let state = response.account.storage.first(where: { $0.key == "state" }) else {
                XCTFail("Contract state not found")
                return
            }

            let decoder = try ScaleDecoder(data: state.value)
            let campaign: SCCampaign = try SCCampaign(scaleDecoder: decoder)
            Logger.shared.info("Campaign: \(campaign)")

        } catch {
            XCTFail("Did receive error: \(error)")
        }
    }

    func testCreateContract() {
        do {
            guard let customer = performAccountCreation() else {
                XCTFail("Customer creation failed")
                return
            }

            guard let supplier = performAccountCreation() else {
                XCTFail("Supplier creation failed")
                return
            }

            guard let bank = performAccountCreation() else {
                XCTFail("Bank account failed")
                return
            }

            let contractAccountId = try AccountId.createRandom()
            let contractInit = ContractInit(
                customerAccoundId: customer.0,
                customerName: "Customer",
                supplierAccoundId: supplier.0,
                supplierName: "Supplier",
                agreement: "Agreement #10",
                bankAccountId: bank.0
            )

            let transaction = try TransactionBuilder()
                .from(customer.0)
                .nonce(1)
                .createContract(contractAccountId, contractInit: contractInit)
                .sign(using: [GostSigner(privateKey: customer.1.privateKey())])

            let operationFactory = NodeOperationFactory(url: PrivateUrl.main)
            let call = try SubmitRawTransactionCall.fromSigned(transaction: transaction)
            let operation: BaseOperation<SubmitRawTransactionResponse> = operationFactory.callMethodOperation(
                RPCMethod.submitRawTransaction,
                params: call
            )

            OperationQueue().addOperations([operation], waitUntilFinished: true)

            let response = try operation.extractNoCancellableResultData()

            let contractAccountHex = contractAccountId.value.toHex(includePrefix: true)
            Logger.shared.info("Did receive contract \(contractAccountHex) tx hash: \(response.hash)")

        } catch {
            XCTFail("Did receive error: \(error)")
        }
    }

    // MARK: Private

    @discardableResult
    private func performAccountCreation() -> (AccountId, IRCryptoKeypairProtocol)? {
        do {
            let keypair = try GostKeyFactory().createRandomKeypair()
            let signer = GostSigner(privateKey: keypair.privateKey())

            let sender = try AccountId.createRandom()
            let publicKey = PublicKey(value: keypair.publicKey().rawData())

            let transaction = try TransactionBuilder()
                .createUser(with: sender, publicKeys: SortedSet(items: [publicKey]))
                .sign(using: [signer])

            let operationFactory = NodeOperationFactory(url: PrivateUrl.main)
            let call = try SubmitRawTransactionCall.fromSigned(transaction: transaction)
            let operation: BaseOperation<SubmitRawTransactionResponse> = operationFactory.callMethodOperation(
                RPCMethod.submitRawTransaction,
                params: call
            )

            OperationQueue().addOperations([operation], waitUntilFinished: true)

            let response = try operation.extractNoCancellableResultData()

            Logger.shared.info("Did receive tx hash: \(response.hash)")

            return (sender, keypair)

        } catch {
            XCTFail("Unexpected error: \(error)")
            return nil
        }
    }
}
