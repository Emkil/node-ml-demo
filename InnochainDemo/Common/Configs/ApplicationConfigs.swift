import Foundation
import os

protocol ApplicationConfigProtocol {
    var txPollingInterval: TimeInterval { get }
    var txPendinigInterval: TimeInterval { get }
}

final class ApplicationConfig {
    static let shared = ApplicationConfig()
}

extension ApplicationConfig: ApplicationConfigProtocol {
    var txPollingInterval: TimeInterval { 2.0 }
    var txPendinigInterval: TimeInterval { 6.0 }
}
