import Foundation
import RobinHood

final class NodeOperationFactory: JSONRPCOperationFactoryProtocol {
    let url: URL

    init(url: URL) {
        self.url = url
    }

    func callMethodOperation<P, T>(
        _ method: String,
        params: P?
    ) -> BaseOperation<T> where P: Encodable, T: Decodable {
        let identifier = UInt16.random(in: 0...UInt16.max)
        let info = JSONRPCInfo(identifier: identifier, jsonrpc: "2.0", method: method, params: params)

        let requestFactory = BlockNetworkRequestFactory {
            var request = URLRequest(url: self.url)
            request.httpMethod = HttpMethod.post.rawValue
            request.httpBody = try JSONEncoder().encode(info)
            request.addValue(
                HttpHeaderKey.contentType.rawValue,
                forHTTPHeaderField: HttpContentType.json.rawValue
            )

            return request
        }

        let responseFactory = AnyNetworkResultFactory<T> { (data: Data) in
            let resultData = try JSONDecoder().decode(JSONRPCData<T>.self, from: data)

            guard let result = resultData.result else {
                if let error = resultData.error {
                    throw error
                } else {
                    throw NetworkBaseError.unexpectedEmptyData
                }
            }

            return result
        }

        let networkOperation = NetworkOperation(requestFactory: requestFactory, resultFactory: responseFactory)

        return networkOperation
    }
}
