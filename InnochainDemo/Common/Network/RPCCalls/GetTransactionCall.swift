import Foundation

struct GetTransactionCall: Codable {
    let hash: String
}
