import Foundation
import FearlessUtils

struct GetTransactionResponse: Decodable {
    let transaction: SignedTransaction?
    let commitInfo: TxCommitInfo?

    init(from decoder: Decoder) throws {
        let json = try JSON(from: decoder)

        guard
            let txHex = json.msg?.SearchTxResponse?.tx?.stringValue else {
            self.transaction = nil
            self.commitInfo = nil
            return
        }

        let txData = try Data(hexString: txHex)
        let scaleDecoder = try ScaleDecoder(data: txData)
        transaction = try SignedTransaction(scaleDecoder: scaleDecoder)

        if let commitJson = json.msg?.SearchTxResponse?.commit_info, !commitJson.isNull {
            guard
                let version = commitJson.version?.unsignedIntValue,
                let ledgerIndex = commitJson.ledger_index?.unsignedIntValue else {
                throw JSONError.unsupported
            }

            commitInfo = TxCommitInfo(version: version, ledgerIndex: ledgerIndex)
        } else {
            commitInfo = nil
        }
    }
}
