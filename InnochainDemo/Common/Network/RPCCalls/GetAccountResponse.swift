import Foundation
import FearlessUtils

struct GetAccountResponse: Decodable {
    let account: Account

    init(from decoder: Decoder) throws {
        let json = try JSON(from: decoder)

        guard
            let accountJson = json.msg?.AccountResponse?.account else {
            throw JSONError.unsupported
        }

        account = try accountJson.map(to: Account.self)
    }
}
