import Foundation
import FearlessUtils

struct SubmitRawTransactionCall: Encodable {
    enum CodingKeys: String, CodingKey {
        case hexTransaction = "tx"
    }

    let hexTransaction: String
}

extension SubmitRawTransactionCall {
    static func fromSigned(transaction: SignedTransaction) throws -> SubmitRawTransactionCall {
        let encoder = ScaleEncoder()
        try transaction.encode(scaleEncoder: encoder)
        let hex = encoder.encode().toHex(includePrefix: true)
        return SubmitRawTransactionCall(hexTransaction: hex)
    }
}
