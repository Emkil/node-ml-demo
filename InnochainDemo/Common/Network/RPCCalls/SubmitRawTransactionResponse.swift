import Foundation
import FearlessUtils

struct SubmitRawTransactionResponse: Decodable {
    let hash: String

    init(from decoder: Decoder) throws {
        let json = try JSON(from: decoder)

        guard let value = json.msg?.NewTransactionResponse?.hash?.stringValue else {
            throw JSONError.unsupported
        }

        hash = value
    }
}
