import Foundation

struct GetAccountCall: Encodable {
    enum CodingKeys: String, CodingKey {
        case accountId = "account_id"
    }

    let accountId: AccountId

    func encode(to encoder: Encoder) throws {
        var container = encoder.container(keyedBy: CodingKeys.self)
        try container.encode(accountId.value.toHex(includePrefix: true), forKey: .accountId)
    }
}
