import Foundation

enum RPCMethod {
    static let submitRawTransaction = "submitRawTransaction"
    static let getTransaction = "getTransaction"
    static let getAccount = "getAccount"
}
