import Foundation
import RobinHood

enum JSONRPCEngineError: Error {
    case emptyResult
    case remoteCancelled
    case clientCancelled
    case unknownError
}

protocol JSONRPCOperationFactoryProtocol: AnyObject {
    func callMethodOperation<P: Encodable, T: Decodable>(
        _ method: String,
        params: P?
    ) -> BaseOperation<T>
}
