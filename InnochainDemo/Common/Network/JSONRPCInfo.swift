import Foundation
import FearlessUtils

enum JSONRPCInfoError: Error {
    case unsupportedParams
}

struct JSONRPCInfo<P: Encodable>: Encodable {
    enum CodingKeys: String, CodingKey {
        case identifier = "id"
        case jsonrpc
        case method
        case params
    }

    let identifier: UInt16
    let jsonrpc: String
    let method: String
    let params: P

    func encode(to encoder: Encoder) throws {
        var container = encoder.container(keyedBy: CodingKeys.self)
        try container.encode(identifier, forKey: .identifier)
        try container.encode(jsonrpc, forKey: .jsonrpc)
        try container.encode(method, forKey: .method)

        guard var dict = try params.toScaleCompatibleJSON().dictValue else {
            throw JSONRPCInfoError.unsupportedParams
        }

        dict.merge(
            [CodingKeys.identifier.rawValue: JSON.unsignedIntValue(UInt64(identifier))]
        ) { (first, _) in first }

        try container.encode(dict, forKey: .params)
    }
}
