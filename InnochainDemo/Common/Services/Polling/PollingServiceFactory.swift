import Foundation
import SoraKeystore
import RobinHood
import SoraFoundation

struct PollingServiceFactory {

    private func createTransactionCompletionPoll() -> TransactionCompletionPoll {
        let logger = Logger.shared

        let userStoreFacade = UserDataStorageFacade.shared
        let repository: CoreDataRepository<DbTransaction, CDTransactionItem>
            = userStoreFacade.createRepository()

        let contextObservable = CoreDataContextObservable(service: userStoreFacade.databaseService,
                                                                      mapper: repository.dataMapper,
                                                                      predicate: { _ in true })

        let streamableProvider = StreamableProvider(
            source: AnyStreamableSource(EmptyStreamableSource()),
            repository: AnyDataProviderRepository(repository),
            observable: AnyDataProviderRepositoryObservable(contextObservable),
            operationManager: OperationQueueFacade.defaultManager
        )

        let operationFactory = NodeOperationFactory(url: PrivateUrl.main)

        contextObservable.start { error in
            if let error = error {
                logger.error("Can't start context observable: \(error)")
            }
        }

        return TransactionCompletionPoll(
            operationFactory: operationFactory,
            pendingTxProvider: streamableProvider,
            repository: AnyDataProviderRepository(repository),
            pendingFailureDelay: ApplicationConfig.shared.txPendinigInterval,
            operationQueue: OperationQueueFacade.defaultQueue,
            logger: Logger.shared
        )
    }
}

extension PollingServiceFactory: UserApplicationServiceFactoryProtocol {
    func createServices() -> [UserApplicationServiceProtocol] {
        let transactionCompletionPoll = createTransactionCompletionPoll()

        let pollingInterval = ApplicationConfig.shared.txPollingInterval

        let pollingService = PollingService(
            pollables: [transactionCompletionPoll],
            pollingInterval: pollingInterval,
            applicationHandler: ApplicationHandler(),
            logger: Logger.shared
        )

        return [pollingService]
    }
}
