import Foundation
import RobinHood
import CoreData

final class TransactionCompletionPoll: Pollable {
    let operationFactory: JSONRPCOperationFactoryProtocol
    let pendingTxProvider: StreamableProvider<DbTransaction>
    let repository: AnyDataProviderRepository<DbTransaction>
    let operationQueue: OperationQueue
    let pendingFailureDelay: TimeInterval
    let logger: LoggerProtocol

    var state: PollableState = .initial {
        didSet {
            delegate?.pollableDidChangeState(self, from: oldValue)
        }
    }

    private var transactionIds: Set<Data> = []

    private var pollOperations: [Operation]?

    weak var delegate: PollableDelegate?

    init(operationFactory: JSONRPCOperationFactoryProtocol,
         pendingTxProvider: StreamableProvider<DbTransaction>,
         repository: AnyDataProviderRepository<DbTransaction>,
         pendingFailureDelay: TimeInterval,
         operationQueue: OperationQueue,
         logger: LoggerProtocol
    ) {
        self.operationFactory = operationFactory
        self.pendingTxProvider = pendingTxProvider
        self.repository = repository
        self.pendingFailureDelay = pendingFailureDelay
        self.operationQueue = operationQueue
        self.logger = logger
    }

    func setup() {
        guard state == .initial else {
            return
        }

        state = .setup

        transactionIds.removeAll()

        pendingTxProvider.removeObserver(self)

        let changesClosure: ([DataProviderChange<DbTransaction>]) -> Void = { [weak self] changes in
            self?.handle(changes: changes)
        }

        let failureChanges: (Error) -> Void = { [weak self] error in
            self?.handle(error: error)
        }

        let options = StreamableProviderObserverOptions(
            alwaysNotifyOnRefresh: false,
            waitsInProgressSyncOnAdd: false,
            initialSize: 0,
            refreshWhenEmpty: true
        )

        pendingTxProvider.addObserver(
            self,
            deliverOn: .main,
            executing: changesClosure,
            failing: failureChanges,
            options: options
        )
    }

    func poll() {
        guard state == .ready, pollOperations == nil else {
            return
        }

        logger.debug("Did start polling: \(transactionIds.map { $0.toHex(includePrefix: true) })")

        let wrappers = transactionIds.map { createPollForTxHash($0) }

        let combiningOperation = ClosureOperation {}

        var allOperations = [Operation]()
        for wrapper in wrappers {
            wrapper.allOperations.forEach { combiningOperation.addDependency($0) }
            allOperations.append(contentsOf: wrapper.allOperations)
        }

        allOperations.append(combiningOperation)

        combiningOperation.completionBlock = {
            DispatchQueue.main.async {
                self.pollOperations = nil
                self.logger.debug("Did complete polling")
            }
        }

        self.pollOperations = allOperations

        operationQueue.addOperations(allOperations, waitUntilFinished: false)
    }

    func cancel() {
        clearStateAndCancelOperations()
    }

    // MARK: Override

    func updateTransactionIdsWithChanges(_ changes: [DataProviderChange<DbTransaction>]) throws {
        let newTransactionIds = try changes.reduce(transactionIds) { (currentIds, change) in
            switch change {
            case let .insert(item), let .update(item):
                let txHash = try Data(hexString: item.identifier)
                if item.status == .pending {
                    return currentIds.union([txHash])
                } else {
                    return currentIds.subtracting([txHash])
                }
            case let .delete(deletedIdentifier):
                let txHash = try Data(hexString: deletedIdentifier)
                return currentIds.subtracting([txHash])
            }
        }

        self.transactionIds = newTransactionIds
    }

    func createPollForTxHash(_ txHash: Data) -> CompoundOperationWrapper<Void> {
        let hexHash = txHash.toHex(includePrefix: true)
        let getTransaction = GetTransactionCall(hash: hexHash)
        let remoteTransactionOperation: BaseOperation<GetTransactionResponse?> = operationFactory.callMethodOperation(
            RPCMethod.getTransaction,
            params: getTransaction
        )

        let localOperation = repository.fetchOperation(by: hexHash, options: RepositoryFetchOptions())

        let saveClosure: () throws -> [DbTransaction] = {
            guard
                let localTransaction = try localOperation.extractNoCancellableResultData(),
                localTransaction.status == .pending else {
                return []
            }

            let optionalRemoteTransaction = try remoteTransactionOperation.extractNoCancellableResultData()

            if let remoteTransaction = optionalRemoteTransaction {
                if remoteTransaction.commitInfo != nil {
                    let changed = localTransaction.changingStatus(to: .completed)
                    return [changed]
                } else if
                    remoteTransaction.transaction == nil,
                    Date().timeIntervalSince(localTransaction.createdAt) >= self.pendingFailureDelay {
                    let changed = localTransaction.changingStatus(to: .failed)
                    return [changed]
                }
            }

            return []
        }

        let saveOperation = repository.saveOperation(saveClosure, { [] })

        saveOperation.addDependency(remoteTransactionOperation)
        saveOperation.addDependency(localOperation)

        return CompoundOperationWrapper(
            targetOperation: saveOperation,
            dependencies: [remoteTransactionOperation, localOperation]
        )
    }

    // MARK: Private

    private func handle(changes: [DataProviderChange<DbTransaction>]) {
        do {
            try updateTransactionIdsWithChanges(changes)

            switch state {
            case .setup:
                if !transactionIds.isEmpty {
                    state = .ready
                }
            case .ready:
                if transactionIds.isEmpty {
                    state = .setup
                }
            default:
                break
            }
        } catch {
            handle(error: error)
        }
    }

    private func handle(error: Error) {
        logger.error("Did receive error: \(error)")
        clearStateAndCancelOperations()
    }

    private func clearStateAndCancelOperations() {
        state = .initial

        pollOperations?.forEach { $0.cancel() }
        pollOperations = nil

        pendingTxProvider.removeObserver(self)

        transactionIds.removeAll()
    }
}
