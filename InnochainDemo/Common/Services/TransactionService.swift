import Foundation
import RobinHood
import FearlessUtils

protocol TransactionServiceProtocol {
    func submit(
        transaction: SignedTransaction,
        runningIn queue: DispatchQueue,
        completionBlock: @escaping (Result<String, Error>) -> Void
    )
}

final class TransactionService {
    let operationFactory: JSONRPCOperationFactoryProtocol
    let repository: AnyDataProviderRepository<DbTransaction>
    let operationQueue: OperationQueue

    init(
        operationFactory: JSONRPCOperationFactoryProtocol,
        repository: AnyDataProviderRepository<DbTransaction>,
        operationQueue: OperationQueue
    ) {
        self.operationFactory = operationFactory
        self.operationQueue = operationQueue
        self.repository = repository
    }
}

extension TransactionService: TransactionServiceProtocol {
    func submit(
        transaction: SignedTransaction,
        runningIn queue: DispatchQueue,
        completionBlock: @escaping (Result<String, Error>) -> Void
    ) {
        do {
            let encoder = ScaleEncoder()
            try transaction.encode(scaleEncoder: encoder)
            let txData = encoder.encode()
            let hexTransaction = txData.toHex(includePrefix: true)

            let call = SubmitRawTransactionCall(hexTransaction: hexTransaction)
            let submitOperation: BaseOperation<SubmitRawTransactionResponse> = operationFactory.callMethodOperation(
                RPCMethod.submitRawTransaction,
                params: call
            )

            let saveOperation = repository.saveOperation({
                let result = try submitOperation.extractNoCancellableResultData()

                let dbTransaction = DbTransaction(
                    identifier: result.hash,
                    sender: transaction.payload.sender.value.toHex(includePrefix: true),
                    data: txData,
                    createdAt: Date(),
                    status: .pending
                )

                return [dbTransaction]
            }, { [] })

            saveOperation.addDependency(submitOperation)

            saveOperation.completionBlock = {
                do {
                    let response = try submitOperation.extractNoCancellableResultData()
                    completionBlock(.success(response.hash))
                } catch {
                    completionBlock(.failure(error))
                }
            }

            operationQueue.addOperations([submitOperation, saveOperation], waitUntilFinished: false)

        } catch {
            queue.async {
                completionBlock(.failure(error))
            }
        }
    }
}
