import Foundation
import RobinHood
import FearlessUtils

protocol LocalContractFetching: AnyObject {
    func fetchLocalContract(
        for accountId: AccountId,
        repository: AnyDataProviderRepository<DbContract>,
        operationQueue: OperationQueue,
        completionClosure: @escaping (Result<DbContract?, Error>) -> Void
    )
}

extension LocalContractFetching {
    func fetchLocalContract(
        for accountId: AccountId,
        repository: AnyDataProviderRepository<DbContract>,
        operationQueue: OperationQueue,
        completionClosure: @escaping (Result<DbContract?, Error>) -> Void
    ) {
        let operation = repository.fetchOperation(
            by: accountId.value.toHex(includePrefix: true),
            options: RepositoryFetchOptions()
        )

        operation.completionBlock = {
            DispatchQueue.main.async {
                do {
                    let account = try operation.extractNoCancellableResultData()
                    completionClosure(.success(account))
                } catch {
                    completionClosure(.failure(error))
                }
            }
        }

        operationQueue.addOperation(operation)
    }
}
