import Foundation
import RobinHood
import FearlessUtils

protocol RemoteContractFetching: AnyObject {
    func fetchRemoteContract(
        for accountId: AccountId,
        operationFactory: JSONRPCOperationFactoryProtocol,
        operationQueue: OperationQueue,
        completionClosure: @escaping (Result<SCCampaign, Error>) -> Void
    )
}

extension RemoteContractFetching {
    func fetchRemoteContract(
        for accountId: AccountId,
        operationFactory: JSONRPCOperationFactoryProtocol,
        operationQueue: OperationQueue,
        completionClosure: @escaping (Result<SCCampaign, Error>) -> Void
    ) {
        let call = GetAccountCall(accountId: accountId)

        let accountOperation: BaseOperation<GetAccountResponse> = operationFactory.callMethodOperation(
            RPCMethod.getAccount,
            params: call
        )

        let decodingOperation: BaseOperation<SCCampaign> = ClosureOperation {
            let response = try accountOperation.extractNoCancellableResultData()

            guard let state = response.account.storage.first(where: { $0.key == "state" }) else {
                throw ScaleCodingError.unexpectedDecodedValue
            }

            let decoder = try ScaleDecoder(data: state.value)
            return try SCCampaign(scaleDecoder: decoder)
        }

        decodingOperation.addDependency(accountOperation)

        decodingOperation.completionBlock = {
            DispatchQueue.main.async {
                do {
                    let contract = try decodingOperation.extractNoCancellableResultData()
                    completionClosure(.success(contract))
                } catch {
                    completionClosure(.failure(error))
                }
            }
        }

        operationQueue.addOperations([accountOperation, decodingOperation], waitUntilFinished: false)
    }
}
