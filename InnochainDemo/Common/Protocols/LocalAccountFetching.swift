import Foundation
import RobinHood

protocol LocalAccountFetching: AnyObject {
    func fetchLocalAccount(
        for accountId: AccountId,
        repository: AnyDataProviderRepository<DbAccount>,
        operationQueue: OperationQueue,
        completionClosure: @escaping (Result<DbAccount?, Error>) -> Void
    )
}

extension LocalAccountFetching {
    func fetchLocalAccount(
        for accountId: AccountId,
        repository: AnyDataProviderRepository<DbAccount>,
        operationQueue: OperationQueue,
        completionClosure: @escaping (Result<DbAccount?, Error>) -> Void
    ) {
        let operation = repository.fetchOperation(
            by: accountId.value.toHex(includePrefix: true),
            options: RepositoryFetchOptions()
        )

        operation.completionBlock = {
            DispatchQueue.main.async {
                do {
                    let account = try operation.extractNoCancellableResultData()
                    completionClosure(.success(account))
                } catch {
                    completionClosure(.failure(error))
                }
            }
        }

        operationQueue.addOperation(operation)
    }
}
