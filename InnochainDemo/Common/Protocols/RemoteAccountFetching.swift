import Foundation
import RobinHood

protocol RemoteAccountFetching: AnyObject {
    func fetchRemoteAccount(
        for accountId: AccountId,
        operationFactory: JSONRPCOperationFactoryProtocol,
        operationQueue: OperationQueue,
        completionClosure: @escaping (Result<Account, Error>) -> Void
    )
}

extension RemoteAccountFetching {
    func fetchRemoteAccount(
        for accountId: AccountId,
        operationFactory: JSONRPCOperationFactoryProtocol,
        operationQueue: OperationQueue,
        completionClosure: @escaping (Result<Account, Error>) -> Void
    ) {
        let call = GetAccountCall(accountId: accountId)

        let accountOperation: BaseOperation<GetAccountResponse> = operationFactory.callMethodOperation(
            RPCMethod.getAccount,
            params: call
        )

        accountOperation.completionBlock = {
            DispatchQueue.main.async {
                do {
                    let response = try accountOperation.extractNoCancellableResultData()
                    completionClosure(.success(response.account))
                } catch {
                    completionClosure(.failure(error))
                }
            }
        }

        operationQueue.addOperation(accountOperation)
    }
}
