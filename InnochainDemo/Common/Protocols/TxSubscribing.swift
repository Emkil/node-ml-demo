import Foundation
import RobinHood

protocol TxSubscribing: AnyObject {
    func handleTransactions(result: Result<[DbTransaction], Error>)
}

extension TxSubscribing {
    func subscribeToTransactions(using provider: StreamableProvider<DbTransaction>) {
        let changeClosure: ([DataProviderChange<DbTransaction>]) -> Void = { [weak self] changes in
            let transactions: [DbTransaction] = changes.compactMap {
                switch $0 {
                case .insert(let newItem), .update(let newItem):
                    return newItem
                case .delete:
                    return nil
                }
            }

            self?.handleTransactions(result: .success(transactions))
        }

        let failureClosure: (Error) -> Void = { [weak self] error in
            self?.handleTransactions(result: .failure(error))
        }

        provider.addObserver(
            self,
            deliverOn: .main,
            executing: changeClosure,
            failing: failureClosure,
            options: StreamableProviderObserverOptions()
        )
    }
}
