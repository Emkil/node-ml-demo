import UIKit

protocol AlertInputPresenting {
    func presentInputWithTitle(
        _ title: String,
        placeholder: String,
        from view: ControllerBackedProtocol,
        completionClosure: @escaping (String?) -> Void
    )
}

extension AlertInputPresenting {
    func presentInputWithTitle(
        _ title: String,
        placeholder: String,
        from view: ControllerBackedProtocol,
        completionClosure: @escaping (String?) -> Void
    ) {
        let alertView = UIAlertController(
            title: title,
            message: nil,
            preferredStyle: .alert
        )

        alertView.addTextField { textField in
            textField.placeholder = placeholder
        }

        let cancelAction = UIAlertAction(
            title: R.string.localizable.commonCancel(),
            style: .cancel
        ) { _ in
            completionClosure(nil)
        }

        let doneAction = UIAlertAction(
            title: R.string.localizable.commonDone(),
            style: .default
        ) { [weak alertView] _ in
            completionClosure(alertView?.textFields?.first?.text)
        }

        alertView.addAction(cancelAction)
        alertView.addAction(doneAction)

        view.controller.present(alertView, animated: true, completion: nil)
    }
}
