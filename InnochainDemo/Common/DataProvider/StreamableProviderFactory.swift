import Foundation
import RobinHood

final class StreamableProviderFactory {
    let storageFacade: StorageFacadeProtocol
    let logger: LoggerProtocol

    init(storageFacade: StorageFacadeProtocol, logger: LoggerProtocol) {
        self.storageFacade = storageFacade
        self.logger = logger
    }

    func createAllTransactionProvider() -> StreamableProvider<DbTransaction> {
        let repository: CoreDataRepository<DbTransaction, CDTransactionItem> = storageFacade.createRepository()

        let contextObservable = CoreDataContextObservable(
            service: storageFacade.databaseService,
            mapper: repository.dataMapper,
            predicate: { _ in true }
        )

        let streamableProvider = StreamableProvider(
            source: AnyStreamableSource(EmptyStreamableSource()),
            repository: AnyDataProviderRepository(repository),
            observable: AnyDataProviderRepositoryObservable(contextObservable),
            operationManager: OperationQueueFacade.defaultManager
        )

        contextObservable.start { [weak self] error in
            if let error = error {
                self?.logger.error("Can't start context observable: \(error)")
            }
        }

        return streamableProvider
    }
}
