import Foundation
import FearlessUtils

enum SCNegotiation: UInt8 {
    case notSet
    case waitingCustomer
    case waitingSupplier
    case negotiationRejected
    case negotiationApproved
}

extension SCNegotiation: ScaleCodable {
    init(scaleDecoder: ScaleDecoding) throws {
        let rawValue = try UInt8(scaleDecoder: scaleDecoder)

        guard let enumValue = SCNegotiation(rawValue: rawValue) else {
            throw ScaleCodingError.unexpectedDecodedValue
        }

        self = enumValue
    }

    func encode(scaleEncoder: ScaleEncoding) throws {
        try rawValue.encode(scaleEncoder: scaleEncoder)
    }
}
