import Foundation
import FearlessUtils
import BigInt

struct SCTask {
    let taskId: UInt32
    let negotiation: SCNegotiation
    let captain: SCPerson
    let worker: SCPerson
    let expectedGas: BigUInt
    let requestedGas: BigUInt
    let suppliedGas: BigUInt
    let totalGas: BigUInt
    let requestedTime: BigUInt
    let suppliedTime: BigUInt
    let completionTime: BigUInt
    let paymentTime: BigUInt
    let status: SCTaskStatus
    let paymentType: SCPaymentType
}

extension SCTask: ScaleCodable {
    init(scaleDecoder: ScaleDecoding) throws {
        taskId = try UInt32(scaleDecoder: scaleDecoder)
        negotiation = try SCNegotiation(scaleDecoder: scaleDecoder)
        captain = try SCPerson(scaleDecoder: scaleDecoder)
        worker = try SCPerson(scaleDecoder: scaleDecoder)
        expectedGas = try BigUInt(scaleDecoder: scaleDecoder)
        requestedGas = try BigUInt(scaleDecoder: scaleDecoder)
        suppliedGas = try BigUInt(scaleDecoder: scaleDecoder)
        totalGas = try BigUInt(scaleDecoder: scaleDecoder)
        requestedTime = try BigUInt(scaleDecoder: scaleDecoder)
        suppliedTime = try BigUInt(scaleDecoder: scaleDecoder)
        completionTime = try BigUInt(scaleDecoder: scaleDecoder)
        paymentTime = try BigUInt(scaleDecoder: scaleDecoder)
        status = try SCTaskStatus(scaleDecoder: scaleDecoder)
        paymentType = try SCPaymentType(scaleDecoder: scaleDecoder)
    }

    func encode(scaleEncoder: ScaleEncoding) throws {
        try taskId.encode(scaleEncoder: scaleEncoder)
        try negotiation.encode(scaleEncoder: scaleEncoder)
        try captain.encode(scaleEncoder: scaleEncoder)
        try worker.encode(scaleEncoder: scaleEncoder)
        try expectedGas.encode(scaleEncoder: scaleEncoder)
        try requestedGas.encode(scaleEncoder: scaleEncoder)
        try suppliedGas.encode(scaleEncoder: scaleEncoder)
        try totalGas.encode(scaleEncoder: scaleEncoder)
        try requestedTime.encode(scaleEncoder: scaleEncoder)
        try suppliedTime.encode(scaleEncoder: scaleEncoder)
        try completionTime.encode(scaleEncoder: scaleEncoder)
        try paymentTime.encode(scaleEncoder: scaleEncoder)
        try status.encode(scaleEncoder: scaleEncoder)
        try paymentType.encode(scaleEncoder: scaleEncoder)
    }
}
