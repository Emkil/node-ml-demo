import Foundation
import FearlessUtils

enum SCPhase: UInt8 {
    case phaseAgreement
    case phaseTasks
    case phaseDeclined
}

extension SCPhase: ScaleCodable {
    init(scaleDecoder: ScaleDecoding) throws {
        let rawValue = try UInt8(scaleDecoder: scaleDecoder)

        guard let enumValue = SCPhase(rawValue: rawValue) else {
            throw ScaleCodingError.unexpectedDecodedValue
        }

        self = enumValue
    }

    func encode(scaleEncoder: ScaleEncoding) throws {
        try rawValue.encode(scaleEncoder: scaleEncoder)
    }
}
