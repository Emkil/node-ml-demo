import Foundation
import FearlessUtils

enum SCTaskStatus: UInt8 {
    case taskNotAccepted
    case taskAccepted
    case taskReadyToPerform
    case gasRequested
    case performing
    case confirmed
    case taskCompleted
}

extension SCTaskStatus: ScaleCodable {
    init(scaleDecoder: ScaleDecoding) throws {
        let rawValue = try UInt8(scaleDecoder: scaleDecoder)

        guard let enumValue = SCTaskStatus(rawValue: rawValue) else {
            throw ScaleCodingError.unexpectedDecodedValue
        }

        self = enumValue
    }

    func encode(scaleEncoder: ScaleEncoding) throws {
        try rawValue.encode(scaleEncoder: scaleEncoder)
    }
}
