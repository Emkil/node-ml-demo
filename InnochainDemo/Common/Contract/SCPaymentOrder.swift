import Foundation
import FearlessUtils
import BigInt

struct SCPaymentOrder {
    let amount: BigUInt
    let paymentTime: BigUInt
    let paymentId: BigUInt
    let taskId: BigUInt
    let paymentStatus: SCPaymentStatus
    let direction: Bool
}

extension SCPaymentOrder: ScaleCodable {
    init(scaleDecoder: ScaleDecoding) throws {
        amount = try BigUInt(scaleDecoder: scaleDecoder)
        paymentTime = try BigUInt(scaleDecoder: scaleDecoder)
        paymentId = try BigUInt(scaleDecoder: scaleDecoder)
        taskId = try BigUInt(scaleDecoder: scaleDecoder)
        paymentStatus = try SCPaymentStatus(scaleDecoder: scaleDecoder)
        direction = try Bool(scaleDecoder: scaleDecoder)
    }

    func encode(scaleEncoder: ScaleEncoding) throws {
        try amount.encode(scaleEncoder: scaleEncoder)
        try paymentTime.encode(scaleEncoder: scaleEncoder)
        try paymentId.encode(scaleEncoder: scaleEncoder)
        try taskId.encode(scaleEncoder: scaleEncoder)
        try paymentStatus.encode(scaleEncoder: scaleEncoder)
        try direction.encode(scaleEncoder: scaleEncoder)
    }
}
