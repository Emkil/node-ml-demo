import Foundation
import BigInt
import FearlessUtils

struct SCPerson {
    let personId: BigUInt
    let name: String
}

extension SCPerson: ScaleCodable {
    init(scaleDecoder: ScaleDecoding) throws {
        personId = try BigUInt(scaleDecoder: scaleDecoder)
        name = try String(scaleDecoder: scaleDecoder)
    }

    func encode(scaleEncoder: ScaleEncoding) throws {
        try personId.encode(scaleEncoder: scaleEncoder)
        try name.encode(scaleEncoder: scaleEncoder)
    }
}
