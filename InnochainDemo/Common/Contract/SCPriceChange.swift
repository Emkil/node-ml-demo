import Foundation
import FearlessUtils
import BigInt

struct SCPriceChange {
    let price: BigUInt
    let negotiation: SCNegotiation
    let startTime: BigUInt
}

extension SCPriceChange: ScaleCodable {
    init(scaleDecoder: ScaleDecoding) throws {
        price = try BigUInt(scaleDecoder: scaleDecoder)
        negotiation = try SCNegotiation(scaleDecoder: scaleDecoder)
        startTime = try BigUInt(scaleDecoder: scaleDecoder)
    }

    func encode(scaleEncoder: ScaleEncoding) throws {
        try price.encode(scaleEncoder: scaleEncoder)
        try negotiation.encode(scaleEncoder: scaleEncoder)
        try startTime.encode(scaleEncoder: scaleEncoder)
    }
}
