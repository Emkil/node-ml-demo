import Foundation
import BigInt
import FearlessUtils

struct SCAgreementDetails {
    let text: String
    let bankId: BigUInt
}

extension SCAgreementDetails: ScaleCodable {
    init(scaleDecoder: ScaleDecoding) throws {
        text = try String(scaleDecoder: scaleDecoder)
        bankId = try BigUInt(scaleDecoder: scaleDecoder)
    }

    func encode(scaleEncoder: ScaleEncoding) throws {
        try text.encode(scaleEncoder: scaleEncoder)
        try bankId.encode(scaleEncoder: scaleEncoder)
    }
}
