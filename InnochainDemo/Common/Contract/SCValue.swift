import Foundation
import FearlessUtils
import BigInt

enum SCValue {
    case scString(_ value: String)
    case scInt(_ value: BigUInt)
    case scBool(_ value: Bool)
    case scAgreement(_ value: SCAgreement)
    case scTask(_ value: SCTask)
    case scNegotiation(_ value: SCNegotiation)
    case scPriceChange(_ value: SCPriceChange)
    case scPaymentOrder(_ value: SCPaymentOrder)
    case scCampaign(_ value: SCCampaign)
    case scPerson(_ value: SCPerson)
    case scPaymentStatus(_ value: SCPaymentStatus)
    case scTaskStatus(_ value: SCTaskStatus)
    case scAgreementDetails(_ value: SCAgreementDetails)
    case scPaymentType(_ value: SCPaymentType)
    case scPhase(_ value: SCPhase)
}

extension SCValue: ScaleCodable {
    init(scaleDecoder: ScaleDecoding) throws {
        let rawType = try UInt8(scaleDecoder: scaleDecoder)

        switch rawType {
        case 0:
            let value = try String(scaleDecoder: scaleDecoder)
            self = .scString(value)
        case 1:
            let value = try BigUInt(scaleDecoder: scaleDecoder)
            self = .scInt(value)
        case 2:
            let value = try Bool(scaleDecoder: scaleDecoder)
            self = .scBool(value)
        case 3:
            let value = try SCNegotiation(scaleDecoder: scaleDecoder)
            self = .scNegotiation(value)
        case 4:
            let value = try SCPaymentStatus(scaleDecoder: scaleDecoder)
            self = .scPaymentStatus(value)
        case 5:
            let value = try SCAgreementDetails(scaleDecoder: scaleDecoder)
            self = .scAgreementDetails(value)
        case 6:
            let value = try SCPerson(scaleDecoder: scaleDecoder)
            self = .scPerson(value)
        case 7:
            let value = try SCPaymentOrder(scaleDecoder: scaleDecoder)
            self = .scPaymentOrder(value)
        case 8:
            let value = try SCPriceChange(scaleDecoder: scaleDecoder)
            self = .scPriceChange(value)
        case 9:
            let value = try SCPhase(scaleDecoder: scaleDecoder)
            self = .scPhase(value)
        case 10:
            let value = try SCTaskStatus(scaleDecoder: scaleDecoder)
            self = .scTaskStatus(value)
        case 11:
            let value = try SCPaymentType(scaleDecoder: scaleDecoder)
            self = .scPaymentType(value)
        case 12:
            let value = try SCAgreement(scaleDecoder: scaleDecoder)
            self = .scAgreement(value)
        case 13:
            let value = try SCTask(scaleDecoder: scaleDecoder)
            self = .scTask(value)
        case 14:
            let value = try SCCampaign(scaleDecoder: scaleDecoder)
            self = .scCampaign(value)
        default:
            throw ScaleCodingError.unexpectedDecodedValue
        }
    }

    func encode(scaleEncoder: ScaleEncoding) throws {
        switch self {
        case let .scString(value):
            try UInt8(0).encode(scaleEncoder: scaleEncoder)
            try value.encode(scaleEncoder: scaleEncoder)
        case let .scInt(value):
            try UInt8(1).encode(scaleEncoder: scaleEncoder)
            try value.encode(scaleEncoder: scaleEncoder)
        case let .scBool(value):
            try UInt8(2).encode(scaleEncoder: scaleEncoder)
            try value.encode(scaleEncoder: scaleEncoder)
        case let .scAgreement(value):
            try UInt8(12).encode(scaleEncoder: scaleEncoder)
            try value.encode(scaleEncoder: scaleEncoder)
        case let .scTask(value):
            try UInt8(13).encode(scaleEncoder: scaleEncoder)
            try value.encode(scaleEncoder: scaleEncoder)
        case let .scNegotiation(value):
            try UInt8(3).encode(scaleEncoder: scaleEncoder)
            try value.encode(scaleEncoder: scaleEncoder)
        case let .scPriceChange(value):
            try UInt8(8).encode(scaleEncoder: scaleEncoder)
            try value.encode(scaleEncoder: scaleEncoder)
        case let .scPaymentOrder(value):
            try UInt8(7).encode(scaleEncoder: scaleEncoder)
            try value.encode(scaleEncoder: scaleEncoder)
        case let .scCampaign(value):
            try UInt8(14).encode(scaleEncoder: scaleEncoder)
            try value.encode(scaleEncoder: scaleEncoder)
        case let .scPerson(value):
            try UInt8(6).encode(scaleEncoder: scaleEncoder)
            try value.encode(scaleEncoder: scaleEncoder)
        case let .scPaymentStatus(value):
            try UInt8(4).encode(scaleEncoder: scaleEncoder)
            try value.encode(scaleEncoder: scaleEncoder)
        case let .scTaskStatus(value):
            try UInt8(10).encode(scaleEncoder: scaleEncoder)
            try value.encode(scaleEncoder: scaleEncoder)
        case let .scAgreementDetails(value):
            try UInt8(5).encode(scaleEncoder: scaleEncoder)
            try value.encode(scaleEncoder: scaleEncoder)
        case let .scPaymentType(value):
            try UInt8(11).encode(scaleEncoder: scaleEncoder)
            try value.encode(scaleEncoder: scaleEncoder)
        case let .scPhase(value):
            try UInt8(9).encode(scaleEncoder: scaleEncoder)
            try value.encode(scaleEncoder: scaleEncoder)
        }
    }
}
