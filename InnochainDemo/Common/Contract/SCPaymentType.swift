import Foundation
import FearlessUtils

enum SCPaymentType: UInt8 {
    case pre
    case post
    case delayed
}

extension SCPaymentType: ScaleCodable {
    init(scaleDecoder: ScaleDecoding) throws {
        let rawValue = try UInt8(scaleDecoder: scaleDecoder)

        guard let enumValue = SCPaymentType(rawValue: rawValue) else {
            throw ScaleCodingError.unexpectedDecodedValue
        }

        self = enumValue
    }

    func encode(scaleEncoder: ScaleEncoding) throws {
        try rawValue.encode(scaleEncoder: scaleEncoder)
    }
}
