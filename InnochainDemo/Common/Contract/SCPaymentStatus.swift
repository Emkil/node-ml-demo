import Foundation
import FearlessUtils

enum SCPaymentStatus: UInt8 {
    case waitingForPayment
    case paymentCompleted
    case paymentRejected
}

extension SCPaymentStatus {
    init(scaleDecoder: ScaleDecoding) throws {
        let rawValue = try UInt8(scaleDecoder: scaleDecoder)

        guard let enumValue = SCPaymentStatus(rawValue: rawValue) else {
            throw ScaleCodingError.unexpectedDecodedValue
        }

        self = enumValue
    }

    func encode(scaleEncoder: ScaleEncoding) throws {
        try rawValue.encode(scaleEncoder: scaleEncoder)
    }
}
