import Foundation
import FearlessUtils

struct SCAgreement {
    let negotiation: SCNegotiation
    let customer: SCPerson
    let supplier: SCPerson
    let details: SCAgreementDetails
}

extension SCAgreement: ScaleCodable {
    init(scaleDecoder: ScaleDecoding) throws {
        negotiation = try SCNegotiation(scaleDecoder: scaleDecoder)
        customer = try SCPerson(scaleDecoder: scaleDecoder)
        supplier = try SCPerson(scaleDecoder: scaleDecoder)
        details = try SCAgreementDetails(scaleDecoder: scaleDecoder)
    }

    func encode(scaleEncoder: ScaleEncoding) throws {
        try negotiation.encode(scaleEncoder: scaleEncoder)
        try customer.encode(scaleEncoder: scaleEncoder)
        try supplier.encode(scaleEncoder: scaleEncoder)
        try details.encode(scaleEncoder: scaleEncoder)
    }
}
