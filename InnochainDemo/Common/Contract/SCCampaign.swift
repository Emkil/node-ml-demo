import Foundation
import FearlessUtils

struct SCCampaign {
    let agreement: SCAgreement
    let tasks: [SCTask]
    let negotiation: SCNegotiation
    let priceChanges: [SCPriceChange]
    let phase: SCPhase
    let paymentOrders: [SCPaymentOrder]
}

extension SCCampaign: ScaleCodable {
    init(scaleDecoder: ScaleDecoding) throws {
        agreement = try SCAgreement(scaleDecoder: scaleDecoder)
        tasks = try [SCTask](scaleDecoder: scaleDecoder)
        negotiation = try SCNegotiation(scaleDecoder: scaleDecoder)
        priceChanges = try [SCPriceChange](scaleDecoder: scaleDecoder)
        phase = try SCPhase(scaleDecoder: scaleDecoder)
        paymentOrders = try [SCPaymentOrder](scaleDecoder: scaleDecoder)
    }

    func encode(scaleEncoder: ScaleEncoding) throws {
        try agreement.encode(scaleEncoder: scaleEncoder)
        try tasks.encode(scaleEncoder: scaleEncoder)
        try negotiation.encode(scaleEncoder: scaleEncoder)
        try priceChanges.encode(scaleEncoder: scaleEncoder)
        try phase.encode(scaleEncoder: scaleEncoder)
        try paymentOrders.encode(scaleEncoder: scaleEncoder)
    }
}
