import Foundation
import FearlessUtils

struct Signature: FixedLengthDataStoring, Comparable {
    static var length: Int { 64 }
    let value: Data

    static func < (lhs: Signature, rhs: Signature) -> Bool {
        lhs.value.lexicographicallyPrecedes(rhs.value)
    }
}
