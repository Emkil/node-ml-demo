import Foundation

struct DbAccount: Codable {
    enum CodingKeys: String, CodingKey {
        case identifier
        case name
        case createdAt
    }

    let identifier: String
    let name: String
    let createdAt: Date

    init(identifier: String, name: String, createdAt: Date) {
        self.identifier = identifier
        self.name = name
        self.createdAt = createdAt
    }
}
