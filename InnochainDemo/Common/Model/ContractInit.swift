import Foundation

struct ContractInit {
    let customerAccoundId: AccountId
    let customerName: String
    let supplierAccoundId: AccountId
    let supplierName: String
    let agreement: String
    let bankAccountId: AccountId
}
