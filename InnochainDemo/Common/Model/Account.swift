import Foundation
import FearlessUtils
import BigInt

struct AccountId: FixedLengthDataStoring, Equatable {
    static var length: Int { 16 }
    let value: Data
}

extension AccountId {
    static func createRandom() throws -> AccountId {
        let value = try Data.random(length: AccountId.length)
        return AccountId(value: value)
    }

    static func fromHex(string: String) throws -> AccountId {
        let value = try Data(hexString: string)
        return AccountId(value: value)
    }

    func toBigEndian() -> BigUInt {
        BigUInt(value)
    }
}

struct Account: ScaleCodable, Decodable {
    enum CodingKeys: String, CodingKey {
        case accountId = "id"
        case publicKeys = "public_keys"
        case type
        case nonce
        case isConfirmed = "is_confirmed"
        case code
        case storage
    }

    let accountId: AccountId
    let publicKeys: SortedSet<PublicKey>
    let type: UInt8
    let nonce: UInt32
    let isConfirmed: Bool
    let code: Data?
    let storage: AccountStorage

    init(from decoder: Decoder) throws {
        let container = try decoder.container(keyedBy: CodingKeys.self)

        let rawAccountId = try container.decode(String.self, forKey: .accountId)
        accountId = AccountId(value: try Data(hexString: rawAccountId))

        let rawPublicKeys: [PublicKey] = try container.decode([String].self, forKey: .publicKeys)
            .map { hexPubKey in
            let rawPubKey = try Data(hexString: hexPubKey)
            return PublicKey(value: rawPubKey)
        }

        self.publicKeys = SortedSet(items: rawPublicKeys)

        type = try container.decode(UInt8.self, forKey: .type)
        nonce = try container.decode(UInt32.self, forKey: .nonce)
        isConfirmed = try container.decode(Bool.self, forKey: .isConfirmed)
        code = try container.decodeIfPresent(Data.self, forKey: .code)
        storage = try container.decode(AccountStorage.self, forKey: .storage)
    }

    init(scaleDecoder: ScaleDecoding) throws {
        accountId = try AccountId(scaleDecoder: scaleDecoder)
        publicKeys = try SortedSet(scaleDecoder: scaleDecoder)
        type = try UInt8(scaleDecoder: scaleDecoder)
        nonce = try UInt32(scaleDecoder: scaleDecoder)
        isConfirmed = try Bool(scaleDecoder: scaleDecoder)

        switch try ScaleOption<Data>(scaleDecoder: scaleDecoder) {
        case .none:
            code = nil
        case let .some(value):
            code = value
        }

        storage = try AccountStorage.init(scaleDecoder: scaleDecoder)
    }

    func encode(scaleEncoder: ScaleEncoding) throws {
        try accountId.encode(scaleEncoder: scaleEncoder)
        try publicKeys.encode(scaleEncoder: scaleEncoder)
        try type.encode(scaleEncoder: scaleEncoder)
        try nonce.encode(scaleEncoder: scaleEncoder)
        try isConfirmed.encode(scaleEncoder: scaleEncoder)

        if let code = code {
            try ScaleOption.some(value: code).encode(scaleEncoder: scaleEncoder)
        } else {
            try ScaleOption<Data>.none.encode(scaleEncoder: scaleEncoder)
        }

        try storage.encode(scaleEncoder: scaleEncoder)
    }
}

typealias AccountStorage = [AccountStorageElement]

struct AccountStorageElement: ScaleCodable, Decodable {
    enum ColCodingKeys: String, CodingKey {
        case col1
        case col2
    }

    let key: String
    let value: Data

    init(from decoder: Decoder) throws {
        let container = try decoder.container(keyedBy: ColCodingKeys.self)
        key = try container.decode(String.self, forKey: .col1)

        let valueHex = try container.decode(String.self, forKey: .col2)
        value = try Data(hexString: valueHex)
    }

    init(scaleDecoder: ScaleDecoding) throws {
        key = try String(scaleDecoder: scaleDecoder)
        value = try Data(scaleDecoder: scaleDecoder)
    }

    func encode(scaleEncoder: ScaleEncoding) throws {
        try key.encode(scaleEncoder: scaleEncoder)
        try value.encode(scaleEncoder: scaleEncoder)
    }
}
