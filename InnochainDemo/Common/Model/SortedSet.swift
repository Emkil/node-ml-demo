import Foundation
import FearlessUtils

struct SortedSet<T: ScaleCodable & Comparable>: ScaleCodable {
    let items: [T]

    init(items: [T]) {
        self.items = items.sorted()
    }

    init(scaleDecoder: ScaleDecoding) throws {
        items = try [T].init(scaleDecoder: scaleDecoder).sorted()
    }

    func encode(scaleEncoder: ScaleEncoding) throws {
        try items.encode(scaleEncoder: scaleEncoder)
    }
}
