import Foundation
import IrohaCrypto
import FearlessUtils
import BigInt

enum TransactionBuilderError: Error {
    case emptyPublicKeys
    case missingSender
    case noTransactionInitiated
    case missingNonce
}

protocol TransactionBuilderProtocol: AnyObject {
    func from(_ sender: AccountId) -> Self
    func timestamp(_ timestamp: Date) -> Self
    func nonce(_ nonce: UInt32) -> Self
    func createUser(with accountId: AccountId, publicKeys: SortedSet<PublicKey>) throws -> Self
    func createContract(_ contractAccountId: AccountId, contractInit: ContractInit) throws -> Self
    func approveAgreement(_ contractAccountId: AccountId) throws -> Self
    func changePrice(_ newPrice: BigUInt, startTime: BigUInt, contractAccountId: AccountId) throws -> Self
    func approvePrice(_ contractAccountId: AccountId) throws -> Self
    func addTask(taskInit: TaskInit, to contractAccountId: AccountId) throws -> Self
    func sign(using signers: [IRSignatureCreatorProtocol]) throws -> SignedTransaction
}

final class TransactionBuilder {
    private lazy var timestamp: Date = Date()
    private var sender: AccountId?
    private var receiver: AccountId?
    private var type: TransactionPayload.TxType?
    private var nonce: UInt32?
    private var data: Data?
}

extension TransactionBuilder: TransactionBuilderProtocol {
    func from(_ sender: AccountId) -> Self {
        self.sender = sender
        return self
    }

    func timestamp(_ timestamp: Date) -> Self {
        self.timestamp = timestamp
        return self
    }

    func nonce(_ nonce: UInt32) -> Self {
        self.nonce = nonce
        return self
    }

    func createUser(with accountId: AccountId, publicKeys: SortedSet<PublicKey>) throws -> Self {
        guard !publicKeys.items.isEmpty else {
            throw TransactionBuilderError.emptyPublicKeys
        }

        self.sender = accountId
        self.type = .createUserAccount
        self.nonce = 0

        let encoder = ScaleEncoder()
        try publicKeys.encode(scaleEncoder: encoder)
        self.data = encoder.encode()

        return self
    }

    func createContract(_ contractAccountId: AccountId, contractInit: ContractInit) throws -> Self {
        let params: [SCValue] = [
            .scString("constructor"),
            .scInt(contractInit.customerAccoundId.toBigEndian()),
            .scString(contractInit.customerName),
            .scInt(contractInit.supplierAccoundId.toBigEndian()),
            .scString(contractInit.supplierName),
            .scString(contractInit.agreement),
            .scInt(contractInit.bankAccountId.toBigEndian())
        ]

        type = .createContract
        receiver = contractAccountId

        let encoder = ScaleEncoder()
        try params.encode(scaleEncoder: encoder)
        data = encoder.encode()

        return self
    }

    func approveAgreement(_ contractAccountId: AccountId) throws -> Self {
        let params: [SCValue] = [
            .scString("approveAgreement")
        ]

        type = .contractCall
        receiver = contractAccountId

        let encoder = ScaleEncoder()
        try params.encode(scaleEncoder: encoder)
        data = encoder.encode()

        return self
    }

    func changePrice(_ newPrice: BigUInt, startTime: BigUInt, contractAccountId: AccountId) throws -> Self {
        let params: [SCValue] = [
            .scString("createPriceChange"),
            .scInt(newPrice),
            .scInt(startTime)
        ]

        type = .contractCall
        receiver = contractAccountId

        let encoder = ScaleEncoder()
        try params.encode(scaleEncoder: encoder)
        data = encoder.encode()

        return self
    }

    func approvePrice(_ contractAccountId: AccountId) throws -> Self {
        let params: [SCValue] = [
            .scString("approvePrice")
        ]

        type = .contractCall
        receiver = contractAccountId

        let encoder = ScaleEncoder()
        try params.encode(scaleEncoder: encoder)
        data = encoder.encode()

        return self
    }

    func addTask(taskInit: TaskInit, to contractAccountId: AccountId) throws -> Self {
        let taskId = try AccountId.createRandom().toBigEndian()
        let params: [SCValue] = [
            .scString("addTask"),
            .scInt(taskId),
            .scNegotiation(.waitingSupplier),
            .scInt(taskInit.captainAccountId.toBigEndian()),
            .scString(taskInit.captainName),
            .scInt(taskInit.workerAccountId.toBigEndian()),
            .scString(taskInit.workerName),
            .scInt(taskInit.expectedGas),
            .scInt(0),
            .scInt(0),
            .scInt(0),
            .scInt(0),
            .scInt(0),
            .scInt(0),
            .scInt(0),
            .scTaskStatus(.taskNotAccepted),
            .scPaymentType(taskInit.paymentType)
        ]

        type = .contractCall
        receiver = contractAccountId

        let encoder = ScaleEncoder()
        try params.encode(scaleEncoder: encoder)
        data = encoder.encode()

        return self
    }

    func approveTask(to contractAccountId: AccountId, taskId: BigUInt) throws -> Self {
        let params: [SCValue] = [
            .scString("approveTask"),
            .scInt(taskId)
        ]

        type = .contractCall
        receiver = contractAccountId

        let encoder = ScaleEncoder()
        try params.encode(scaleEncoder: encoder)
        data = encoder.encode()

        return self
    }

    func acceptTask(to contractAccountId: AccountId, taskId: BigUInt) throws -> Self {
        let params: [SCValue] = [
            .scString("acceptTask"),
            .scInt(taskId)
        ]

        type = .contractCall
        receiver = contractAccountId

        let encoder = ScaleEncoder()
        try params.encode(scaleEncoder: encoder)
        data = encoder.encode()

        return self
    }

    func readyToPerformTask(to contractAccountId: AccountId, taskId: BigUInt) throws -> Self {
        let params: [SCValue] = [
            .scString("readyToPerformTask"),
            .scInt(taskId)
        ]

        type = .contractCall
        receiver = contractAccountId

        let encoder = ScaleEncoder()
        try params.encode(scaleEncoder: encoder)
        data = encoder.encode()

        return self
    }

    func requestGas(to contractAccountId: AccountId, taskId: BigUInt, gas: BigUInt) throws -> Self {
        let paymentId = try AccountId.createRandom().toBigEndian()
        let params: [SCValue] = [
            .scString("requestGas"),
            .scInt(taskId),
            .scInt(gas),
            .scInt(paymentId)
        ]

        type = .contractCall
        receiver = contractAccountId

        let encoder = ScaleEncoder()
        try params.encode(scaleEncoder: encoder)
        data = encoder.encode()

        return self
    }

    func performTask(to contractAccountId: AccountId, taskId: BigUInt) throws -> Self {
        let params: [SCValue] = [
            .scString("performTask"),
            .scInt(taskId)
        ]

        type = .contractCall
        receiver = contractAccountId

        let encoder = ScaleEncoder()
        try params.encode(scaleEncoder: encoder)
        data = encoder.encode()

        return self
    }

    func completeTask(to contractAccountId: AccountId, taskId: BigUInt, gas: BigUInt) throws -> Self {
        let params: [SCValue] = [
            .scString("taskCompleted"),
            .scInt(taskId),
            .scInt(gas)
        ]

        type = .contractCall
        receiver = contractAccountId

        let encoder = ScaleEncoder()
        try params.encode(scaleEncoder: encoder)
        data = encoder.encode()

        return self
    }

    func confirmTask(to contractAccountId: AccountId, taskId: BigUInt) throws -> Self {
        let params: [SCValue] = [
            .scString("confirmTask"),
            .scInt(taskId)
        ]

        type = .contractCall
        receiver = contractAccountId

        let encoder = ScaleEncoder()
        try params.encode(scaleEncoder: encoder)
        data = encoder.encode()

        return self
    }

    func confirmPayment(to contractAccountId: AccountId, paymentId: BigUInt) throws -> Self {
        let params: [SCValue] = [
            .scString("paymentCompleted"),
            .scInt(paymentId)
        ]

        type = .contractCall
        receiver = contractAccountId

        let encoder = ScaleEncoder()
        try params.encode(scaleEncoder: encoder)
        data = encoder.encode()

        return self
    }

    func sign(using signers: [IRSignatureCreatorProtocol]) throws -> SignedTransaction {
        guard let sender = sender else {
            throw TransactionBuilderError.missingSender
        }

        guard let type = type else {
            throw TransactionBuilderError.noTransactionInitiated
        }

        guard let nonce = nonce else {
            throw TransactionBuilderError.missingNonce
        }

        let payload = TransactionPayload(
            timestamp: UInt64(timestamp.timeIntervalSince1970 * 1e+6),
            sender: sender,
            receiver: receiver,
            type: type,
            nonce: nonce,
            data: data
        )

        let encoder = ScaleEncoder()
        try payload.encode(scaleEncoder: encoder)
        let originalData = encoder.encode()

        let signatures: [Signature] = try signers.map { signer in
            let rawSignature = try signer.sign(originalData).rawData()
            return Signature(value: rawSignature)
        }

        return SignedTransaction(payload: payload, signatures: SortedSet(items: signatures))
    }
}
