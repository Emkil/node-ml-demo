import Foundation

struct DbContract: Codable {
    enum CodingKeys: String, CodingKey {
        case identifier
        case participant
        case type
        case name
        case createdAt
    }

    let identifier: String
    let participant: String
    let type: UInt16
    let name: String
    let createdAt: Date
}
