import Foundation

struct DbTransaction: Codable {
    enum CodingKeys: String, CodingKey {
        case identifier
        case sender
        case data
        case createdAt
        case status
    }

    let identifier: String
    let sender: String
    let data: Data
    let createdAt: Date
    let status: TransactionStatus
}

extension DbTransaction {
    func changingStatus(to newStatus: TransactionStatus) -> DbTransaction {
        DbTransaction(identifier: identifier, sender: sender, data: data, createdAt: createdAt, status: newStatus)
    }
}
