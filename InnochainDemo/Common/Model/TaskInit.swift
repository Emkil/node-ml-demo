import Foundation
import BigInt

struct TaskInit {
    let captainAccountId: AccountId
    let captainName: String
    let workerAccountId: AccountId
    let workerName: String
    let expectedGas: BigUInt
    let paymentType: SCPaymentType
}
