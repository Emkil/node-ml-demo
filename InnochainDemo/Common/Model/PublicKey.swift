import Foundation
import FearlessUtils

struct PublicKey: FixedLengthDataStoring & Comparable {
    static var length: Int { 182 }
    let value: Data

    static func < (lhs: PublicKey, rhs: PublicKey) -> Bool {
        lhs.value.lexicographicallyPrecedes(rhs.value)
    }
}
