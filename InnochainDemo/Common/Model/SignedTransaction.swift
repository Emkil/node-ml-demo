import Foundation
import FearlessUtils
import BigInt

struct SignedTransaction: ScaleCodable {
    let payload: TransactionPayload
    let signatures: SortedSet<Signature>

    init(payload: TransactionPayload, signatures: SortedSet<Signature>) {
        self.payload = payload
        self.signatures = signatures
    }

    init(scaleDecoder: ScaleDecoding) throws {
        payload = try TransactionPayload(scaleDecoder: scaleDecoder)
        signatures = try SortedSet(scaleDecoder: scaleDecoder)
    }

    func encode(scaleEncoder: ScaleEncoding) throws {
        try payload.encode(scaleEncoder: scaleEncoder)
        try signatures.encode(scaleEncoder: scaleEncoder)
    }
}

struct TransactionPayload: ScaleCodable {
    enum TxType: UInt8 {
        case createContract
        case contractCall
        case createUserAccount
        case approveUserAccount
        case blockUserAccount
        case setStorage
        case setPublicKeys
        case addPublicKeys
        case removePublicKeys
    }

    let timestamp: UInt64
    let sender: AccountId
    let receiver: AccountId?
    let type: TxType
    let nonce: UInt32
    let data: Data?

    init(
        timestamp: UInt64,
        sender: AccountId,
        receiver: AccountId?,
        type: TxType,
        nonce: UInt32,
        data: Data?
    ) {
        self.timestamp = timestamp
        self.sender = sender
        self.receiver = receiver
        self.type = type
        self.nonce = nonce
        self.data = data
    }

    init(scaleDecoder: ScaleDecoding) throws {
        let timestampValue = try BigUInt(scaleDecoder: scaleDecoder)
        timestamp = UInt64(timestampValue)

        sender = try AccountId(scaleDecoder: scaleDecoder)

        switch try ScaleOption<AccountId>(scaleDecoder: scaleDecoder) {
        case .none:
            receiver = nil
        case let .some(value):
            receiver = value
        }

        let rawType = try UInt8(scaleDecoder: scaleDecoder)

        guard let txType = TxType(rawValue: rawType) else {
            throw ScaleCodingError.unexpectedDecodedValue
        }

        self.type = txType

        nonce = try UInt32(scaleDecoder: scaleDecoder)

        switch try ScaleOption<Data>(scaleDecoder: scaleDecoder) {
        case .none:
            data = nil
        case let .some(value):
            data = value
        }
    }

    func encode(scaleEncoder: ScaleEncoding) throws {
        try BigUInt(timestamp).encode(scaleEncoder: scaleEncoder)
        try sender.encode(scaleEncoder: scaleEncoder)

        if let receiver = receiver {
            try ScaleOption.some(value: receiver).encode(scaleEncoder: scaleEncoder)
        } else {
            try ScaleOption<AccountId>.none.encode(scaleEncoder: scaleEncoder)
        }

        try type.rawValue.encode(scaleEncoder: scaleEncoder)
        try nonce.encode(scaleEncoder: scaleEncoder)

        if let data = data {
            try ScaleOption.some(value: data).encode(scaleEncoder: scaleEncoder)
        } else {
            try ScaleOption<Data>.none.encode(scaleEncoder: scaleEncoder)
        }
    }
}
