import Foundation

enum TransactionStatus: UInt8, Codable {
    case pending
    case failed
    case completed
}
