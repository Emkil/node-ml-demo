import Foundation

struct TxCommitInfo: Codable {
    enum CodingKeys: String, CodingKey {
        case version
        case ledgerIndex = "ledger_index"
    }

    let version: UInt64
    let ledgerIndex: UInt64
}
