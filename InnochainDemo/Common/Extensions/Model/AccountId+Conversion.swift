import Foundation
import BigInt

extension AccountId {
    static func fromBigUInt(_ value: BigUInt) -> AccountId {
        let data = value.serialize()
        let trailingZeros = max(AccountId.length - data.count, 0)

        var result = Data(repeating: 0, count: trailingZeros)
        result.append(data)

        return AccountId(value: result)
    }
}
