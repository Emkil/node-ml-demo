import Foundation

extension DbAccount {
    var accountId: AccountId? {
        try? AccountId.fromHex(string: identifier)
    }
}
