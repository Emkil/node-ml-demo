import Foundation
import RobinHood

final class OperationQueueFacade {
    static let defaultQueue = OperationQueue()
    static let defaultManager = OperationManager(operationQueue: defaultQueue)
}
