import Foundation

extension SCNegotiation {
    var title: String {
        switch self {
        case .notSet:
            return ""
        case .negotiationApproved:
            return R.string.localizable.contractNegotiationWaitingApproved()
        case .negotiationRejected:
            return R.string.localizable.contractNegotiationWaitingRejected()
        case .waitingCustomer:
            return R.string.localizable.contractNegotiationWaitingCustomer()
        case .waitingSupplier:
            return R.string.localizable.contractNegotiationWaitingSupplier()
        }
    }
}
