import Foundation
import RobinHood

extension DbTransaction: Identifiable {}
extension DbAccount: Identifiable {}
extension DbContract: Identifiable {}
