import Foundation
import RobinHood
import CoreData

extension CDTransactionItem: CoreDataCodable {
    public func populate(from decoder: Decoder, using _: NSManagedObjectContext) throws {
        let container = try decoder.container(keyedBy: DbTransaction.CodingKeys.self)

        identifier = try container.decode(String.self, forKey: .identifier)
        sender = try container.decode(String.self, forKey: .sender)
        status = try container.decode(Int16.self, forKey: .status)
        data = try container.decode(Data.self, forKey: .data)
        createdAt = try container.decode(Date.self, forKey: .createdAt)
    }

    public func encode(to encoder: Encoder) throws {
        var container = encoder.container(keyedBy: DbTransaction.CodingKeys.self)

        try container.encodeIfPresent(identifier, forKey: .identifier)
        try container.encodeIfPresent(sender, forKey: .sender)
        try container.encodeIfPresent(status, forKey: .status)
        try container.encodeIfPresent(data, forKey: .data)
        try container.encodeIfPresent(createdAt, forKey: .createdAt)
    }
}
