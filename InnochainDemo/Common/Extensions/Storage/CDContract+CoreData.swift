import Foundation
import RobinHood
import CoreData

extension CDContract: CoreDataCodable {
    public func populate(from decoder: Decoder, using _: NSManagedObjectContext) throws {
        let container = try decoder.container(keyedBy: DbContract.CodingKeys.self)

        identifier = try container.decode(String.self, forKey: .identifier)
        participant = try container.decode(String.self, forKey: .participant)
        name = try container.decode(String.self, forKey: .name)
        type = try container.decode(Int16.self, forKey: .type)
        createdAt = try container.decode(Date.self, forKey: .createdAt)
    }

    public func encode(to encoder: Encoder) throws {
        var container = encoder.container(keyedBy: DbContract.CodingKeys.self)

        try container.encodeIfPresent(identifier, forKey: .identifier)
        try container.encodeIfPresent(participant, forKey: .participant)
        try container.encodeIfPresent(type, forKey: .type)
        try container.encodeIfPresent(name, forKey: .name)
        try container.encodeIfPresent(createdAt, forKey: .createdAt)
    }
}
