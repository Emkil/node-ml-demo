import Foundation
import SoraKeystore

enum KeystoreTag {
    static func secretKeyTagForAddress(_ accountId: AccountId) -> String { accountId.value.toHex() + "-" + "secretKey" }
}

extension KeystoreProtocol {
    func loadIfKeyExists(_ tag: String) throws -> Data? {
        guard try checkKey(for: tag) else {
            return nil
        }

        return try fetchKey(for: tag)
    }

    func saveSecretKey(_ secretKey: Data, accountId: AccountId) throws {
        let tag = KeystoreTag.secretKeyTagForAddress(accountId)

        try saveKey(secretKey, with: tag)
    }

    func fetchSecretKeyForAccountId(_ accountId: AccountId) throws -> Data? {
        let tag = KeystoreTag.secretKeyTagForAddress(accountId)

        return try loadIfKeyExists(tag)
    }

    func checkSecretKeyForAccountId(_ accountId: AccountId) throws -> Bool {
        let tag = KeystoreTag.secretKeyTagForAddress(accountId)
        return try checkKey(for: tag)
    }
}
