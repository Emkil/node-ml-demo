import Foundation

extension DateFormatter {
    static var shortDateAndTime: DateFormatter {
        let format = DateFormatter.dateFormat(fromTemplate: "ddMMMyyyyHH:mm", options: 0, locale: Locale.current)
        let dateFormatter = DateFormatter()
        dateFormatter.dateFormat = format
        return dateFormatter
    }
}
