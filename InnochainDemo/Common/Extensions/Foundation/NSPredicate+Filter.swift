import Foundation

extension NSPredicate {
    static func contracts(for participant: AccountId) -> NSPredicate {
        return NSPredicate(
            format: "%K == %@",
            #keyPath(CDContract.participant),
            participant.value.toHex(includePrefix: true)
        )
    }
}
