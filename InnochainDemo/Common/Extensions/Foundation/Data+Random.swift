import Foundation

enum DataRandomError: Error {
    case internalError
}

extension Data {
    static func random(length: Int) throws -> Data {
        var data = Data(count: length)
        let result = data.withUnsafeMutableBytes {
            return SecRandomCopyBytes(kSecRandomDefault, length, $0)
        }

        guard result == errSecSuccess else {
            throw DataRandomError.internalError
        }

        return data
    }
}
