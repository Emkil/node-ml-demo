import Foundation

extension NSPredicate {
    static var notEmpty: NSPredicate {
        NSPredicate(format: "SELF != ''")
    }

    static var account: NSPredicate {
        let format = "(0x)?[a-fA-F0-9]{32}"
        return NSPredicate(format: "SELF MATCHES %@", format)
    }

    static var price: NSPredicate {
        let format = "[1-9]([0-9]*)"
        return NSPredicate(format: "SELF MATCHES %@", format)
    }

    static var quantity: NSPredicate {
        let format = "[1-9]([0-9]*)"
        return NSPredicate(format: "SELF MATCHES %@", format)
    }
}
