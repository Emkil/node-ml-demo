import Foundation
import IrohaCrypto
import FearlessUtils

extension SignedTransaction {
    func transactionHash() throws -> Data {
        let encoder = ScaleEncoder()
        try encode(scaleEncoder: encoder)
        return try (encoder.encode() as NSData).stribog()
    }
}
