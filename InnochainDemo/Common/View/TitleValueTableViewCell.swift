import UIKit

final class TitleValueTableViewCell: BaseTitleValueTableViewCell {
    override init(style: UITableViewCell.CellStyle, reuseIdentifier: String?) {
        super.init(style: style, reuseIdentifier: reuseIdentifier)

        selectionStyle = .none
    }

    required init?(coder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }

    func bind(title: String, value: String) {
        titleLabel.text = title
        detailsLabel.text = value
    }
}
