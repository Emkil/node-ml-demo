import Foundation

final class ActionTableViewCell: BaseTitleValueTableViewCell {
    func bind(title: String) {
        titleLabel.text = title
        detailsLabel.text = ""
        accessoryType = .disclosureIndicator
    }
}
