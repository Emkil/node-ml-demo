import Foundation
import SoraKeystore
import IrohaCrypto

class SigningWrapper {
    var keystore: KeystoreProtocol
    var tag: String

    init(keystore: KeystoreProtocol, tag: String) {
        self.keystore = keystore
        self.tag = tag
    }
}

extension SigningWrapper: IRSignatureCreatorProtocol {
    func sign(_ originalData: Data) throws -> IRSignatureProtocol {
        let rawKey = try keystore.fetchKey(for: tag)

        let privateKey = try GostPrivateKey(rawData: rawKey)

        let rawSigner = GostSigner(privateKey: privateKey)

        return try rawSigner.sign(originalData)
    }
}
