import BigInt

protocol TaskDetailsViewProtocol: ControllerBackedProtocol, LoadableViewProtocol {
    func didReceive(viewModel: TaskDetailsViewModel)
}

protocol TaskDetailsPresenterProtocol: class {
    func setup()
    func performAction(from viewModel: TaskDetailsViewModel)
}

protocol TaskDetailsInteractorInputProtocol: class {
    func setup()
    func approveTask(_ taskId: BigUInt)
    func acceptTask(_ taskId: BigUInt)
    func readyToPerformTask(_ taskId: BigUInt)
    func requestGas(_ taskId: BigUInt, amount: BigUInt)
    func startTask(_ taskId: BigUInt)
    func complete(_ taskId: BigUInt, amount: BigUInt)
    func confirmTask(_ taskId: BigUInt)
    func confirmPayment(_ paymentId: BigUInt)
}

protocol TaskDetailsInteractorOutputProtocol: class {
    func didReceiveCampaign(result: Result<SCCampaign, Error>)
    func didReceiveAccount(result: Result<DbAccount?, Error>)

    func willSubmitTransaction(for hash: String)
    func didSubmitTransaction(for hash: String)
    func didReceiveTransaction(result: Result<Bool, Error>, for hash: String?)
}

protocol TaskDetailsWireframeProtocol: class {}
