import UIKit

final class TaskDetailsViewController: UIViewController, ViewHolder {
    enum Row: UInt8, CaseIterable {
        case status
        case captain
        case worker
        case estimatedGas
        case requestedGas
        case suppliedGas
        case paymentWaiting
        case paymentConfirmed

        init?(row: Int, viewModel: TaskDetailsViewModel) {
            let paymentRow: Row? = {
                switch viewModel.paymentState {
                case .notSet:
                    return nil
                case .paid:
                    return .paymentConfirmed
                case .waiting:
                    return .paymentWaiting
                }
            }()

            switch row {
            case 0:
                self = .status
            case 1:
                self = .captain
            case 2:
                self = .worker
            case 3:
                if case .estimated = viewModel.gasState {
                    self = .estimatedGas
                } else {
                    self = .requestedGas
                }
            case 4:
                if viewModel.suppliedGas != nil {
                    self = .suppliedGas
                    return
                }

                if let paymentRow = paymentRow {
                    self = paymentRow
                } else {
                    return nil
                }

            case 5:
                if let paymentRow = paymentRow {
                    self = paymentRow
                } else {
                    return nil
                }
            default:
                return nil
            }
        }

        var title: String {
            switch self {
            case .status:
                return R.string.localizable.taskDetailsStatusTitle()
            case .captain:
                return R.string.localizable.taskDetailsCaptainTitle()
            case .worker:
                return R.string.localizable.taskDetailsWorkerTitle()
            case .estimatedGas:
                return R.string.localizable.taskDetailsEstimatedGasTitle()
            case .requestedGas:
                return R.string.localizable.taskDetailsRequestedGasTitle()
            case .suppliedGas:
                return R.string.localizable.taskDetailsSuppliedGasTitle()
            case .paymentWaiting:
                return R.string.localizable.taskDetailsPaymentWaitingTitle()
            case .paymentConfirmed:
                return R.string.localizable.taskDetailsPaymentConfirmedTitle()
            }
        }

        func value(from viewModel: TaskDetailsViewModel) -> String {
            switch self {
            case .status:
                return viewModel.taskState.title
            case .captain:
                return viewModel.captain
            case .worker:
                return viewModel.worker
            case .estimatedGas, .requestedGas:
                return viewModel.gasState.value
            case .suppliedGas:
                return viewModel.suppliedGas ?? ""
            case .paymentWaiting, .paymentConfirmed:
                return viewModel.paymentState.amount ?? ""
            }
        }
    }

    enum ActionsRow: UInt8, CaseIterable {
        case tasks
        case priceChanges
        case payment

        var title: String {
            switch self {
            case .tasks:
                return R.string.localizable.contractDetailsActionTasks()
            case .priceChanges:
                return R.string.localizable.contractDetailsActionChangePrice()
            case .payment:
                return R.string.localizable.contractDetailsActionPayment()
            }
        }
    }

    typealias RootViewType = TaskDetailsViewLayout

    let presenter: TaskDetailsPresenterProtocol

    private var viewModel: TaskDetailsViewModel?

    init(presenter: TaskDetailsPresenterProtocol) {
        self.presenter = presenter
        super.init(nibName: nil, bundle: nil)
    }

    @available(*, unavailable)
    required init?(coder _: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }

    override func loadView() {
        view = TaskDetailsViewLayout()
    }

    override func viewDidLoad() {
        super.viewDidLoad()

        setupTableView()

        presenter.setup()
    }

    private func setupTableView() {
        let refreshControl = UIRefreshControl()
        rootView.tableView.refreshControl = refreshControl

        rootView.tableView.registerClassForCell(TitleValueTableViewCell.self)

        rootView.tableView.dataSource = self
        rootView.tableView.rowHeight = 48.0
    }

    @objc private func actionTask() {
        guard let viewModel = viewModel else {
            return
        }

        presenter.performAction(from: viewModel)
    }
}

extension TaskDetailsViewController: UITableViewDataSource {
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        guard let viewModel = viewModel else {
            return 0
        }

        var numberOfRows = 4

        if viewModel.suppliedGas != nil {
            numberOfRows += 1
        }

        if viewModel.paymentState.amount != nil {
            numberOfRows += 1
        }

        return numberOfRows
    }

    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        guard let viewModel = viewModel, let row = Row(row: indexPath.row, viewModel: viewModel) else {
            return UITableViewCell()
        }

        let cell = tableView.dequeueReusableCellWithType(TitleValueTableViewCell.self)!
        cell.bind(title: row.title, value: row.value(from: viewModel))
        return cell
    }

    func tableView(_ tableView: UITableView, titleForHeaderInSection section: Int) -> String? {
        guard let viewModel = viewModel else {
            return nil
        }

        return viewModel.taskState.details
    }
}

extension TaskDetailsViewController: TaskDetailsViewProtocol {
    func didReceive(viewModel: TaskDetailsViewModel) {
        self.viewModel = viewModel
        rootView.tableView.reloadData()

        title = viewModel.taskId

        if let action = viewModel.action {
            rootView.setupActionBar(for: action.title)

            rootView.actionButtonItem?.target = self
            rootView.actionButtonItem?.action = #selector(actionTask)
        } else {
            rootView.clearActionBar()
        }
    }
}
