import UIKit
import RobinHood
import IrohaCrypto
import BigInt

final class TaskDetailsInteractor {
    weak var presenter: TaskDetailsInteractorOutputProtocol!

    let selectedAccountId: AccountId
    let contractAccountId: AccountId
    let nodeOperationFactory: JSONRPCOperationFactoryProtocol
    let transactionService: TransactionServiceProtocol
    let txProvider: StreamableProvider<DbTransaction>
    let signer: IRSignatureCreatorProtocol
    let contractRepository: AnyDataProviderRepository<DbContract>
    let accountRepository: AnyDataProviderRepository<DbAccount>
    let operationQueue: OperationQueue

    init(
        selectedAccountId: AccountId,
        contractAccountId: AccountId,
        nodeOperationFactory: JSONRPCOperationFactoryProtocol,
        transactionService: TransactionServiceProtocol,
        txProvider: StreamableProvider<DbTransaction>,
        signer: IRSignatureCreatorProtocol,
        contractRepository: AnyDataProviderRepository<DbContract>,
        accountRepository: AnyDataProviderRepository<DbAccount>,
        operationQueue: OperationQueue
    ) {
        self.selectedAccountId = selectedAccountId
        self.contractAccountId = contractAccountId
        self.nodeOperationFactory = nodeOperationFactory
        self.transactionService = transactionService
        self.txProvider = txProvider
        self.signer = signer
        self.contractRepository = contractRepository
        self.accountRepository = accountRepository
        self.operationQueue = operationQueue
    }

    private func provideRemoteContract() {
        fetchRemoteContract(
            for: contractAccountId,
            operationFactory: nodeOperationFactory,
            operationQueue: operationQueue
        ) { [weak self] (result) in
            self?.presenter.didReceiveCampaign(result: result)
        }
    }

    private func provideAccount() {
        fetchLocalAccount(
            for: selectedAccountId,
            repository: accountRepository,
            operationQueue: operationQueue
        ) { [weak self] (result) in
            self?.presenter.didReceiveAccount(result: result)
        }
    }

    private func submitTaskApproval(_ taskId: BigUInt, account: Account) {
        do {
            let transaction = try TransactionBuilder()
                .from(selectedAccountId)
                .nonce(account.nonce)
                .approveTask(to: contractAccountId, taskId: taskId)
                .sign(using: [signer])

            let hash = try transaction.transactionHash().toHex(includePrefix: true)

            presenter.willSubmitTransaction(for: hash)

            transactionService.submit(transaction: transaction, runningIn: .main) { _ in }
        } catch {
            presenter.didReceiveTransaction(result: .failure(error), for: nil)
        }
    }

    private func submitTaskAcceptance(_ taskId: BigUInt, account: Account) {
        do {
            let transaction = try TransactionBuilder()
                .from(selectedAccountId)
                .nonce(account.nonce)
                .acceptTask(to: contractAccountId, taskId: taskId)
                .sign(using: [signer])

            let hash = try transaction.transactionHash().toHex(includePrefix: true)

            presenter.willSubmitTransaction(for: hash)

            transactionService.submit(transaction: transaction, runningIn: .main) { _ in }
        } catch {
            presenter.didReceiveTransaction(result: .failure(error), for: nil)
        }
    }

    private func submitReadyToPerform(_ taskId: BigUInt, account: Account) {
        do {
            let transaction = try TransactionBuilder()
                .from(selectedAccountId)
                .nonce(account.nonce)
                .readyToPerformTask(to: contractAccountId, taskId: taskId)
                .sign(using: [signer])

            let hash = try transaction.transactionHash().toHex(includePrefix: true)

            presenter.willSubmitTransaction(for: hash)

            transactionService.submit(transaction: transaction, runningIn: .main) { _ in }
        } catch {
            presenter.didReceiveTransaction(result: .failure(error), for: nil)
        }
    }

    private func submitGasRequest(_ taskId: BigUInt, gas: BigUInt, account: Account) {
        do {
            let transaction = try TransactionBuilder()
                .from(selectedAccountId)
                .nonce(account.nonce)
                .requestGas(to: contractAccountId, taskId: taskId, gas: gas)
                .sign(using: [signer])

            let hash = try transaction.transactionHash().toHex(includePrefix: true)

            presenter.willSubmitTransaction(for: hash)

            transactionService.submit(transaction: transaction, runningIn: .main) { _ in }
        } catch {
            presenter.didReceiveTransaction(result: .failure(error), for: nil)
        }
    }

    private func submitStartTask(_ taskId: BigUInt, account: Account) {
        do {
            let transaction = try TransactionBuilder()
                .from(selectedAccountId)
                .nonce(account.nonce)
                .performTask(to: contractAccountId, taskId: taskId)
                .sign(using: [signer])

            let hash = try transaction.transactionHash().toHex(includePrefix: true)

            presenter.willSubmitTransaction(for: hash)

            transactionService.submit(transaction: transaction, runningIn: .main) { _ in }
        } catch {
            presenter.didReceiveTransaction(result: .failure(error), for: nil)
        }
    }

    private func submitCompleteTask(_ taskId: BigUInt, gas: BigUInt, account: Account) {
        do {
            let transaction = try TransactionBuilder()
                .from(selectedAccountId)
                .nonce(account.nonce)
                .completeTask(to: contractAccountId, taskId: taskId, gas: gas)
                .sign(using: [signer])

            let hash = try transaction.transactionHash().toHex(includePrefix: true)

            presenter.willSubmitTransaction(for: hash)

            transactionService.submit(transaction: transaction, runningIn: .main) { _ in }
        } catch {
            presenter.didReceiveTransaction(result: .failure(error), for: nil)
        }
    }

    private func submitConfirmTask(_ taskId: BigUInt, account: Account) {
        do {
            let transaction = try TransactionBuilder()
                .from(selectedAccountId)
                .nonce(account.nonce)
                .confirmTask(to: contractAccountId, taskId: taskId)
                .sign(using: [signer])

            let hash = try transaction.transactionHash().toHex(includePrefix: true)

            presenter.willSubmitTransaction(for: hash)

            transactionService.submit(transaction: transaction, runningIn: .main) { _ in }
        } catch {
            presenter.didReceiveTransaction(result: .failure(error), for: nil)
        }
    }

    private func submitConfirmPayment(_ paymentId: BigUInt, account: Account) {
        do {
            let transaction = try TransactionBuilder()
                .from(selectedAccountId)
                .nonce(account.nonce)
                .confirmPayment(to: contractAccountId, paymentId: paymentId)
                .sign(using: [signer])

            let hash = try transaction.transactionHash().toHex(includePrefix: true)

            presenter.willSubmitTransaction(for: hash)

            transactionService.submit(transaction: transaction, runningIn: .main) { _ in }
        } catch {
            presenter.didReceiveTransaction(result: .failure(error), for: nil)
        }
    }
}

extension TaskDetailsInteractor: TaskDetailsInteractorInputProtocol {
    func setup() {
        provideRemoteContract()
        provideAccount()
        subscribeToTransactions(using: txProvider)
    }

    func approveTask(_ taskId: BigUInt) {
        fetchRemoteAccount(
            for: selectedAccountId,
            operationFactory: nodeOperationFactory,
            operationQueue: operationQueue) { [weak self] result in
            switch result {
            case let .success(account):
                self?.submitTaskApproval(taskId, account: account)
            case let .failure(error):
                self?.presenter.didReceiveTransaction(result: .failure(error), for: nil)
            }
        }
    }

    func acceptTask(_ taskId: BigUInt) {
        fetchRemoteAccount(
            for: selectedAccountId,
            operationFactory: nodeOperationFactory,
            operationQueue: operationQueue) { [weak self] result in
            switch result {
            case let .success(account):
                self?.submitTaskAcceptance(taskId, account: account)
            case let .failure(error):
                self?.presenter.didReceiveTransaction(result: .failure(error), for: nil)
            }
        }
    }

    func readyToPerformTask(_ taskId: BigUInt) {
        fetchRemoteAccount(
            for: selectedAccountId,
            operationFactory: nodeOperationFactory,
            operationQueue: operationQueue) { [weak self] result in
            switch result {
            case let .success(account):
                self?.submitReadyToPerform(taskId, account: account)
            case let .failure(error):
                self?.presenter.didReceiveTransaction(result: .failure(error), for: nil)
            }
        }
    }

    func requestGas(_ taskId: BigUInt, amount: BigUInt) {
        fetchRemoteAccount(
            for: selectedAccountId,
            operationFactory: nodeOperationFactory,
            operationQueue: operationQueue) { [weak self] result in
            switch result {
            case let .success(account):
                self?.submitGasRequest(taskId, gas: amount, account: account)
            case let .failure(error):
                self?.presenter.didReceiveTransaction(result: .failure(error), for: nil)
            }
        }
    }

    func startTask(_ taskId: BigUInt) {
        fetchRemoteAccount(
            for: selectedAccountId,
            operationFactory: nodeOperationFactory,
            operationQueue: operationQueue) { [weak self] result in
            switch result {
            case let .success(account):
                self?.submitStartTask(taskId, account: account)
            case let .failure(error):
                self?.presenter.didReceiveTransaction(result: .failure(error), for: nil)
            }
        }
    }

    func complete(_ taskId: BigUInt, amount: BigUInt) {
        fetchRemoteAccount(
            for: selectedAccountId,
            operationFactory: nodeOperationFactory,
            operationQueue: operationQueue) { [weak self] result in
            switch result {
            case let .success(account):
                self?.submitCompleteTask(taskId, gas: amount, account: account)
            case let .failure(error):
                self?.presenter.didReceiveTransaction(result: .failure(error), for: nil)
            }
        }
    }

    func confirmTask(_ taskId: BigUInt) {
        fetchRemoteAccount(
            for: selectedAccountId,
            operationFactory: nodeOperationFactory,
            operationQueue: operationQueue) { [weak self] result in
            switch result {
            case let .success(account):
                self?.submitConfirmTask(taskId, account: account)
            case let .failure(error):
                self?.presenter.didReceiveTransaction(result: .failure(error), for: nil)
            }
        }
    }

    func confirmPayment(_ paymentId: BigUInt) {
        fetchRemoteAccount(
            for: selectedAccountId,
            operationFactory: nodeOperationFactory,
            operationQueue: operationQueue) { [weak self] result in
            switch result {
            case let .success(account):
                self?.submitConfirmPayment(paymentId, account: account)
            case let .failure(error):
                self?.presenter.didReceiveTransaction(result: .failure(error), for: nil)
            }
        }
    }
}

extension TaskDetailsInteractor: LocalAccountFetching, RemoteAccountFetching,
                                 RemoteContractFetching, TxSubscribing {
    func handleTransactions(result: Result<[DbTransaction], Error>) {
        provideRemoteContract()

        switch result {
        case let .success(transactions):
            for transaction in transactions {
                switch transaction.status {
                case .completed:
                    presenter.didReceiveTransaction(result: .success(true), for: transaction.identifier)
                case .failed:
                    presenter.didReceiveTransaction(result: .success(false), for: transaction.identifier)
                case .pending:
                    presenter.didSubmitTransaction(for: transaction.identifier)
                }
            }
        case let .failure(error):
            presenter.didReceiveTransaction(result: .failure(error), for: nil)
        }
    }

}
