import Foundation
import BigInt
import RobinHood
import SoraKeystore

struct TaskDetailsViewFactory {
    static func createView(
        for contractId: AccountId,
        taskId: BigUInt,
        accountId: AccountId
    ) -> TaskDetailsViewProtocol? {
        guard let interactor = createInteractor(for: contractId, selectedAccountId: accountId) else {
            return nil
        }

        let wireframe = TaskDetailsWireframe()

        let presenter = TaskDetailsPresenter(
            interactor: interactor,
            wireframe: wireframe,
            viewModelFactory: TaskDetailsViewModelFactory(),
            taskId: taskId,
            logger: Logger.shared
        )

        let view = TaskDetailsViewController(presenter: presenter)

        presenter.view = view
        interactor.presenter = presenter

        return view
    }

    static private func createInteractor(
        for contractId: AccountId,
        selectedAccountId: AccountId
    ) -> TaskDetailsInteractor? {
        let storageFacade = UserDataStorageFacade.shared

        let accountRepository: CoreDataRepository<DbAccount, CDAccountItem> = storageFacade.createRepository()
        let contractRepository: CoreDataRepository<DbContract, CDContract> = storageFacade.createRepository()

        let txRepository: CoreDataRepository<DbTransaction, CDTransactionItem> =
            UserDataStorageFacade.shared.createRepository()

        let transactionService = TransactionService(
            operationFactory: NodeOperationFactory(url: PrivateUrl.main),
            repository: AnyDataProviderRepository(txRepository),
            operationQueue: OperationQueueFacade.defaultQueue
        )

        let txProviderFactory = StreamableProviderFactory(
            storageFacade: UserDataStorageFacade.shared,
            logger: Logger.shared
        )

        let signer = SigningWrapper(
            keystore: Keychain(),
            tag: KeystoreTag.secretKeyTagForAddress(selectedAccountId)
        )

        return TaskDetailsInteractor(
            selectedAccountId: selectedAccountId,
            contractAccountId: contractId,
            nodeOperationFactory: NodeOperationFactory(url: PrivateUrl.main),
            transactionService: transactionService,
            txProvider: txProviderFactory.createAllTransactionProvider(),
            signer: signer,
            contractRepository: AnyDataProviderRepository(contractRepository),
            accountRepository: AnyDataProviderRepository(accountRepository),
            operationQueue: OperationQueueFacade.defaultQueue
        )
    }
}
