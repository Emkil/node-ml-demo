import Foundation
import BigInt

final class TaskDetailsPresenter {
    weak var view: TaskDetailsViewProtocol?
    let wireframe: TaskDetailsWireframeProtocol
    let interactor: TaskDetailsInteractorInputProtocol
    let viewModelFactory: TaskDetailsViewModelFactoryProtocol
    let taskId: BigUInt
    let logger: LoggerProtocol?

    private var campaign: SCCampaign?
    private var account: DbAccount?

    init(
        interactor: TaskDetailsInteractorInputProtocol,
        wireframe: TaskDetailsWireframeProtocol,
        viewModelFactory: TaskDetailsViewModelFactoryProtocol,
        taskId: BigUInt,
        logger: LoggerProtocol? = nil
    ) {
        self.interactor = interactor
        self.wireframe = wireframe
        self.viewModelFactory = viewModelFactory
        self.taskId = taskId
        self.logger = logger
    }

    private func provideViewModel() {
        guard
            let campaign = campaign,
            let task = campaign.tasks.first(where: { $0.taskId == taskId }),
            let account = account else {
            return
        }

        let viewModel = viewModelFactory.createViewModel(from: campaign, task: task, account: account)
        view?.didReceive(viewModel: viewModel)
    }
}

extension TaskDetailsPresenter: TaskDetailsPresenterProtocol {
    func setup() {
        interactor.setup()
    }

    func performAction(from viewModel: TaskDetailsViewModel) {
        guard let action = viewModel.action, let task = campaign?.tasks.first(where: { $0.taskId == taskId }) else {
            return
        }

        switch action {
        case .approve:
            interactor.approveTask(taskId)
        case .accept:
            interactor.acceptTask(taskId)
        case .readyToPerform:
            interactor.readyToPerformTask(taskId)
        case .requestGas:
            interactor.requestGas(taskId, amount: task.expectedGas)
        case .performTask:
            interactor.startTask(taskId)
        case .completeTask:
            interactor.complete(taskId, amount: task.expectedGas)
        case .confirmTask:
            interactor.confirmTask(taskId)
        case .confirmPayment:
            if let payment = campaign?.paymentOrders.first(where: { $0.taskId == taskId }) {
                interactor.confirmPayment(payment.paymentId)
            } else {
                logger?.error("No payment id found")
            }
        }
    }
}

extension TaskDetailsPresenter: TaskDetailsInteractorOutputProtocol {
    func didReceiveCampaign(result: Result<SCCampaign, Error>) {
        switch result {
        case .success(let campaign):
            self.campaign = campaign
            provideViewModel()
        case .failure(let error):
            logger?.error("Did receive remote contract error: \(error)")
        }
    }

    func didReceiveAccount(result: Result<DbAccount?, Error>) {
        switch result {
        case .success(let account):
            self.account = account
            provideViewModel()
        case .failure(let error):
            logger?.error("Did receive account error: \(error)")
        }
    }

    func willSubmitTransaction(for hash: String) {
        view?.didStartLoading()

        logger?.debug("Will submit transaction: \(hash)")
    }

    func didSubmitTransaction(for hash: String) {
        logger?.debug("Did submit transaction: \(hash)")
    }

    func didReceiveTransaction(result: Result<Bool, Error>, for hash: String?) {
        view?.didStopLoading()

        switch result {
        case let .success(value):
            logger?.debug("Transaction result \(hash ?? ""): \(value)")
        case let .failure(error):
            logger?.error("Transaction submition \(hash ?? "") error: \(error)")
        }
    }
}
