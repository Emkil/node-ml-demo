import Foundation

struct TaskDetailsViewModel {
    enum GasState {
        case estimated(value: String)
        case requested(value: String)

        var value: String {
            switch self {
            case let .estimated(value), let .requested(value):
                return value
            }
        }
    }

    enum PaymentState {
        case notSet
        case waiting(amount: String)
        case paid(amount: String)

        var amount: String? {
            switch self {
            case .notSet:
                return nil
            case let .paid(amount), let .waiting(amount):
                return amount
            }
        }
    }

    enum TaskState {
        case needsApproval
        case declined
        case taskNotAccepted
        case taskAccepted
        case taskReadyToPerform
        case gasRequested
        case performing
        case waitingConfirmation
        case taskCompleted
        case waitingForPayment

        var title: String {
            switch self {
            case .needsApproval:
                return R.string.localizable.taskDetailsStatusNeedsApprovalTitle()
            case .declined:
                return R.string.localizable.taskDetailsStatusDeclinedTitle()
            case .taskNotAccepted:
                return R.string.localizable.taskDetailsStatusTaskNotAcceptedTitle()
            case .taskAccepted:
                return R.string.localizable.taskDetailsStatusTaskAcceptedTitle()
            case .taskReadyToPerform:
                return R.string.localizable.taskDetailsStatusReadyToPerformTitle()
            case .gasRequested:
                return R.string.localizable.taskDetailsStatusGasRequestedTitle()
            case .performing:
                return R.string.localizable.taskDetailsStatusPerformingTitle()
            case .waitingForPayment:
                return R.string.localizable.taskDetailsStatusWaitingPaymentTite()
            case .waitingConfirmation:
                return R.string.localizable.taskDetailsStatusWaitingConfirmationTitle()
            case .taskCompleted:
                return R.string.localizable.taskDetailsStatusTaskCompletedTitle()
            }
        }

        var details: String {
            switch self {
            case .needsApproval:
                return R.string.localizable.taskDetailsStatusNeedsApprovalDetails()
            case .declined:
                return R.string.localizable.taskDetailsStatusDeclinedDetails()
            case .taskNotAccepted:
                return R.string.localizable.taskDetailsStatusTaskNotAcceptedDetails()
            case .taskAccepted:
                return R.string.localizable.taskDetailsStatusTaskAcceptedDetails()
            case .taskReadyToPerform:
                return R.string.localizable.taskDetailsStatusReadyToPerformDetails()
            case .gasRequested:
                return R.string.localizable.taskDetailsStatusGasRequestedDetails()
            case .performing:
                return R.string.localizable.taskDetailsStatusPerformingDetails()
            case .waitingForPayment:
                return R.string.localizable.taskDetailsStatusWaitingPaymentDetails()
            case .waitingConfirmation:
                return R.string.localizable.taskDetailsStatusWaitingConfirmationDetails()
            case .taskCompleted:
                return R.string.localizable.taskDetailsStatusTaskCompletedDetails()
            }
        }
    }

    enum Action {
        case approve
        case confirmPayment
        case accept
        case readyToPerform
        case requestGas
        case performTask
        case completeTask
        case confirmTask

        var title: String {
            switch self {
            case .approve:
                return R.string.localizable.commonConfirm()
            case .confirmPayment:
                return R.string.localizable.taskDetailsActionConfirmPayment()
            case .accept:
                return R.string.localizable.taskDetailsActionAccept()
            case .readyToPerform:
                return R.string.localizable.taskDetailsActionReadyToPerform()
            case .requestGas:
                return R.string.localizable.taskDetailsActionRequestGas()
            case .performTask:
                return R.string.localizable.taskDetailsActionStartFueling()
            case .completeTask:
                return R.string.localizable.taskDetailsActionCompleteFueling()
            case .confirmTask:
                return R.string.localizable.taskDetailsActionConfirmFueling()
            }
        }
    }

    let taskId: String
    let captain: String
    let worker: String
    let gasState: GasState
    let suppliedGas: String?
    let taskState: TaskState
    let paymentState: PaymentState
    let action: Action?
}
