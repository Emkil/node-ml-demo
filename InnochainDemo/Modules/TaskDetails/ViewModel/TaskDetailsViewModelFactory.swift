import Foundation
import BigInt

protocol TaskDetailsViewModelFactoryProtocol {
    func createViewModel(from campaign: SCCampaign, task: SCTask, account: DbAccount) -> TaskDetailsViewModel
}

final class TaskDetailsViewModelFactory: TaskDetailsViewModelFactoryProtocol {
    private lazy var amountFormatter = NumberFormatter.amount

    private func createTaskState(
        from campaign: SCCampaign,
        task: SCTask,
        account: DbAccount
    ) -> TaskDetailsViewModel.TaskState {
        if task.negotiation == .negotiationRejected {
            return .declined
        } else if task.negotiation != .negotiationApproved {
            return .needsApproval
        }

        switch task.status {
        case .taskNotAccepted:
            return .taskNotAccepted
        case .taskAccepted:
            return .taskAccepted
        case .taskReadyToPerform:
            return .taskReadyToPerform
        case .gasRequested:
            return .gasRequested
        case .performing:
            return .performing
        case .taskCompleted:
            if let paymentOrder = campaign.paymentOrders.first(where: { $0.taskId == task.taskId }),
               paymentOrder.paymentStatus == .paymentCompleted {
                return .waitingConfirmation
            } else {
                return .waitingForPayment
            }
        case .confirmed:
            if let paymentOrder = campaign.paymentOrders.first(where: { $0.taskId == task.taskId }),
               paymentOrder.paymentStatus == .paymentCompleted {
                return .taskCompleted
            } else {
                return .waitingForPayment
            }
        }
    }

    private func createGasState(
        from campaign: SCCampaign,
        task: SCTask,
        account: DbAccount
    ) -> TaskDetailsViewModel.GasState {
        if task.suppliedGas > 0 {
            return .requested(value: R.string.localizable.commonFuelFormat(String(task.totalGas)))
        } else {
            return .estimated(value: R.string.localizable.commonFuelFormat(String(task.expectedGas)))
        }
    }

    private func createPaymentState(
        from campaign: SCCampaign,
        task: SCTask,
        account: DbAccount
    ) -> TaskDetailsViewModel.PaymentState {
        if let paymentOrder = campaign.paymentOrders.first(where: { $0.taskId == task.taskId }) {
            let amount = Decimal(string: String(paymentOrder.amount)) ?? 0
            let amountString = "₽" + (amountFormatter.stringFromDecimal(amount) ?? "")
            if paymentOrder.paymentStatus == .paymentCompleted {
                return .paid(amount: amountString)
            } else {
                return .waiting(amount: amountString)
            }
        } else {
            return .notSet
        }
    }

    private func createAction(
        from campaign: SCCampaign,
        task: SCTask,
        account: DbAccount
    ) -> TaskDetailsViewModel.Action? {
        if task.negotiation == .negotiationRejected {
            return nil
        } else if task.negotiation != .negotiationApproved {
            return campaign.agreement.supplier.personId == account.accountId?.toBigEndian() ? .approve : nil
        }

        if
            let paymentOrder = campaign.paymentOrders.first(where: { $0.taskId == task.taskId }),
            paymentOrder.paymentStatus == .waitingForPayment,
            campaign.agreement.details.bankId == account.accountId?.toBigEndian() {
            return .confirmPayment
        }

        switch task.status {
        case .taskNotAccepted:
            return task.worker.personId == account.accountId?.toBigEndian() ? .accept : nil
        case .taskAccepted:
            return task.worker.personId == account.accountId?.toBigEndian() ? .readyToPerform : nil
        case .taskReadyToPerform:
            return task.captain.personId == account.accountId?.toBigEndian() ? .requestGas : nil
        case .gasRequested:
            return task.worker.personId == account.accountId?.toBigEndian() ? .performTask : nil
        case .performing:
            return task.worker.personId == account.accountId?.toBigEndian() ? .completeTask : nil
        case .taskCompleted:
            return task.captain.personId == account.accountId?.toBigEndian() ? .confirmTask : nil
        case .confirmed:
            return nil
        }
    }

    func createViewModel(from campaign: SCCampaign, task: SCTask, account: DbAccount) -> TaskDetailsViewModel {
        let taskState = createTaskState(from: campaign, task: task, account: account)

        let gasState = createGasState(from: campaign, task: task, account: account)

        let paymentState = createPaymentState(from: campaign, task: task, account: account)

        let action = createAction(from: campaign, task: task, account: account)

        let taskId = R.string.localizable.taskNumberFormat(String(task.taskId))

        return TaskDetailsViewModel(
            taskId: taskId,
            captain: task.captain.name,
            worker: task.worker.name,
            gasState: gasState,
            suppliedGas: task.suppliedGas > 0 ? R.string.localizable.commonFuelFormat(String(task.suppliedGas)) : nil,
            taskState: taskState,
            paymentState: paymentState,
            action: action
        )
    }

}
