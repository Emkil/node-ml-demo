import UIKit

final class ContractDetailsViewController: UIViewController, ViewHolder {
    enum Section: UInt8, CaseIterable {
        case main
        case actions

        var title: String {
            switch self {
            case .main:
                return R.string.localizable.contractDetailsMainTitle()
            case .actions:
                return R.string.localizable.contractDetailsActionsTitle()
            }
        }
    }

    enum MainRow: UInt8, CaseIterable {
        case status
        case customer
        case supplier
        case agreement
        case bank

        var title: String {
            switch self {
            case .status:
                return R.string.localizable.contractDetailsMainStatus()
            case .customer:
                return R.string.localizable.contractDetailsMainCustomer()
            case .supplier:
                return R.string.localizable.contractDetailsMainSupplier()
            case .agreement:
                return R.string.localizable.contractDetailsMainAgreement()
            case .bank:
                return R.string.localizable.contractDetailsMainBank()
            }
        }

        func value(from viewModel: ContractDetailsViewModel) -> String {
            switch self {
            case .status:
                return viewModel.status
            case .customer:
                return viewModel.customer
            case .supplier:
                return viewModel.supplier
            case .agreement:
                return viewModel.agreement
            case .bank:
                return viewModel.bank
            }
        }
    }

    enum ActionsRow: UInt8, CaseIterable {
        case priceChanges
        case tasks

        var title: String {
            switch self {
            case .tasks:
                return R.string.localizable.contractDetailsActionTasks()
            case .priceChanges:
                return R.string.localizable.contractDetailsActionChangePrice()
            }
        }
    }

    typealias RootViewType = ContractDetailsViewLayout

    let presenter: ContractDetailsPresenterProtocol

    private var shouldNotifyOnAppearance: Bool = false

    private var viewModel: ContractDetailsViewModel?

    init(presenter: ContractDetailsPresenterProtocol) {
        self.presenter = presenter
        super.init(nibName: nil, bundle: nil)
    }

    @available(*, unavailable)
    required init?(coder _: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }

    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)

        if shouldNotifyOnAppearance {
            presenter.refresh()
        } else {
            shouldNotifyOnAppearance = true
        }
    }

    override func loadView() {
        view = ContractDetailsViewLayout()
    }

    override func viewDidLoad() {
        super.viewDidLoad()

        setupTableView()
        setupNavigationBar()

        presenter.setup()
    }

    private func setupTableView() {
        let refreshControl = UIRefreshControl()
        rootView.tableView.refreshControl = refreshControl
        refreshControl.addTarget(self, action: #selector(actionRefresh), for: .valueChanged)

        rootView.tableView.registerClassForCell(TitleValueTableViewCell.self)
        rootView.tableView.registerClassForCell(ActionTableViewCell.self)

        rootView.tableView.dataSource = self
        rootView.tableView.delegate = self
        rootView.tableView.rowHeight = 48.0
    }

    private func setupNavigationBar() {
        let shareBarItem = UIBarButtonItem(
            barButtonSystemItem: .action,
            target: self,
            action: #selector(actionShare)
        )

        navigationItem.rightBarButtonItem = shareBarItem
    }

    private func updateActionBar() {
        guard let viewModel = viewModel else {
            return
        }

        if viewModel.state == .waitingCurrent {
            rootView.setupActionBar()

            rootView.actionButtonItem?.target = self
            rootView.actionButtonItem?.action = #selector(actionApprove)
        } else {
            rootView.clearActionBar()
        }
    }

    @objc private func actionRefresh() {
        presenter.refresh()
    }

    @objc private func actionApprove() {
        presenter.approveAgreement()
    }

    @objc private func actionShare() {
        presenter.activateShare()
    }
}

extension ContractDetailsViewController: UITableViewDataSource {
    func numberOfSections(in tableView: UITableView) -> Int {
        guard let viewModel = viewModel else {
            return 0
        }

        if viewModel.state != .declined {
            return Section.allCases.count
        } else {
            return 1
        }
    }

    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        guard let sectionValue = Section(rawValue: UInt8(section)) else {
            return 0
        }

        switch sectionValue {
        case .main:
            return MainRow.allCases.count
        case .actions:
            return ActionsRow.allCases.count
        }
    }

    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        guard let section = Section(rawValue: UInt8(indexPath.section)) else {
            return UITableViewCell()
        }

        switch section {
        case .main:
            return provideMainCell(for: indexPath.row, using: tableView)
        case .actions:
            return provideActionCell(for: indexPath.row, using: tableView)
        }
    }

    func tableView(_ tableView: UITableView, titleForHeaderInSection section: Int) -> String? {
        guard let sectionValue = Section(rawValue: UInt8(section)) else {
            return nil
        }

        return sectionValue.title
    }

    func provideMainCell(for row: Int, using tableView: UITableView) -> UITableViewCell {
        guard let rowValue = MainRow(rawValue: UInt8(row)), let viewModel = viewModel else {
            return UITableViewCell()
        }

        let cell = tableView.dequeueReusableCellWithType(TitleValueTableViewCell.self)!
        let title = rowValue.title
        let value = rowValue.value(from: viewModel)
        cell.bind(title: title, value: value)

        return cell
    }

    func provideActionCell(for row: Int, using tableView: UITableView) -> UITableViewCell {
        guard let rowValue = ActionsRow(rawValue: UInt8(row)) else {
            return UITableViewCell()
        }

        let cell = tableView.dequeueReusableCellWithType(ActionTableViewCell.self)!
        cell.bind(title: rowValue.title)

        return cell
    }
}

extension ContractDetailsViewController: UITableViewDelegate {
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        tableView.deselectRow(at: indexPath, animated: true)

        guard indexPath.section == Section.actions.rawValue else {
            return
        }

        guard let actionRow = ActionsRow(rawValue: UInt8(indexPath.row)) else {
            return
        }

        switch actionRow {
        case .tasks:
            presenter.activateTasks()
        case .priceChanges:
            presenter.activatePriceChanges()
        }
    }
}

extension ContractDetailsViewController: ContractDetailsViewProtocol {
    func didReceive(viewModel: ContractDetailsViewModel) {
        self.viewModel = viewModel

        title = viewModel.name

        updateActionBar()

        rootView.tableView.refreshControl?.endRefreshing()
        rootView.tableView.reloadData()
    }
}
