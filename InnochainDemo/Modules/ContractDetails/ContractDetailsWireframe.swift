import Foundation

final class ContractDetailsWireframe: ContractDetailsWireframeProtocol {
    func showPriceChanges(
        from view: ContractDetailsViewProtocol?,
        contractAccountId: AccountId,
        selectedAccountId: AccountId
    ) {
        guard let priceChangeView = PriceChangeViewFactory.createView(
                for: contractAccountId,
                selectedAccountId: selectedAccountId
        ) else {
            return
        }

        view?.controller.navigationController?.pushViewController(priceChangeView.controller, animated: true)
    }

    func showTasks(
        from view: ContractDetailsViewProtocol?,
        contractAccountId: AccountId,
        selectedAccountId: AccountId
    ) {
        guard let tasksView = TasksListViewFactory.createView(
                for: contractAccountId,
                selectedAccountId: selectedAccountId
        ) else {
            return
        }

        view?.controller.navigationController?.pushViewController(tasksView.controller, animated: true)
    }
}
