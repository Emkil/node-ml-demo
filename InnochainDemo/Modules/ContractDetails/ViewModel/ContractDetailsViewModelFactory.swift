import Foundation

protocol ContractDetailsViewModelFactoryProtocol {
    func createViewModel(
        from account: DbAccount,
        localContract: DbContract,
        remoteContract: SCCampaign
    ) -> ContractDetailsViewModel
}

final class ContractDetailsViewModelFactory: ContractDetailsViewModelFactoryProtocol {
    func createViewModel(
        from account: DbAccount,
        localContract: DbContract,
        remoteContract: SCCampaign
    ) -> ContractDetailsViewModel {
        let state: ContractDetailsState = {
            guard
                remoteContract.phase != .phaseDeclined,
                let accountIdData = try? Data(hexString: account.identifier) else {
                return .declined
            }

            let accountId = AccountId(value: accountIdData)
            let agreement = remoteContract.agreement
            let negotiation = agreement.negotiation

            if negotiation == .waitingCustomer, agreement.customer.personId == accountId.toBigEndian() {
                return .waitingCurrent
            }

            if negotiation == .waitingSupplier, agreement.supplier.personId == accountId.toBigEndian() {
                return .waitingCurrent
            }

            if negotiation == .negotiationApproved {
                return .finalized
            }

            return .waitingOther
        }()

        return ContractDetailsViewModel(
            name: localContract.name,
            status: remoteContract.agreement.negotiation.title,
            customer: remoteContract.agreement.customer.name,
            supplier: remoteContract.agreement.supplier.name,
            agreement: remoteContract.agreement.details.text,
            bank: AccountId.fromBigUInt(remoteContract.agreement.details.bankId).value.toHex(includePrefix: true),
            state: state
        )
    }
}
