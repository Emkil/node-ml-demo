import Foundation

enum ContractDetailsState {
    case finalized
    case waitingCurrent
    case waitingOther
    case declined
}

struct ContractDetailsViewModel {
    let name: String
    let status: String
    let customer: String
    let supplier: String
    let agreement: String
    let bank: String
    let state: ContractDetailsState
}
