import UIKit
import IrohaCrypto
import RobinHood

final class ContractDetailsInteractor {
    weak var presenter: ContractDetailsInteractorOutputProtocol!

    let selectedAccountId: AccountId
    let contractAccountId: AccountId
    let nodeOperationFactory: JSONRPCOperationFactoryProtocol
    let transactionService: TransactionServiceProtocol
    let txProvider: StreamableProvider<DbTransaction>
    let signer: IRSignatureCreatorProtocol
    let accountRepository: AnyDataProviderRepository<DbAccount>
    let contractRepository: AnyDataProviderRepository<DbContract>
    let operationQueue: OperationQueue

    init(
        selectedAccountId: AccountId,
        contractAccountId: AccountId,
        nodeOperationFactory: JSONRPCOperationFactoryProtocol,
        transactionService: TransactionServiceProtocol,
        txProvider: StreamableProvider<DbTransaction>,
        signer: IRSignatureCreatorProtocol,
        accountRepository: AnyDataProviderRepository<DbAccount>,
        contractRepository: AnyDataProviderRepository<DbContract>,
        operationQueue: OperationQueue
    ) {
        self.selectedAccountId = selectedAccountId
        self.contractAccountId = contractAccountId
        self.nodeOperationFactory = nodeOperationFactory
        self.transactionService = transactionService
        self.txProvider = txProvider
        self.signer = signer
        self.accountRepository = accountRepository
        self.contractRepository = contractRepository
        self.operationQueue = operationQueue
    }

    private func provideRemoteContract() {
        fetchRemoteContract(
            for: contractAccountId,
            operationFactory: nodeOperationFactory,
            operationQueue: operationQueue
        ) { [weak self] (result) in
            self?.presenter.didReceiveCampaign(result: result)
        }
    }

    private func provideLocalContract() {
        fetchLocalContract(
            for: contractAccountId,
            repository: contractRepository,
            operationQueue: operationQueue
        ) { [weak self] result in
            self?.presenter.didReceiveLocalContract(result: result)
        }
    }

    private func submitAgreementApproval(_ contractId: AccountId, account: Account) {
        do {
            let transaction = try TransactionBuilder()
                .from(selectedAccountId)
                .nonce(account.nonce)
                .approveAgreement(contractId)
                .sign(using: [signer])

            let hash = try transaction.transactionHash().toHex(includePrefix: true)

            presenter.willSubmitTransaction(for: hash)

            transactionService.submit(transaction: transaction, runningIn: .main) { _ in }
        } catch {
            presenter.didReceiveTransaction(result: .failure(error), for: nil)
        }
    }
}

extension ContractDetailsInteractor: ContractDetailsInteractorInputProtocol {
    func setup() {
        fetchLocalAccount(
            for: selectedAccountId,
            repository: accountRepository,
            operationQueue: operationQueue
        ) { [weak self] (result) in
            self?.presenter.didReceiveAccount(result: result)
        }

        provideLocalContract()

        subscribeToTransactions(using: txProvider)
    }

    func refresh() {
        provideRemoteContract()
    }

    func approveAgreement(for contractId: AccountId) {
        fetchRemoteAccount(
            for: selectedAccountId,
            operationFactory: nodeOperationFactory,
            operationQueue: operationQueue) { [weak self] result in
            switch result {
            case let .success(account):
                self?.submitAgreementApproval(contractId, account: account)
            case let .failure(error):
                self?.presenter.didReceiveTransaction(result: .failure(error), for: nil)
            }
        }
    }
}

extension ContractDetailsInteractor: RemoteAccountFetching, LocalAccountFetching,
                                     RemoteContractFetching, LocalContractFetching, TxSubscribing {
    func handleTransactions(result: Result<[DbTransaction], Error>) {
        provideRemoteContract()

        switch result {
        case let .success(transactions):
            for transaction in transactions {
                switch transaction.status {
                case .completed:
                    presenter.didReceiveTransaction(result: .success(true), for: transaction.identifier)
                case .failed:
                    presenter.didReceiveTransaction(result: .success(false), for: transaction.identifier)
                case .pending:
                    presenter.didSubmitTransaction(for: transaction.identifier)
                }
            }
        case let .failure(error):
            presenter.didReceiveTransaction(result: .failure(error), for: nil)
        }
    }
}
