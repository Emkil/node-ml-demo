import UIKit

final class ContractDetailsViewLayout: UIView {

    let tableView: UITableView = {
        let view = UITableView(frame: .zero, style: .grouped)
        view.contentInsetAdjustmentBehavior = .always
        return view
    }()

    private(set) var toolbar: UIToolbar?
    private(set) var actionButtonItem: UIBarButtonItem?

    override init(frame: CGRect) {
        super.init(frame: frame)

        backgroundColor = .white
        setupLayout()
    }

    @available(*, unavailable)
    required init?(coder _: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }

    private func setupLayout() {
        addSubview(tableView)

        tableView.snp.makeConstraints { make in
            make.leading.trailing.equalToSuperview()
            make.top.bottom.equalToSuperview()
        }
    }

    func setupActionBar() {
        guard toolbar == nil else {
            return
        }

        let toolbar = UIToolbar()

        addSubview(toolbar)

        toolbar.snp.makeConstraints { make in
            make.leading.trailing.equalToSuperview()
            make.bottom.equalTo(safeAreaLayoutGuide)
            make.height.equalTo(44.0)
        }

        let actionButtonItem = UIBarButtonItem(
            title: R.string.localizable.commonConfirm(),
            style: .plain,
            target: nil,
            action: nil
        )

        let spacing = UIBarButtonItem(barButtonSystemItem: .flexibleSpace, target: nil, action: nil)

        toolbar.items = [spacing, actionButtonItem]

        self.toolbar = toolbar
        self.actionButtonItem = actionButtonItem

        tableView.contentInset = UIEdgeInsets(top: 0, left: 0, bottom: 44, right: 0)
    }

    func clearActionBar() {
        toolbar?.removeFromSuperview()

        toolbar = nil
        actionButtonItem = nil

        tableView.contentInset = .zero
    }
}
