import UIKit

final class ContractDetailsPresenter {
    weak var view: ContractDetailsViewProtocol?
    let wireframe: ContractDetailsWireframeProtocol
    let interactor: ContractDetailsInteractorInputProtocol
    let viewModelFactory: ContractDetailsViewModelFactoryProtocol
    let logger: LoggerProtocol?

    private var account: DbAccount?
    private var campaign: SCCampaign?
    private var localContract: DbContract?

    init(
        interactor: ContractDetailsInteractorInputProtocol,
        wireframe: ContractDetailsWireframeProtocol,
        viewModelFactory: ContractDetailsViewModelFactoryProtocol,
        logger: LoggerProtocol? = nil
    ) {
        self.interactor = interactor
        self.wireframe = wireframe
        self.viewModelFactory = viewModelFactory
        self.logger = logger
    }

    private func provideViewModel() {
        guard
            let account = account,
            let campaign = campaign,
            let contract = localContract else {
            return
        }

        let viewModel = viewModelFactory.createViewModel(
            from: account,
            localContract: contract,
            remoteContract: campaign
        )

        view?.didReceive(viewModel: viewModel)
    }
}

extension ContractDetailsPresenter: ContractDetailsPresenterProtocol {
    func setup() {
        interactor.setup()
    }

    func refresh() {
        interactor.refresh()
    }

    func approveAgreement() {
        do {
            guard let contract = localContract else {
                return
            }

            let contractId = AccountId(value: try Data(hexString: contract.identifier))

            interactor.approveAgreement(for: contractId)
        } catch {
            logger?.error("Approve error: \(error)")
        }
    }

    func activatePriceChanges() {
        guard let account = account, let contract = localContract else {
            return
        }

        do {
            let selectedAccountId = try Data(hexString: account.identifier)
            let contractAccountId = try Data(hexString: contract.identifier)

            wireframe.showPriceChanges(
                from: view,
                contractAccountId: AccountId(value: contractAccountId),
                selectedAccountId: AccountId(value: selectedAccountId)
            )
        } catch {
            logger?.error("Did receive error \(error)")
        }
    }

    func activateTasks() {
        guard
            let account = account,
            let contract = localContract,
            let accountId = try? AccountId.fromHex(string: account.identifier),
            let contractId = try? AccountId.fromHex(string: contract.identifier) else {
            return
        }

        wireframe.showTasks(
            from: view,
            contractAccountId: contractId,
            selectedAccountId: accountId
        )
    }

    func activatePayments() {

    }

    func activateShare() {
        guard let contract = localContract else {
            return
        }

        let copyAction = AlertPresentableAction(title: R.string.localizable.commonCopyId()) {
            UIPasteboard.general.string = contract.identifier
        }

        let alertViewModel = AlertPresentableViewModel(
            title: R.string.localizable.commonChooseAction(),
            message: "",
            actions: [copyAction],
            closeAction: R.string.localizable.commonCancel()
        )

        wireframe.present(viewModel: alertViewModel, style: .actionSheet, from: view)
    }
}

extension ContractDetailsPresenter: ContractDetailsInteractorOutputProtocol {
    func didReceiveAccount(result: Result<DbAccount?, Error>) {
        switch result {
        case .success(let account):
            self.account = account
            provideViewModel()
        case .failure(let error):
            logger?.error("Did receive account error: \(error)")
        }
    }

    func didReceiveCampaign(result: Result<SCCampaign, Error>) {
        switch result {
        case .success(let campaign):
            self.campaign = campaign
            provideViewModel()
        case .failure(let error):
            logger?.error("Did receive remote contract error: \(error)")
        }
    }

    func didReceiveLocalContract(result: Result<DbContract?, Error>) {
        switch result {
        case .success(let localContract):
            self.localContract = localContract
            provideViewModel()
        case .failure(let error):
            logger?.error("Did receive local contract error: \(error)")
        }
    }

    func willSubmitTransaction(for hash: String) {
        view?.didStartLoading()

        logger?.debug("Will submit transaction: \(hash)")
    }

    func didSubmitTransaction(for hash: String) {
        logger?.debug("Did submit transaction: \(hash)")
    }

    func didReceiveTransaction(result: Result<Bool, Error>, for hash: String?) {
        view?.didStopLoading()

        switch result {
        case let .success(value):
            logger?.debug("Transaction result \(hash ?? ""): \(value)")
        case let .failure(error):
            logger?.error("Transaction submition \(hash ?? "") error: \(error)")
        }
    }
}
