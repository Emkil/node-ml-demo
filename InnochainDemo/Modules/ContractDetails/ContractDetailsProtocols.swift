protocol ContractDetailsViewProtocol: ControllerBackedProtocol, LoadableViewProtocol {
    func didReceive(viewModel: ContractDetailsViewModel)
}

protocol ContractDetailsPresenterProtocol: class {
    func setup()
    func refresh()
    func approveAgreement()
    func activatePriceChanges()
    func activateTasks()
    func activateShare()
}

protocol ContractDetailsInteractorInputProtocol: class {
    func setup()
    func refresh()
    func approveAgreement(for contractId: AccountId)
}

protocol ContractDetailsInteractorOutputProtocol: class {
    func didReceiveAccount(result: Result<DbAccount?, Error>)
    func didReceiveCampaign(result: Result<SCCampaign, Error>)
    func didReceiveLocalContract(result: Result<DbContract?, Error>)

    func willSubmitTransaction(for hash: String)
    func didSubmitTransaction(for hash: String)
    func didReceiveTransaction(result: Result<Bool, Error>, for hash: String?)
}

protocol ContractDetailsWireframeProtocol: AlertPresentable {
    func showPriceChanges(
        from view: ContractDetailsViewProtocol?,
        contractAccountId: AccountId,
        selectedAccountId: AccountId
    )

    func showTasks(
        from view: ContractDetailsViewProtocol?,
        contractAccountId: AccountId,
        selectedAccountId: AccountId
    )
}
