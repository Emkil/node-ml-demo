import UIKit
import SoraFoundation

final class ContractListViewController: UIViewController, ViewHolder {
    typealias RootViewType = ContractListViewLayout

    let presenter: ContractListPresenterProtocol

    private var viewModels: [ContractListViewModel] = []

    init(presenter: ContractListPresenterProtocol) {
        self.presenter = presenter
        super.init(nibName: nil, bundle: nil)
    }

    @available(*, unavailable)
    required init?(coder _: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }

    override func loadView() {
        view = ContractListViewLayout()
    }

    override func viewDidLoad() {
        super.viewDidLoad()

        configureNavigationItem()
        configureTableView()
        setupLocalization()
        presenter.setup()
    }

    private func configureNavigationItem() {
        let addBarItem = UIBarButtonItem(
            barButtonSystemItem: .add,
            target: self,
            action: #selector(actionCreateContract)
        )

        navigationItem.rightBarButtonItem = addBarItem
    }

    private func configureTableView() {
        rootView.tableView.registerClassForCell(ContractListTableViewCell.self)
        rootView.tableView.delegate = self
        rootView.tableView.dataSource = self
    }

    private func setupLocalization() {
        title = R.string.localizable.contractListTitle()
    }

    @objc private func actionCreateContract() {
        presenter.createContract()
    }
}

extension ContractListViewController: UITableViewDataSource {
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        viewModels.count
    }

    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCellWithType(ContractListTableViewCell.self)!
        cell.bind(viewModel: viewModels[indexPath.row])
        return cell
    }
}

extension ContractListViewController: UITableViewDelegate {
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        tableView.deselectRow(at: indexPath, animated: true)

        presenter.select(viewModel: viewModels[indexPath.row])
    }
}

extension ContractListViewController: ContractListViewProtocol {
    func didReceive(viewModels: [ContractListViewModel]) {
        self.viewModels = viewModels
        rootView.tableView.reloadData()
    }
}
