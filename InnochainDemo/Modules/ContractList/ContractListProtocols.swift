protocol ContractListViewProtocol: ControllerBackedProtocol {
    func didReceive(viewModels: [ContractListViewModel])
}

protocol ContractListPresenterProtocol: class {
    func setup()
    func createContract()
    func select(viewModel: ContractListViewModel)
}

protocol ContractListInteractorInputProtocol: class {
    func setup()
    func createContract(for name: String, contractData: ContractInit)
    func addContract(for name: String, accountId: AccountId)
}

protocol ContractListInteractorOutputProtocol: class {
    func didReceiveAccount(result: Result<DbAccount?, Error>)
    func didReceive(existingContracts: [DbContract], pendingContracts: [DbContract])
    func didReceiveContractCreation(result: Result<Void, Error>)
}

protocol ContractListWireframeProtocol: AlertPresentable {
    func showNewContractForm(
        from view: ContractListViewProtocol?,
        customer: DbAccount?,
        delegate: FormInputDelegate,
        context: Any
    )

    func showExistingContractForm(
        from view: ContractListViewProtocol?,
        delegate: FormInputDelegate,
        context: Any
    )

    func showContractDetails(
        from view: ContractListViewProtocol?,
        selectedAccountId: AccountId,
        contractAccountId: AccountId
    )
}
