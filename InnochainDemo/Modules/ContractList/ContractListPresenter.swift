import Foundation
import SoraFoundation

final class ContractListPresenter {
    enum ContractAddType {
        case create
        case existing
    }

    weak var view: ContractListViewProtocol?
    let wireframe: ContractListWireframeProtocol
    let interactor: ContractListInteractorInputProtocol
    let viewModelFactory: ContractListViewModelFactoryProtocol
    let logger: LoggerProtocol?

    private var account: DbAccount?

    init(
        interactor: ContractListInteractorInputProtocol,
        wireframe: ContractListWireframeProtocol,
        viewModelFactory: ContractListViewModelFactoryProtocol,
        logger: LoggerProtocol? = nil
    ) {
        self.interactor = interactor
        self.wireframe = wireframe
        self.viewModelFactory = viewModelFactory
        self.logger = logger
    }

    func provideViewModel(from existingContracts: [DbContract], pendingContracts: [DbContract]) {
        let viewModels = viewModelFactory.createViewModelList(
            from: existingContracts,
            pendingContracts: pendingContracts
        )

        view?.didReceive(viewModels: viewModels)
    }

    private func newContract(from inputViewModels: [InputViewModelProtocol]) {
        do {
            let customerId = AccountId(
                value: try Data(hexString: inputViewModels[1].inputHandler.value)
            )

            let supplierId = AccountId(
                value: try Data(hexString: inputViewModels[3].inputHandler.value)
            )

            let bankId = AccountId(
                value: try Data(hexString: inputViewModels[6].inputHandler.value)
            )

            let contractData = ContractInit(
                customerAccoundId: customerId,
                customerName: inputViewModels[2].inputHandler.value,
                supplierAccoundId: supplierId,
                supplierName: inputViewModels[4].inputHandler.value,
                agreement: inputViewModels[5].inputHandler.value,
                bankAccountId: bankId
            )

            interactor.createContract(
                for: inputViewModels[0].inputHandler.value,
                contractData: contractData
            )
        } catch {
            logger?.error("Contract init failed: \(error)")
        }
    }

    private func addExistingContract(from inputViewModels: [InputViewModelProtocol]) {
        do {
            let contractId = AccountId(
                value: try Data(hexString: inputViewModels[1].inputHandler.value)
            )

            interactor.addContract(for: inputViewModels[0].inputHandler.value, accountId: contractId)
        } catch {
            logger?.error("Contract init failed: \(error)")
        }
    }
}

extension ContractListPresenter: ContractListPresenterProtocol {
    func setup() {
        interactor.setup()
    }

    func createContract() {
        guard let account = account else {
            return
        }

        let newAction = AlertPresentableAction(title: R.string.localizable.contractCreateNewTitle()) {
            self.wireframe.showNewContractForm(
                from: self.view,
                customer: account,
                delegate: self,
                context: ContractAddType.create
            )
        }

        let addAction = AlertPresentableAction(title: R.string.localizable.contractCreateAddTitle()) {
            self.wireframe.showExistingContractForm(
                from: self.view,
                delegate: self,
                context: ContractAddType.existing
            )
        }

        let alertViewModel = AlertPresentableViewModel(
            title: R.string.localizable.commonChooseAction(),
            message: "",
            actions: [newAction, addAction],
            closeAction: R.string.localizable.commonCancel()
        )

        wireframe.present(viewModel: alertViewModel, style: .actionSheet, from: view)
    }

    func select(viewModel: ContractListViewModel) {
        guard
            case .completed = viewModel.status,
            let account = account,
            let accountId = try? Data(hexString: account.identifier),
            let contractId = try? Data(hexString: viewModel.accountId) else {
            return
        }

        wireframe.showContractDetails(
            from: view,
            selectedAccountId: AccountId(value: accountId),
            contractAccountId: AccountId(value: contractId)
        )
    }
}

extension ContractListPresenter: ContractListInteractorOutputProtocol {
    func didReceiveAccount(result: Result<DbAccount?, Error>) {
        switch result {
        case let .success(account):
            self.account = account
        case let .failure(error):
            logger?.error("Did receive account error: \(error)")
        }
    }

    func didReceive(existingContracts: [DbContract], pendingContracts: [DbContract]) {
        provideViewModel(from: existingContracts, pendingContracts: pendingContracts)
    }

    func didReceiveContractCreation(result: Result<Void, Error>) {
        switch result {
        case .success:
            logger?.debug("Contract successfully created")
        case let .failure(error):
            logger?.error("Contract creation failed: \(error)")
        }
    }
}

extension ContractListPresenter: FormInputDelegate {
    func didReceive(inputViewModels: [InputViewModelProtocol], context: Any?) {
        guard let contractAddType = context as? ContractAddType else {
            return
        }

        switch contractAddType {
        case .create:
            newContract(from: inputViewModels)
        case .existing:
            addExistingContract(from: inputViewModels)
        }
    }
}
