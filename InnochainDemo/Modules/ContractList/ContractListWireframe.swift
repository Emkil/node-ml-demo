import UIKit

final class ContractListWireframe: ContractListWireframeProtocol {
    func showNewContractForm(
        from view: ContractListViewProtocol?,
        customer: DbAccount?,
        delegate: FormInputDelegate,
        context: Any
    ) {
        guard let formView = FormInputViewFactory.createContractCreateView(
                for: customer,
                delegate: delegate,
                context: context
        ) else {
            return
        }

        let navigationController = UINavigationController(rootViewController: formView.controller)
        view?.controller.present(navigationController, animated: true, completion: nil)
    }

    func showExistingContractForm(
        from view: ContractListViewProtocol?,
        delegate: FormInputDelegate,
        context: Any
    ) {
        guard let formView = FormInputViewFactory.createContractAddView(
                delegate: delegate,
                context: context
        ) else {
            return
        }

        let navigationController = UINavigationController(rootViewController: formView.controller)
        view?.controller.present(navigationController, animated: true, completion: nil)
    }

    func showContractDetails(
        from view: ContractListViewProtocol?,
        selectedAccountId: AccountId,
        contractAccountId: AccountId
    ) {
        guard let detailsView = ContractDetailsViewFactory.createView(
                for: contractAccountId,
                selectedAccountId: selectedAccountId
        ) else {
            return
        }

        view?.controller.navigationController?.pushViewController(detailsView.controller, animated: true)
    }
}
