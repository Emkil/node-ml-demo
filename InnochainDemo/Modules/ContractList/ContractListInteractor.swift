import UIKit
import RobinHood
import IrohaCrypto

enum ContractListInteractorError: Error {
    case contractCreationFailed
    case contractInProgress
}

final class ContractListInteractor {
    struct PendingContract {
        let txHash: String?
        let contractId: AccountId
        let contractName: String
        let createdAt: Date

        func byChanging(txHash: String?) -> PendingContract {
            PendingContract(
                txHash: txHash,
                contractId: contractId,
                contractName: contractName,
                createdAt: createdAt
            )
        }
    }

    weak var presenter: ContractListInteractorOutputProtocol!

    let accountId: AccountId
    let transactionService: TransactionServiceProtocol
    let nodeOperationFactory: JSONRPCOperationFactoryProtocol
    let operationQueue: OperationQueue
    let txProvider: StreamableProvider<DbTransaction>
    let contractRepository: AnyDataProviderRepository<DbContract>
    let accountRepository: AnyDataProviderRepository<DbAccount>
    let signer: IRSignatureCreatorProtocol
    let logger: LoggerProtocol?

    private var pendingContract: PendingContract?

    init(
        accountId: AccountId,
        transactionService: TransactionServiceProtocol,
        nodeOperationFactory: JSONRPCOperationFactoryProtocol,
        txProvider: StreamableProvider<DbTransaction>,
        contractRepository: AnyDataProviderRepository<DbContract>,
        accountRepository: AnyDataProviderRepository<DbAccount>,
        signer: IRSignatureCreatorProtocol,
        operationQueue: OperationQueue,
        logger: LoggerProtocol? = nil
    ) {
        self.accountId = accountId
        self.transactionService = transactionService
        self.nodeOperationFactory = nodeOperationFactory
        self.txProvider = txProvider
        self.contractRepository = contractRepository
        self.accountRepository = accountRepository
        self.signer = signer
        self.operationQueue = operationQueue
        self.logger = logger
    }

    private func submitContractCreation(for contractId: AccountId, sender: Account, name: String, data: ContractInit) {
        do {
            let transaction = try TransactionBuilder()
                .from(accountId)
                .nonce(sender.nonce)
                .createContract(contractId, contractInit: data)
                .sign(using: [signer])

            transactionService.submit(transaction: transaction, runningIn: .main) { [weak self] result in
                self?.handleTxSubmittion(result: result)
            }
        } catch {
            failPendingContract(with: error)
        }
    }

    private func handleTxSubmittion(result: Result<String, Error>) {
        guard let pendingContract = pendingContract else {
            return
        }

        switch result {
        case let .success(txHash):
            self.pendingContract = pendingContract.byChanging(txHash: txHash)
        case let .failure(error):
            failPendingContract(with: error)
        }
    }

    func applyPendingContractAndNotify() {
        guard let pendingContract = pendingContract else {
            return
        }

        self.pendingContract = nil

        let dbContract = DbContract(
            identifier: pendingContract.contractId.value.toHex(includePrefix: true),
            participant: accountId.value.toHex(includePrefix: true),
            type: UInt16(ContractType.fuel.rawValue),
            name: pendingContract.contractName,
            createdAt: pendingContract.createdAt
        )

        let saveContractOperation = contractRepository.saveOperation({
            return [dbContract]
        }, { [] })

        saveContractOperation.completionBlock = { [weak self] in
            DispatchQueue.main.async {
                do {
                    try saveContractOperation.extractNoCancellableResultData()
                    self?.presenter.didReceiveContractCreation(result: .success(()))
                    self?.provideContracts()
                } catch {
                    self?.presenter.didReceiveContractCreation(result: .failure(error))
                }
            }
        }

        operationQueue.addOperations([saveContractOperation], waitUntilFinished: false)
    }

    func failPendingContract(with error: Error) {
        self.pendingContract = nil

        presenter.didReceiveContractCreation(result: .failure(error))
    }

    private func checkContractPendingCompletion(using transaction: DbTransaction) {
        switch transaction.status {
        case .completed:
            applyPendingContractAndNotify()
        case .failed:
            failPendingContract(with: ContractListInteractorError.contractCreationFailed)
        case .pending:
            break
        }
    }

    private func provideContracts() {
        let allContractsOperation = contractRepository.fetchAllOperation(with: RepositoryFetchOptions())
        allContractsOperation.completionBlock = { [weak self] in
            DispatchQueue.main.async {
                do {
                    let contracts = try allContractsOperation.extractNoCancellableResultData()

                    let pendingContracts: [DbContract] = self?.pendingContract.map { pending in
                        guard let accountId = self?.accountId.value.toHex(includePrefix: true) else {
                            return []
                        }

                        let contract = DbContract(
                            identifier: pending.contractId.value.toHex(includePrefix: true),
                            participant: accountId,
                            type: UInt16(ContractType.fuel.rawValue),
                            name: pending.contractName,
                            createdAt: pending.createdAt
                        )

                        return [
                            contract
                        ]
                    } ?? []
                    self?.presenter.didReceive(existingContracts: contracts, pendingContracts: pendingContracts)
                } catch {
                    self?.logger?.error("Account fetching failed: \(error)")
                }
            }
        }

        operationQueue.addOperation(allContractsOperation)
    }

    private func provideAccount() {
        fetchLocalAccount(
            for: accountId,
            repository: accountRepository,
            operationQueue: operationQueue
        ) { [weak self] result in
            self?.presenter.didReceiveAccount(result: result)
        }
    }
}

extension ContractListInteractor: ContractListInteractorInputProtocol {
    func setup() {
        provideAccount()
        provideContracts()

        subscribeToTransactions(using: txProvider)
    }

    func createContract(for name: String, contractData: ContractInit) {
        guard pendingContract == nil else {
            presenter.didReceiveContractCreation(result: .failure(ContractListInteractorError.contractInProgress))
            return
        }

        do {
            let contractId = try AccountId.createRandom()

            pendingContract = PendingContract(
                txHash: nil,
                contractId: contractId,
                contractName: name,
                createdAt: Date()
            )

            let call = GetAccountCall(accountId: accountId)

            let accountOperation: BaseOperation<GetAccountResponse> = nodeOperationFactory.callMethodOperation(
                RPCMethod.getAccount,
                params: call
            )

            accountOperation.completionBlock = { [weak self] in
                DispatchQueue.main.async {
                    do {
                        let account = try accountOperation.extractNoCancellableResultData().account
                        self?.submitContractCreation(for: contractId, sender: account, name: name, data: contractData)
                    } catch {
                        self?.failPendingContract(with: error)
                    }
                }
            }

            operationQueue.addOperation(accountOperation)

            provideContracts()
        } catch {
            presenter.didReceiveContractCreation(result: .failure(error))
        }
    }

    func addContract(for name: String, accountId: AccountId) {
        let call = GetAccountCall(accountId: accountId)

        let remoteOperation: BaseOperation<GetAccountResponse> = nodeOperationFactory.callMethodOperation(
            RPCMethod.getAccount,
            params: call
        )

        let saveOperation = contractRepository.saveOperation({
            let account = try remoteOperation.extractNoCancellableResultData().account

            return [
                DbContract(
                    identifier: account.accountId.value.toHex(includePrefix: true),
                    participant: self.accountId.value.toHex(includePrefix: true),
                    type: UInt16(ContractType.fuel.rawValue),
                    name: name,
                    createdAt: Date()
                )
            ]
        }, { [] })

        saveOperation.addDependency(remoteOperation)

        saveOperation.completionBlock = { [weak self] in
            DispatchQueue.main.async {
                self?.provideContracts()
            }
        }

        operationQueue.addOperations([remoteOperation, saveOperation], waitUntilFinished: false)
    }
}

extension ContractListInteractor: LocalAccountFetching, LocalContractFetching,
                                  RemoteAccountFetching, TxSubscribing {
    func handleTransactions(result: Result<[DbTransaction], Error>) {
        guard let pendingContract = pendingContract else {
            return
        }

        switch result {
        case let .success(transactions):
            for transaction in transactions where pendingContract.txHash == transaction.identifier {
                checkContractPendingCompletion(using: transaction)
            }
        case let .failure(error):
            failPendingContract(with: error)
        }
    }
}
