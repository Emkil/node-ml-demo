import Foundation
import RobinHood
import IrohaCrypto
import SoraKeystore
import SoraFoundation

struct ContractListViewFactory {
    static func createView(for accountId: AccountId) -> ContractListViewProtocol? {
        let interactor = createInteractor(for: accountId)
        let wireframe = ContractListWireframe()

        let viewModelFactory = ContractListViewModelFactory()
        let presenter = ContractListPresenter(
            interactor: interactor,
            wireframe: wireframe,
            viewModelFactory: viewModelFactory,
            logger: Logger.shared
        )

        let view = ContractListViewController(presenter: presenter)

        presenter.view = view
        interactor.presenter = presenter

        return view
    }

    private static func createInteractor(for accountId: AccountId) -> ContractListInteractor {
        let storageFacade = UserDataStorageFacade.shared

        let accountRepository: CoreDataRepository<DbAccount, CDAccountItem> = storageFacade.createRepository()

        let contractRepository: CoreDataRepository<DbContract, CDContract> = storageFacade.createRepository(
            filter: NSPredicate.contracts(for: accountId)
        )

        let txRepository: CoreDataRepository<DbTransaction, CDTransactionItem> =
            UserDataStorageFacade.shared.createRepository()

        let transactionService = TransactionService(
            operationFactory: NodeOperationFactory(url: PrivateUrl.main),
            repository: AnyDataProviderRepository(txRepository),
            operationQueue: OperationQueueFacade.defaultQueue
        )

        let txProviderFactory = StreamableProviderFactory(
            storageFacade: UserDataStorageFacade.shared,
            logger: Logger.shared
        )

        let signer = SigningWrapper(keystore: Keychain(), tag: KeystoreTag.secretKeyTagForAddress(accountId))

        return ContractListInteractor(
            accountId: accountId,
            transactionService: transactionService,
            nodeOperationFactory: NodeOperationFactory(url: PrivateUrl.main),
            txProvider: txProviderFactory.createAllTransactionProvider(),
            contractRepository: AnyDataProviderRepository(contractRepository),
            accountRepository: AnyDataProviderRepository(accountRepository),
            signer: signer,
            operationQueue: OperationQueueFacade.defaultQueue
        )
    }
}
