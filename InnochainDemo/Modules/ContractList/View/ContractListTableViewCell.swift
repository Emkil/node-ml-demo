import UIKit

final class ContractListTableViewCell: UITableViewCell {
    let nameLabel: UILabel = {
        let label = UILabel()
        label.textColor = .black
        label.font = UIFont.systemFont(ofSize: 16)
        return label
    }()

    let accountLabel: UILabel = {
        let label = UILabel()
        label.textColor = .lightGray
        label.font = UIFont.systemFont(ofSize: 14)
        return label
    }()

    let dateLabel: UILabel = {
        let label = UILabel()
        label.textColor = .lightGray
        label.font = UIFont.systemFont(ofSize: 14)
        return label
    }()

    override init(style: UITableViewCell.CellStyle, reuseIdentifier: String?) {
        super.init(style: style, reuseIdentifier: reuseIdentifier)
        setupLayout()
    }

    required init?(coder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }

    func setupLayout() {
        contentView.addSubview(nameLabel)
        nameLabel.snp.makeConstraints { make in
            make.leading.equalToSuperview().inset(16)
            make.top.equalToSuperview().inset(8.0)
        }

        contentView.addSubview(accountLabel)
        accountLabel.snp.makeConstraints { make in
            make.leading.equalToSuperview().inset(16)
            make.top.equalTo(nameLabel.snp.bottom).offset(4.0)
            make.bottom.equalToSuperview().inset(8.0)
        }

        contentView.addSubview(dateLabel)
        dateLabel.snp.makeConstraints { make in
            make.centerY.equalToSuperview()
            make.trailing.equalToSuperview().inset(-16.0)
            make.leading.greaterThanOrEqualTo(nameLabel.snp.trailing).offset(8.0)
            make.leading.greaterThanOrEqualTo(accountLabel.snp.trailing).offset(8.0)
        }
    }

    func bind(viewModel: ContractListViewModel) {
        nameLabel.text = viewModel.name
        accountLabel.text = viewModel.accountId

        switch viewModel.status {
        case .pending:
            nameLabel.textColor = UIColor.black.withAlphaComponent(0.5)
            dateLabel.text = ""

            accessoryType = .none
            let activityIndicator = UIActivityIndicatorView()
            activityIndicator.style = .gray
            activityIndicator.startAnimating()
            activityIndicator.sizeToFit()

            self.accessoryView = activityIndicator

            selectionStyle = .none
        case let .completed(details):
            nameLabel.textColor = UIColor.black
            dateLabel.text = details
            accessoryView = nil
            accessoryType = .disclosureIndicator

            selectionStyle = .default
        }
    }
}
