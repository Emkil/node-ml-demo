import Foundation

protocol ContractListViewModelFactoryProtocol {
    func createViewModelList(
        from existingContracts: [DbContract],
        pendingContracts: [DbContract]
    ) -> [ContractListViewModel]
}

final class ContractListViewModelFactory {}

extension ContractListViewModelFactory: ContractListViewModelFactoryProtocol {
    func createViewModelList(
        from existingContracts: [DbContract],
        pendingContracts: [DbContract]
    ) -> [ContractListViewModel] {
        let sortClosure: (DbContract, DbContract) -> Bool = { $0.createdAt > $1.createdAt}

        let createdViewModels = existingContracts.sorted(by: sortClosure).map { contract in
            ContractListViewModel(
                name: contract.name,
                accountId: contract.identifier,
                status: .completed(details: "")
            )
        }

        let pendingViewModels = pendingContracts.sorted(by: sortClosure).map { contract in
            ContractListViewModel(
                name: contract.name,
                accountId: contract.identifier,
                status: .pending
            )
        }

        return pendingViewModels + createdViewModels
    }
}
