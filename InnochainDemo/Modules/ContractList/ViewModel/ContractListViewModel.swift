import Foundation

struct ContractListViewModel {
    enum Status {
        case pending
        case completed(details: String)
    }

    let name: String
    let accountId: String
    let status: Status
}
