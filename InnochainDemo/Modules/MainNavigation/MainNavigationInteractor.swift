import UIKit

final class MainNavigationInteractor {
    weak var presenter: MainNavigationInteractorOutputProtocol!

    let services: [UserApplicationServiceProtocol]

    deinit {
        services.forEach { $0.throttle() }
    }

    init(services: [UserApplicationServiceProtocol]) {
        self.services = services
    }
}

extension MainNavigationInteractor: MainNavigationInteractorInputProtocol {
    func setup() {
        services.forEach { $0.setup() }
    }
}
