import Foundation

struct MainNavigationViewFactory {
    static func createView() -> MainNavigationViewProtocol? {
        guard let rootView = AccountListViewFactory.createView() else {
            return nil
        }

        let services = PollingServiceFactory().createServices()

        let interactor = MainNavigationInteractor(services: services)
        let wireframe = MainNavigationWireframe()

        let presenter = MainNavigationPresenter(interactor: interactor, wireframe: wireframe)

        let view = MainNavigationViewController(rootViewController: rootView.controller, presenter: presenter)

        presenter.view = view
        interactor.presenter = presenter

        return view
    }
}
