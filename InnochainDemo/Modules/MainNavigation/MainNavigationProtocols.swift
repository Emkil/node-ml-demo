protocol MainNavigationViewProtocol: ControllerBackedProtocol {}

protocol MainNavigationPresenterProtocol: class {
    func setup()
}

protocol MainNavigationInteractorInputProtocol: class {
    func setup()
}

protocol MainNavigationInteractorOutputProtocol: class {}

protocol MainNavigationWireframeProtocol: class {}
