import UIKit

final class MainNavigationViewController: UINavigationController {

    let presenter: MainNavigationPresenterProtocol

    init(rootViewController: UIViewController, presenter: MainNavigationPresenterProtocol) {
        self.presenter = presenter
        super.init(nibName: nil, bundle: nil)

        self.viewControllers = [rootViewController]
    }

    @available(*, unavailable)
    required init?(coder _: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }

    override func viewDidLoad() {
        super.viewDidLoad()

        presenter.setup()
    }
}

extension MainNavigationViewController: MainNavigationViewProtocol {}
