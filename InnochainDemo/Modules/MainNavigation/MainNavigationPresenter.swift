import Foundation

final class MainNavigationPresenter {
    weak var view: MainNavigationViewProtocol?
    let wireframe: MainNavigationWireframeProtocol
    let interactor: MainNavigationInteractorInputProtocol

    init(
        interactor: MainNavigationInteractorInputProtocol,
        wireframe: MainNavigationWireframeProtocol
    ) {
        self.interactor = interactor
        self.wireframe = wireframe
    }
}

extension MainNavigationPresenter: MainNavigationPresenterProtocol {
    func setup() {
        interactor.setup()
    }
}

extension MainNavigationPresenter: MainNavigationInteractorOutputProtocol {}
