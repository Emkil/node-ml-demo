import UIKit

final class PriceChangeWireframe: PriceChangeWireframeProtocol {
    func showPriceForm(from view: PriceChangeViewProtocol?, delegate: FormInputDelegate) {
        guard let formView = FormInputViewFactory.createAddPriceView(delegate: delegate, context: nil) else {
            return
        }

        let navigationController = UINavigationController(rootViewController: formView.controller)
        view?.controller.present(navigationController, animated: true, completion: nil)
    }
}
