import UIKit

final class PriceChangeTableViewCell: BaseTitleValueTableViewCell {
    func bind(viewModel: PriceChangeViewModel) {
        titleLabel.text = viewModel.date
        detailsLabel.text = viewModel.amount

        switch viewModel.status {
        case .approved:
            iconView.image = nil
        case .pending:
            iconView.image = R.image.iconPending()?.withRenderingMode(.alwaysTemplate)
            iconView.tintColor = .lightGray
        case .rejected:
            iconView.image = R.image.iconRejected()
        }
    }
}
