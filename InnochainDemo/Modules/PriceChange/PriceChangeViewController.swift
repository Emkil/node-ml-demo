import UIKit

final class PriceChangeViewController: UIViewController, ViewHolder {
    typealias RootViewType = PriceChangeViewLayout

    let presenter: PriceChangePresenterProtocol

    private var viewModels: [PriceChangeViewModel] = []

    init(presenter: PriceChangePresenterProtocol) {
        self.presenter = presenter
        super.init(nibName: nil, bundle: nil)
    }

    @available(*, unavailable)
    required init?(coder _: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }

    override func loadView() {
        view = PriceChangeViewLayout()
    }

    override func viewDidLoad() {
        super.viewDidLoad()

        configureNavigationItem()
        configureTableView()
        setupLocalization()
        presenter.setup()
    }

    private func configureNavigationItem() {
        let addBarItem = UIBarButtonItem(
            barButtonSystemItem: .add,
            target: self,
            action: #selector(actionAddPrice)
        )

        navigationItem.rightBarButtonItem = addBarItem
    }

    private func configureTableView() {
        rootView.tableView.registerClassForCell(PriceChangeTableViewCell.self)
        rootView.tableView.delegate = self
        rootView.tableView.dataSource = self
        rootView.tableView.rowHeight = 48.0
    }

    private func setupLocalization() {
        title = R.string.localizable.contractDetailsActionChangePrice()
    }

    @objc private func actionAddPrice() {
        presenter.addPrice()
    }
}

extension PriceChangeViewController: UITableViewDataSource {
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        viewModels.count
    }

    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCellWithType(PriceChangeTableViewCell.self)!
        cell.bind(viewModel: viewModels[indexPath.row])
        return cell
    }
}

extension PriceChangeViewController: UITableViewDelegate {
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        tableView.deselectRow(at: indexPath, animated: true)

        presenter.select(at: indexPath.row)
    }
}

extension PriceChangeViewController: PriceChangeViewProtocol {
    func didReceive(viewModels: [PriceChangeViewModel]) {
        self.viewModels = viewModels
        rootView.tableView.reloadData()
    }
}
