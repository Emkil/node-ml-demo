import BigInt

protocol PriceChangeViewProtocol: ControllerBackedProtocol, LoadableViewProtocol {
    func didReceive(viewModels: [PriceChangeViewModel])
}

protocol PriceChangePresenterProtocol: class {
    func setup()
    func addPrice()
    func select(at index: Int)
}

protocol PriceChangeInteractorInputProtocol: class {
    func setup()
    func add(price: BigUInt)
    func approvePrice()
}

protocol PriceChangeInteractorOutputProtocol: class {
    func didReceiveCampaign(result: Result<SCCampaign, Error>)
    func didReceiveAccount(result: Result<DbAccount?, Error>)

    func willSubmitTransaction(for hash: String)
    func didSubmitTransaction(for hash: String)
    func didReceiveTransaction(result: Result<Bool, Error>, for hash: String?)
}

protocol PriceChangeWireframeProtocol: AlertPresentable {
    func showPriceForm(from view: PriceChangeViewProtocol?, delegate: FormInputDelegate)
}
