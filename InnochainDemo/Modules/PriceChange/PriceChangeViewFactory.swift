import Foundation
import RobinHood
import SoraKeystore

struct PriceChangeViewFactory {
    static func createView(for contractId: AccountId, selectedAccountId: AccountId) -> PriceChangeViewProtocol? {
        guard let interactor = createInteractor(for: contractId, selectedAccountId: selectedAccountId) else {
            return nil
        }

        let wireframe = PriceChangeWireframe()

        let presenter = PriceChangePresenter(
            interactor: interactor,
            wireframe: wireframe,
            viewModelFactory: PriceChangeViewModelFactory(),
            logger: Logger.shared
        )

        let view = PriceChangeViewController(presenter: presenter)

        presenter.view = view
        interactor.presenter = presenter

        return view
    }

    static private func createInteractor(
        for contractId: AccountId,
        selectedAccountId: AccountId
    ) -> PriceChangeInteractor? {
        let storageFacade = UserDataStorageFacade.shared

        let accountRepository: CoreDataRepository<DbAccount, CDAccountItem> = storageFacade.createRepository()
        let contractRepository: CoreDataRepository<DbContract, CDContract> = storageFacade.createRepository()

        let txRepository: CoreDataRepository<DbTransaction, CDTransactionItem> =
            UserDataStorageFacade.shared.createRepository()

        let transactionService = TransactionService(
            operationFactory: NodeOperationFactory(url: PrivateUrl.main),
            repository: AnyDataProviderRepository(txRepository),
            operationQueue: OperationQueueFacade.defaultQueue
        )

        let txProviderFactory = StreamableProviderFactory(
            storageFacade: UserDataStorageFacade.shared,
            logger: Logger.shared
        )

        let signer = SigningWrapper(
            keystore: Keychain(),
            tag: KeystoreTag.secretKeyTagForAddress(selectedAccountId)
        )

        return PriceChangeInteractor(
            selectedAccountId: selectedAccountId,
            contractAccountId: contractId,
            nodeOperationFactory: NodeOperationFactory(url: PrivateUrl.main),
            transactionService: transactionService,
            txProvider: txProviderFactory.createAllTransactionProvider(),
            signer: signer,
            contractRepository: AnyDataProviderRepository(contractRepository),
            accountRepository: AnyDataProviderRepository(accountRepository),
            operationQueue: OperationQueueFacade.defaultQueue
        )
    }
}
