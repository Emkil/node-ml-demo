import Foundation
import SoraFoundation
import BigInt

final class PriceChangePresenter {
    weak var view: PriceChangeViewProtocol?
    let wireframe: PriceChangeWireframeProtocol
    let interactor: PriceChangeInteractorInputProtocol
    let viewModelFactory: PriceChangeViewModelFactoryProtocol
    let logger: LoggerProtocol?

    private var campaign: SCCampaign?
    private var account: DbAccount?

    init(
        interactor: PriceChangeInteractorInputProtocol,
        wireframe: PriceChangeWireframeProtocol,
        viewModelFactory: PriceChangeViewModelFactoryProtocol,
        logger: LoggerProtocol? = nil
    ) {
        self.interactor = interactor
        self.wireframe = wireframe
        self.viewModelFactory = viewModelFactory
        self.logger = logger
    }

    private func provideViewModel() {
        guard let campaign = campaign else {
            return
        }

        let viewModels = viewModelFactory.createViewModels(from: campaign)
        view?.didReceive(viewModels: viewModels)
    }
}

extension PriceChangePresenter: PriceChangePresenterProtocol {
    func setup() {
        interactor.setup()
    }

    func addPrice() {
        guard let campaign = campaign, let account = account else {
            return
        }

        guard
            campaign.priceChanges.isEmpty ||
                (campaign.priceChanges.last?.negotiation == .some(.negotiationApproved)) else {

            wireframe.present(
                message: R.string.localizable.priceChangeLatestPendingMessage(),
                title: R.string.localizable.commonError(),
                closeAction: R.string.localizable.commonClose(),
                from: view
            )

            return
        }

        guard campaign.agreement.supplier.personId == account.accountId?.toBigEndian() else {
            wireframe.present(
                message: R.string.localizable.priceChangeAddOnlySupplierMessage(),
                title: R.string.localizable.commonError(),
                closeAction: R.string.localizable.commonClose(),
                from: view
            )

            return
        }

        wireframe.showPriceForm(from: view, delegate: self)
    }

    func select(at index: Int) {
        guard let campaign = campaign, let account = account else {
            return
        }

        guard campaign.priceChanges[index].negotiation == .waitingCustomer else {
            return
        }

        guard campaign.agreement.customer.personId == account.accountId?.toBigEndian() else {
            wireframe.present(
                message: R.string.localizable.priceChangeApproveOnlyCustomerMessage(),
                title: R.string.localizable.commonError(),
                closeAction: R.string.localizable.commonClose(),
                from: view
            )

            return
        }

        let confirmAction = AlertPresentableAction(
            title: R.string.localizable.commonConfirm()
        ) { [weak self] in
            self?.interactor.approvePrice()
        }

        let viewModel = AlertPresentableViewModel(
            title: R.string.localizable.commonChooseAction(),
            message: nil,
            actions: [confirmAction],
            closeAction: R.string.localizable.commonCancel()
        )

        wireframe.present(viewModel: viewModel, style: .actionSheet, from: view)
    }
}

extension PriceChangePresenter: PriceChangeInteractorOutputProtocol {
    func didReceiveCampaign(result: Result<SCCampaign, Error>) {
        switch result {
        case .success(let campaign):
            self.campaign = campaign
            provideViewModel()
        case .failure(let error):
            logger?.error("Did receive remote contract error: \(error)")
        }
    }

    func didReceiveAccount(result: Result<DbAccount?, Error>) {
        switch result {
        case .success(let account):
            self.account = account
            provideViewModel()
        case .failure(let error):
            logger?.error("Did receive account error: \(error)")
        }
    }

    func willSubmitTransaction(for hash: String) {
        view?.didStartLoading()

        logger?.debug("Will submit transaction: \(hash)")
    }

    func didSubmitTransaction(for hash: String) {
        logger?.debug("Did submit transaction: \(hash)")
    }

    func didReceiveTransaction(result: Result<Bool, Error>, for hash: String?) {
        view?.didStopLoading()

        switch result {
        case let .success(value):
            logger?.debug("Transaction result \(hash ?? ""): \(value)")
        case let .failure(error):
            logger?.error("Transaction submition \(hash ?? "") error: \(error)")
        }
    }
}

extension PriceChangePresenter: FormInputDelegate {
    func didReceive(inputViewModels: [InputViewModelProtocol], context: Any?) {
        guard let price = BigUInt(inputViewModels[0].inputHandler.value) else {
            return
        }

        interactor.add(price: price)
    }
}
