import Foundation

protocol PriceChangeViewModelFactoryProtocol {
    func createViewModels(from campaign: SCCampaign) -> [PriceChangeViewModel]
}

final class PriceChangeViewModelFactory: PriceChangeViewModelFactoryProtocol {
    private lazy var amountFormatter = NumberFormatter.amount
    private lazy var dateFormatter = DateFormatter.shortDateAndTime

    func createViewModels(from campaign: SCCampaign) -> [PriceChangeViewModel] {
        campaign.priceChanges.compactMap { change in
            guard
                let amount = Decimal(string: String(change.price)) else {
                return nil
            }

            let date = Date(timeIntervalSince1970: TimeInterval(change.startTime))

            let status: PriceChangeViewModel.Status = {
                switch change.negotiation {
                case .negotiationApproved:
                    return .approved
                case .notSet, .waitingCustomer, .waitingSupplier:
                    return .pending
                case .negotiationRejected:
                    return .rejected
                }
            }()

            let amountString = "₽" + (amountFormatter.string(from: amount as NSNumber) ?? "")

            return PriceChangeViewModel(
                date: dateFormatter.string(from: date),
                amount: amountString,
                status: status
            )
        }
    }
}
