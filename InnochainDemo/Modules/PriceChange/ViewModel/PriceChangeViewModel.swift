import Foundation

struct PriceChangeViewModel {
    enum Status {
        case approved
        case pending
        case rejected
    }

    let date: String
    let amount: String
    let status: Status
}
