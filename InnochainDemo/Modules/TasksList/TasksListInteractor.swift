import UIKit
import RobinHood
import IrohaCrypto
import BigInt

final class TasksListInteractor {
    weak var presenter: TasksListInteractorOutputProtocol!

    let selectedAccountId: AccountId
    let contractAccountId: AccountId
    let nodeOperationFactory: JSONRPCOperationFactoryProtocol
    let transactionService: TransactionServiceProtocol
    let txProvider: StreamableProvider<DbTransaction>
    let signer: IRSignatureCreatorProtocol
    let contractRepository: AnyDataProviderRepository<DbContract>
    let accountRepository: AnyDataProviderRepository<DbAccount>
    let operationQueue: OperationQueue

    init(
        selectedAccountId: AccountId,
        contractAccountId: AccountId,
        nodeOperationFactory: JSONRPCOperationFactoryProtocol,
        transactionService: TransactionServiceProtocol,
        txProvider: StreamableProvider<DbTransaction>,
        signer: IRSignatureCreatorProtocol,
        contractRepository: AnyDataProviderRepository<DbContract>,
        accountRepository: AnyDataProviderRepository<DbAccount>,
        operationQueue: OperationQueue
    ) {
        self.selectedAccountId = selectedAccountId
        self.contractAccountId = contractAccountId
        self.nodeOperationFactory = nodeOperationFactory
        self.transactionService = transactionService
        self.txProvider = txProvider
        self.signer = signer
        self.contractRepository = contractRepository
        self.accountRepository = accountRepository
        self.operationQueue = operationQueue
    }

    private func provideLocalContract() {
        fetchLocalContract(
            for: contractAccountId,
            repository: contractRepository,
            operationQueue: operationQueue
        ) { [weak self] (result) in
            self?.presenter.didReceiveLocalContract(result: result)
        }
    }

    private func provideRemoteContract() {
        fetchRemoteContract(
            for: contractAccountId,
            operationFactory: nodeOperationFactory,
            operationQueue: operationQueue
        ) { [weak self] (result) in
            self?.presenter.didReceiveCampaign(result: result)
        }
    }

    private func provideAccount() {
        fetchLocalAccount(
            for: selectedAccountId,
            repository: accountRepository,
            operationQueue: operationQueue
        ) { [weak self] (result) in
            self?.presenter.didReceiveAccount(result: result)
        }
    }

    private func submitNewTask(_ task: TaskInit, account: Account) {
        do {
            let transaction = try TransactionBuilder()
                .from(selectedAccountId)
                .nonce(account.nonce)
                .addTask(taskInit: task, to: contractAccountId)
                .sign(using: [signer])

            let hash = try transaction.transactionHash().toHex(includePrefix: true)

            presenter.willSubmitTransaction(for: hash)

            transactionService.submit(transaction: transaction, runningIn: .main) { _ in }
        } catch {
            presenter.didReceiveTransaction(result: .failure(error), for: nil)
        }
    }
}

extension TasksListInteractor: TasksListInteractorInputProtocol {
    func setup() {
        provideRemoteContract()
        provideAccount()
        provideLocalContract()
        subscribeToTransactions(using: txProvider)
    }

    func add(task: TaskInit) {
        fetchRemoteAccount(
            for: selectedAccountId,
            operationFactory: nodeOperationFactory,
            operationQueue: operationQueue) { [weak self] result in
            switch result {
            case let .success(account):
                self?.submitNewTask(task, account: account)
            case let .failure(error):
                self?.presenter.didReceiveTransaction(result: .failure(error), for: nil)
            }
        }
    }
}

extension TasksListInteractor: LocalAccountFetching, LocalContractFetching, RemoteAccountFetching,
                                 RemoteContractFetching, TxSubscribing {
    func handleTransactions(result: Result<[DbTransaction], Error>) {
        provideRemoteContract()

        switch result {
        case let .success(transactions):
            for transaction in transactions {
                switch transaction.status {
                case .completed:
                    presenter.didReceiveTransaction(result: .success(true), for: transaction.identifier)
                case .failed:
                    presenter.didReceiveTransaction(result: .success(false), for: transaction.identifier)
                case .pending:
                    presenter.didSubmitTransaction(for: transaction.identifier)
                }
            }
        case let .failure(error):
            presenter.didReceiveTransaction(result: .failure(error), for: nil)
        }
    }
}
