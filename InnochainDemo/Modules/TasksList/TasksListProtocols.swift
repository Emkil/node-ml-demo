import BigInt

protocol TasksListViewProtocol: ControllerBackedProtocol, LoadableViewProtocol {
    func didReceive(viewModels: [TasksListViewModel])
}

protocol TasksListPresenterProtocol: class {
    func setup()
    func addTask()
    func selectTask(at index: Int)
}

protocol TasksListInteractorInputProtocol: class {
    func setup()
    func add(task: TaskInit)
}

protocol TasksListInteractorOutputProtocol: class {
    func didReceiveCampaign(result: Result<SCCampaign, Error>)
    func didReceiveAccount(result: Result<DbAccount?, Error>)
    func didReceiveLocalContract(result: Result<DbContract?, Error>)

    func willSubmitTransaction(for hash: String)
    func didSubmitTransaction(for hash: String)
    func didReceiveTransaction(result: Result<Bool, Error>, for hash: String?)
}

protocol TasksListWireframeProtocol: AlertPresentable {
    func showNewTaskForm(from view: TasksListViewProtocol?, delegate: FormInputDelegate)
    func showTaskDetails(
        from view: TasksListViewProtocol?,
        contractId: AccountId,
        taskId: BigUInt,
        accountId: AccountId
    )
}
