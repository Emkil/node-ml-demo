import UIKit

protocol TasksListViewModelFactoryProtocol {
    func createViewModel(from campaign: SCCampaign) -> [TasksListViewModel]
}

final class TasksListViewModelFactory: TasksListViewModelFactoryProtocol {
    func createViewModel(from campaign: SCCampaign) -> [TasksListViewModel] {
        campaign.tasks.map { task in
            let isPending: Bool = {
                if [.taskCompleted, .confirmed].contains(task.status) {
                    let payments = campaign.paymentOrders.filter({ $0.taskId == task.taskId })
                    let isPaid = !payments.isEmpty && payments.allSatisfy({ $0.paymentStatus == .paymentCompleted })
                    return !isPaid
                } else {
                    return true
                }
            }()

            return TasksListViewModel(
                taskId: R.string.localizable.taskNumberFormat("\(task.taskId)"),
                isPending: isPending
            )
        }
    }
}
