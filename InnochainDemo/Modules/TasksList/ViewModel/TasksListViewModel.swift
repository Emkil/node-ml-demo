import Foundation

struct TasksListViewModel {
    let taskId: String
    let isPending: Bool
}
