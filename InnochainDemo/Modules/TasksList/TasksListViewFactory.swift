import Foundation
import RobinHood
import SoraKeystore

struct TasksListViewFactory {
    static func createView(for contractId: AccountId, selectedAccountId: AccountId) -> TasksListViewProtocol? {
        guard let interactor = createInteractor(for: contractId, selectedAccountId: selectedAccountId) else {
            return nil
        }

        let wireframe = TasksListWireframe()

        let presenter = TasksListPresenter(
            interactor: interactor,
            wireframe: wireframe,
            viewModelFactory: TasksListViewModelFactory(),
            logger: Logger.shared
        )

        let view = TasksListViewController(presenter: presenter)

        presenter.view = view
        interactor.presenter = presenter

        return view
    }

    static private func createInteractor(
        for contractId: AccountId,
        selectedAccountId: AccountId
    ) -> TasksListInteractor? {
        let storageFacade = UserDataStorageFacade.shared

        let accountRepository: CoreDataRepository<DbAccount, CDAccountItem> = storageFacade.createRepository()
        let contractRepository: CoreDataRepository<DbContract, CDContract> = storageFacade.createRepository()

        let txRepository: CoreDataRepository<DbTransaction, CDTransactionItem> =
            UserDataStorageFacade.shared.createRepository()

        let transactionService = TransactionService(
            operationFactory: NodeOperationFactory(url: PrivateUrl.main),
            repository: AnyDataProviderRepository(txRepository),
            operationQueue: OperationQueueFacade.defaultQueue
        )

        let txProviderFactory = StreamableProviderFactory(
            storageFacade: UserDataStorageFacade.shared,
            logger: Logger.shared
        )

        let signer = SigningWrapper(
            keystore: Keychain(),
            tag: KeystoreTag.secretKeyTagForAddress(selectedAccountId)
        )

        return TasksListInteractor(
            selectedAccountId: selectedAccountId,
            contractAccountId: contractId,
            nodeOperationFactory: NodeOperationFactory(url: PrivateUrl.main),
            transactionService: transactionService,
            txProvider: txProviderFactory.createAllTransactionProvider(),
            signer: signer,
            contractRepository: AnyDataProviderRepository(contractRepository),
            accountRepository: AnyDataProviderRepository(accountRepository),
            operationQueue: OperationQueueFacade.defaultQueue
        )
    }
}
