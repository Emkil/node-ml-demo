import UIKit

final class TaskListTableViewCell: BaseTitleValueTableViewCell {
    func bind(viewModel: TasksListViewModel) {
        titleLabel.text = viewModel.taskId

        iconView.image = viewModel.isPending ? R.image.iconPending()?.withRenderingMode(.alwaysTemplate) : nil
        iconView.tintColor = .lightGray

        accessoryType = .disclosureIndicator
    }
}
