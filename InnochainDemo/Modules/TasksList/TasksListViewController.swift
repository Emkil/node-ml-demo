import UIKit

final class TasksListViewController: UIViewController, ViewHolder {
    typealias RootViewType = TasksListViewLayout

    let presenter: TasksListPresenterProtocol

    private var viewModels: [TasksListViewModel] = []

    init(presenter: TasksListPresenterProtocol) {
        self.presenter = presenter
        super.init(nibName: nil, bundle: nil)
    }

    @available(*, unavailable)
    required init?(coder _: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }

    override func loadView() {
        view = TasksListViewLayout()
    }

    override func viewDidLoad() {
        super.viewDidLoad()

        configureNavigationItem()
        configureTableView()
        setupLocalization()
        presenter.setup()
    }

    private func configureNavigationItem() {
        let addBarItem = UIBarButtonItem(
            barButtonSystemItem: .add,
            target: self,
            action: #selector(actionTask)
        )

        navigationItem.rightBarButtonItem = addBarItem
    }

    private func configureTableView() {
        rootView.tableView.registerClassForCell(TaskListTableViewCell.self)
        rootView.tableView.delegate = self
        rootView.tableView.dataSource = self
        rootView.tableView.rowHeight = 48.0
    }

    private func setupLocalization() {
        title = R.string.localizable.tasksListTitle()
    }

    @objc private func actionTask() {
        presenter.addTask()
    }
}

extension TasksListViewController: TasksListViewProtocol {
    func didReceive(viewModels: [TasksListViewModel]) {
        self.viewModels = viewModels
        rootView.tableView.reloadData()
    }
}

extension TasksListViewController: UITableViewDataSource {
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        viewModels.count
    }

    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCellWithType(TaskListTableViewCell.self)!
        cell.bind(viewModel: viewModels[indexPath.row])
        return cell
    }
}

extension TasksListViewController: UITableViewDelegate {
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        tableView.deselectRow(at: indexPath, animated: true)

        presenter.selectTask(at: indexPath.row)
    }
}
