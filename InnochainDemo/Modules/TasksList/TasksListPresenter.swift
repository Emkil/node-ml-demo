import Foundation
import SoraFoundation
import BigInt

final class TasksListPresenter {
    weak var view: TasksListViewProtocol?
    let wireframe: TasksListWireframeProtocol
    let interactor: TasksListInteractorInputProtocol
    let viewModelFactory: TasksListViewModelFactoryProtocol
    let logger: LoggerProtocol?

    private var campaign: SCCampaign?
    private var account: DbAccount?
    private var localContract: DbContract?

    init(
        interactor: TasksListInteractorInputProtocol,
        wireframe: TasksListWireframeProtocol,
        viewModelFactory: TasksListViewModelFactoryProtocol,
        logger: LoggerProtocol? = nil
    ) {
        self.interactor = interactor
        self.wireframe = wireframe
        self.viewModelFactory = viewModelFactory
        self.logger = logger
    }

    private func provideViewModel() {
        guard let campaign = campaign else {
            return
        }

        let viewModels = viewModelFactory.createViewModel(from: campaign)
        view?.didReceive(viewModels: viewModels)
    }
}

extension TasksListPresenter: TasksListPresenterProtocol {
    func setup() {
        interactor.setup()
    }

    func selectTask(at index: Int) {
        guard let localContract = localContract,
              let contractId = try? AccountId.fromHex(string: localContract.identifier),
              let campaign = campaign, index < campaign.tasks.count,
              let accountId = account?.accountId else {
            return
        }

        let task = campaign.tasks[index]

        wireframe.showTaskDetails(
            from: view,
            contractId: contractId,
            taskId: BigUInt(task.taskId),
            accountId: accountId
        )
    }

    func addTask() {
        guard let campaign = campaign, let account = account else {
            return
        }

        guard campaign.agreement.customer.personId == account.accountId?.toBigEndian() else {
            wireframe.present(
                message: R.string.localizable.tasksAddOnlyCustomerMessage(),
                title: R.string.localizable.commonError(),
                closeAction: R.string.localizable.commonClose(),
                from: view
            )

            return
        }

        wireframe.showNewTaskForm(from: view, delegate: self)
    }
}

extension TasksListPresenter: TasksListInteractorOutputProtocol {
    func didReceiveCampaign(result: Result<SCCampaign, Error>) {
        switch result {
        case .success(let campaign):
            self.campaign = campaign
            provideViewModel()
        case .failure(let error):
            logger?.error("Did receive remote contract error: \(error)")
        }
    }

    func didReceiveAccount(result: Result<DbAccount?, Error>) {
        switch result {
        case .success(let account):
            self.account = account
            provideViewModel()
        case .failure(let error):
            logger?.error("Did receive account error: \(error)")
        }
    }

    func didReceiveLocalContract(result: Result<DbContract?, Error>) {
        switch result {
        case .success(let contract):
            self.localContract = contract
            provideViewModel()
        case .failure(let error):
            logger?.error("Did receive contract error: \(error)")
        }
    }

    func willSubmitTransaction(for hash: String) {
        view?.didStartLoading()

        logger?.debug("Will submit transaction: \(hash)")
    }

    func didSubmitTransaction(for hash: String) {
        logger?.debug("Did submit transaction: \(hash)")
    }

    func didReceiveTransaction(result: Result<Bool, Error>, for hash: String?) {
        view?.didStopLoading()

        switch result {
        case let .success(value):
            logger?.debug("Transaction result \(hash ?? ""): \(value)")
        case let .failure(error):
            logger?.error("Transaction submition \(hash ?? "") error: \(error)")
        }
    }
}

extension TasksListPresenter: FormInputDelegate {
    func didReceive(inputViewModels: [InputViewModelProtocol], context: Any?) {
        guard
            let captainAccountId = try? AccountId.fromHex(string: inputViewModels[0].inputHandler.value),
            let workerAccountId = try? AccountId.fromHex(string: inputViewModels[2].inputHandler.value),
            let expectedGas = BigUInt(inputViewModels[4].inputHandler.value) else {
            return
        }

        let taskInit = TaskInit(
            captainAccountId: captainAccountId,
            captainName: inputViewModels[1].inputHandler.value,
            workerAccountId: workerAccountId,
            workerName: inputViewModels[3].inputHandler.value,
            expectedGas: expectedGas,
            paymentType: .pre
        )

        interactor.add(task: taskInit)
    }
}
