import UIKit
import BigInt

final class TasksListWireframe: TasksListWireframeProtocol {
    func showNewTaskForm(from view: TasksListViewProtocol?, delegate: FormInputDelegate) {
        guard let formView = FormInputViewFactory.createNewTaskForm(delegate: delegate, context: nil) else {
            return
        }

        let navigationController = UINavigationController(rootViewController: formView.controller)
        view?.controller.present(navigationController, animated: true, completion: nil)
    }

    func showTaskDetails(
        from view: TasksListViewProtocol?,
        contractId: AccountId,
        taskId: BigUInt,
        accountId: AccountId
    ) {
        guard
            let detalsView = TaskDetailsViewFactory.createView(
                for: contractId,
                taskId: taskId,
                accountId: accountId
            ) else {
            return
        }

        view?.controller.navigationController?.pushViewController(detalsView.controller, animated: true)
    }
}
