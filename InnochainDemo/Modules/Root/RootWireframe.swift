import UIKit

final class RootWireframe: RootWireframeProtocol {
    func showAccountList(on view: UIWindow) {
        guard let mainNavigation = MainNavigationViewFactory.createView() else {
            return
        }

        view.rootViewController = mainNavigation.controller
    }

    func showBroken(on view: UIWindow) {
        // normally user must not see this but on malicious devices it is possible
        view.backgroundColor = .red
    }
}
