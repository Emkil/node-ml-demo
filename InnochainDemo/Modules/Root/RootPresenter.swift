import UIKit

final class RootPresenter {
    var view: UIWindow!
    var wireframe: RootWireframeProtocol!
    var interactor: RootInteractorInputProtocol!
}

extension RootPresenter: RootPresenterProtocol {
    func loadOnLaunch() {
        interactor.setup()
        interactor.decideModuleSynchroniously()
    }
}

extension RootPresenter: RootInteractorOutputProtocol {
    func didDecideAccountList() {
        wireframe.showAccountList(on: view)
    }

    func didDecideBroken() {
        wireframe.showBroken(on: view)
    }
}
