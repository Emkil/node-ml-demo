import UIKit

protocol RootPresenterProtocol: AnyObject {
    func loadOnLaunch()
}

protocol RootWireframeProtocol: AnyObject {
    func showAccountList(on view: UIWindow)
    func showBroken(on view: UIWindow)
}

protocol RootInteractorInputProtocol: AnyObject {
    func setup()
    func decideModuleSynchroniously()
}

protocol RootInteractorOutputProtocol: AnyObject {
    func didDecideAccountList()
    func didDecideBroken()
}

protocol RootPresenterFactoryProtocol: AnyObject {
    static func createPresenter(with view: UIWindow) -> RootPresenterProtocol
}
