import Foundation
import SoraKeystore
import IrohaCrypto
import RobinHood

final class RootInteractor {
    weak var presenter: RootInteractorOutputProtocol?

    let settings: SettingsManagerProtocol
    let keystore: KeystoreProtocol

    init(
        settings: SettingsManagerProtocol,
        keystore: KeystoreProtocol
    ) {
        self.settings = settings
        self.keystore = keystore
    }
}

extension RootInteractor: RootInteractorInputProtocol {
    func decideModuleSynchroniously() {
        presenter?.didDecideAccountList()
    }

    func setup() {}
}
