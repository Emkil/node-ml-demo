import Foundation
import RobinHood

protocol AccountListViewProtocol: ControllerBackedProtocol {
    func didReceive(viewModels: [AccountListViewModel])
}

protocol AccountListPresenterProtocol: class {
    func setup()
    func createAccount()
    func selectAccount(_ viewModel: AccountListViewModel)
}

protocol AccountListInteractorInputProtocol: class {
    func setup()
    func createAccount(with name: String)
}

protocol AccountListInteractorOutputProtocol: class {
    func didReceive(createdAccounts: [DbAccount], pendingAccounts: [DbAccount])
    func didReceiveAccountCreation(result: Result<Void, Error>, for name: String)
}

protocol AccountListWireframeProtocol: AlertInputPresenting, AlertPresentable {
    func showContracts(from view: AccountListViewProtocol?, accountId: AccountId)
}
