import Foundation

protocol AccountListViewModelFactoryProtocol {
    func createViewModelList(
        from createdAcounts: [DbAccount],
        pendingAccounts: [DbAccount]
    ) -> [AccountListViewModel]
}

final class AccountListViewModelFactory {}

extension AccountListViewModelFactory: AccountListViewModelFactoryProtocol {
    func createViewModelList(from createdAcounts: [DbAccount], pendingAccounts: [DbAccount]) -> [AccountListViewModel] {
        let sortClosure: (DbAccount, DbAccount) -> Bool = { $0.createdAt > $1.createdAt}

        let createdViewModels = createdAcounts.sorted(by: sortClosure).map { account in
            AccountListViewModel(
                name: account.name,
                accountId: account.identifier,
                status: .completed(details: "")
            )
        }

        let pendingViewModels = pendingAccounts.sorted(by: sortClosure).map { account in
            AccountListViewModel(
                name: account.name,
                accountId: account.identifier,
                status: .pending
            )
        }

        return pendingViewModels + createdViewModels
    }
}
