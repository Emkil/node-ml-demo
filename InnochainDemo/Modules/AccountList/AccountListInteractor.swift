import UIKit
import IrohaCrypto
import SoraKeystore
import RobinHood

enum AccountListInteractorError: Error {
    case accountCreationFailed
    case accountInProgress
}

final class AccountListInteractor {
    struct PendingAccount {
        let txHash: String?
        let accountId: AccountId
        let accountName: String
        let createdAt: Date
        let keypair: IRCryptoKeypairProtocol

        func byChanging(txHash: String?) -> PendingAccount {
            PendingAccount(
                txHash: txHash,
                accountId: accountId,
                accountName: accountName,
                createdAt: createdAt,
                keypair: keypair
            )
        }
    }

    weak var presenter: AccountListInteractorOutputProtocol!

    let keypairFactory: IRCryptoKeyFactoryProtocol
    let keystore: KeystoreProtocol
    let transactionService: TransactionServiceProtocol
    let operationQueue: OperationQueue
    let txProvider: StreamableProvider<DbTransaction>
    let accountRepository: AnyDataProviderRepository<DbAccount>
    let logger: LoggerProtocol?

    private var pendingAccount: PendingAccount?

    init(
        transactionService: TransactionServiceProtocol,
        keypairFactory: IRCryptoKeyFactoryProtocol,
        keystore: KeystoreProtocol,
        txProvider: StreamableProvider<DbTransaction>,
        accountRepository: AnyDataProviderRepository<DbAccount>,
        operationQueue: OperationQueue,
        logger: LoggerProtocol? = nil
    ) {
        self.transactionService = transactionService
        self.keypairFactory = keypairFactory
        self.keystore = keystore
        self.txProvider = txProvider
        self.accountRepository = accountRepository
        self.operationQueue = operationQueue
        self.logger = logger
    }

    private func handleTxSubmittion(result: Result<String, Error>) {
        guard let pendingAccount = pendingAccount else {
            return
        }

        switch result {
        case let .success(txHash):
            self.pendingAccount = pendingAccount.byChanging(txHash: txHash)
        case let .failure(error):
            failPendingAccount(with: error)
        }
    }

    func applyPendingAccountAndNotify() {
        guard let pendingAccount = pendingAccount else {
            return
        }

        self.pendingAccount = nil

        let dbAccount = DbAccount(
            identifier: pendingAccount.accountId.value.toHex(includePrefix: true),
            name: pendingAccount.accountName,
            createdAt: pendingAccount.createdAt
        )

        let closureOperation = ClosureOperation {
            try self.keystore.saveSecretKey(
                pendingAccount.keypair.privateKey().rawData(),
                accountId: pendingAccount.accountId
            )
        }

        let saveAccountOperation = accountRepository.saveOperation({
            try closureOperation.extractNoCancellableResultData()
            return [dbAccount]
        }, { [] })

        saveAccountOperation.addDependency(closureOperation)

        saveAccountOperation.completionBlock = { [weak self] in
            DispatchQueue.main.async {
                do {
                    try saveAccountOperation.extractNoCancellableResultData()
                    self?.presenter.didReceiveAccountCreation(result: .success(()), for: pendingAccount.accountName)
                    self?.provideAccounts()
                } catch {
                    self?.presenter.didReceiveAccountCreation(
                        result: .failure(error),
                        for: pendingAccount.accountName
                    )
                }
            }
        }

        operationQueue.addOperations([closureOperation, saveAccountOperation], waitUntilFinished: false)
    }

    func failPendingAccount(with error: Error) {
        guard let pendingAccount = pendingAccount else {
            return
        }

        self.pendingAccount = nil

        presenter.didReceiveAccountCreation(result: .failure(error), for: pendingAccount.accountName)
    }

    private func checkAccountPendingCompletion(using transaction: DbTransaction) {
        switch transaction.status {
        case .completed:
            applyPendingAccountAndNotify()
        case .failed:
            failPendingAccount(with: AccountListInteractorError.accountCreationFailed)
        case .pending:
            break
        }
    }

    private func handleTxChanges(_ changes: [DataProviderChange<DbTransaction>]) {
        guard let pendingAccount = pendingAccount else {
            return
        }

        for change in changes {
            switch change {
            case let .insert(newItem), let .update(newItem):
                if pendingAccount.txHash == newItem.identifier {
                    checkAccountPendingCompletion(using: newItem)
                }
            case let .delete(deletedIdentifier):
                if pendingAccount.txHash == deletedIdentifier {
                    failPendingAccount(with: AccountListInteractorError.accountCreationFailed)
                }
            }
        }
    }

    private func subscribeToDbTransactions() {
        let changeClosure: ([DataProviderChange<DbTransaction>]) -> Void = { [weak self] changes in
            self?.handleTxChanges(changes)
        }

        let failureClosure: (Error) -> Void = { [weak self] error in
            self?.logger?.error("Tx subscription failed: \(error)")
        }

        txProvider.addObserver(
            self,
            deliverOn: .main,
            executing: changeClosure,
            failing: failureClosure,
            options: StreamableProviderObserverOptions()
        )
    }

    private func provideAccounts() {
        let allAccountsOperation = accountRepository.fetchAllOperation(with: RepositoryFetchOptions())
        allAccountsOperation.completionBlock = { [weak self] in
            DispatchQueue.main.async {
                do {
                    let accounts = try allAccountsOperation.extractNoCancellableResultData()

                    let pendingAccounts = self?.pendingAccount.map { pending in
                        return [
                            DbAccount(
                                identifier: pending.accountId.value.toHex(includePrefix: true),
                                name: pending.accountName,
                                createdAt: pending.createdAt
                            )
                        ]
                    } ?? []

                    self?.presenter.didReceive(createdAccounts: accounts, pendingAccounts: pendingAccounts)
                } catch {
                    self?.logger?.error("Account fetching failed: \(error)")
                }
            }
        }

        operationQueue.addOperation(allAccountsOperation)
    }
}

extension AccountListInteractor: AccountListInteractorInputProtocol {
    func setup() {
        subscribeToDbTransactions()

        provideAccounts()
    }

    func createAccount(with name: String) {
        guard pendingAccount == nil else {
            presenter.didReceiveAccountCreation(
                result: .failure(AccountListInteractorError.accountInProgress),
                for: name
            )
            return
        }

        do {
            let accountId = try AccountId.createRandom()
            let keypair = try keypairFactory.createRandomKeypair()
            let inMemoryKeystore = InMemoryKeychain()
            try inMemoryKeystore.saveSecretKey(keypair.privateKey().rawData(), accountId: accountId)

            let publicKey = PublicKey(value: keypair.publicKey().rawData())
            let signer = SigningWrapper(
                keystore: inMemoryKeystore,
                tag: KeystoreTag.secretKeyTagForAddress(accountId)
            )

            let transaction = try TransactionBuilder().createUser(
                with: accountId,
                publicKeys: SortedSet(items: [publicKey])
            ).sign(using: [signer])

            pendingAccount = PendingAccount(
                txHash: nil,
                accountId: accountId,
                accountName: name,
                createdAt: Date(),
                keypair: keypair
            )

            provideAccounts()

            transactionService.submit(transaction: transaction, runningIn: .main) { [weak self] result in
                self?.handleTxSubmittion(result: result)
            }

        } catch {
            presenter.didReceiveAccountCreation(result: .failure(error), for: name)
        }
    }
}
