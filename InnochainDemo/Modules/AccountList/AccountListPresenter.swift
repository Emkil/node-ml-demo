import Foundation
import SoraFoundation

final class AccountListPresenter {
    weak var view: AccountListViewProtocol?
    let wireframe: AccountListWireframeProtocol
    let interactor: AccountListInteractorInputProtocol
    let viewModelFactory: AccountListViewModelFactoryProtocol
    let logger: LoggerProtocol?

    init(
        interactor: AccountListInteractorInputProtocol,
        wireframe: AccountListWireframeProtocol,
        viewModelFactory: AccountListViewModelFactoryProtocol,
        logger: LoggerProtocol? = nil
    ) {
        self.interactor = interactor
        self.wireframe = wireframe
        self.viewModelFactory = viewModelFactory
        self.logger = logger
    }

    func provideViewModel(from createdAccounts: [DbAccount], pendingAccounts: [DbAccount]) {
        let viewModes = viewModelFactory.createViewModelList(from: createdAccounts, pendingAccounts: pendingAccounts)
        view?.didReceive(viewModels: viewModes)
    }
}

extension AccountListPresenter: AccountListPresenterProtocol {
    func selectAccount(_ viewModel: AccountListViewModel) {
        guard
            case .completed = viewModel.status,
            let accountId = try? AccountId.fromHex(string: viewModel.accountId) else {
            return
        }

        wireframe.showContracts(from: view, accountId: accountId)
    }

    func setup() {
        interactor.setup()
    }

    func createAccount() {
        guard let view = view else {
            return
        }

        wireframe.presentInputWithTitle(
            R.string.localizable.accountListNameTitle(),
            placeholder: R.string.localizable.accountListNamePlaceholder(),
            from: view
        ) { [weak self] maybeName in
            if let name = maybeName, !name.isEmpty {
                self?.interactor.createAccount(with: name)
            }
        }
    }
}

extension AccountListPresenter: AccountListInteractorOutputProtocol {
    func didReceive(createdAccounts: [DbAccount], pendingAccounts: [DbAccount]) {
        provideViewModel(from: createdAccounts, pendingAccounts: pendingAccounts)
    }

    func didReceiveAccountCreation(result: Result<Void, Error>, for name: String) {
        switch result {
        case .success:
            logger?.debug("Account created with name: \(name)")
        case .failure(let error):
            logger?.error("Account creation failed: \(error)")
        }
    }
}
