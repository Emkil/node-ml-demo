import UIKit
import SoraFoundation

final class AccountListViewController: UIViewController, ViewHolder {
    typealias RootViewType = AccountListViewLayout

    let presenter: AccountListPresenterProtocol

    private var viewModels: [AccountListViewModel] = []

    init(presenter: AccountListPresenterProtocol) {
        self.presenter = presenter
        super.init(nibName: nil, bundle: nil)
    }

    @available(*, unavailable)
    required init?(coder _: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }

    override func loadView() {
        view = AccountListViewLayout()
    }

    override func viewDidLoad() {
        super.viewDidLoad()

        configureNavigationItem()
        configureTableView()
        setupLocalization()

        presenter.setup()
    }

    private func configureNavigationItem() {
        let addBarItem = UIBarButtonItem(
            barButtonSystemItem: .add,
            target: self,
            action: #selector(actionCreateAccount)
        )

        navigationItem.rightBarButtonItem = addBarItem
    }

    private func configureTableView() {
        rootView.tableView.registerClassForCell(AccountListTableViewCell.self)
        rootView.tableView.delegate = self
        rootView.tableView.dataSource = self
    }

    private func setupLocalization() {
        title = R.string.localizable.accountListTitle(preferredLanguages: selectedLocale.rLanguages)
    }

    @objc private func actionCreateAccount() {
        presenter.createAccount()
    }
}

extension AccountListViewController: UITableViewDataSource {
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        viewModels.count
    }

    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCellWithType(AccountListTableViewCell.self)!
        cell.bind(viewModel: viewModels[indexPath.row])
        return cell
    }
}

extension AccountListViewController: UITableViewDelegate {
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        tableView.deselectRow(at: indexPath, animated: true)

        presenter.selectAccount(viewModels[indexPath.row])
    }
}

extension AccountListViewController: AccountListViewProtocol {
    func didReceive(viewModels: [AccountListViewModel]) {
        self.viewModels = viewModels
        rootView.tableView.reloadData()
    }
}

extension AccountListViewController: Localizable {
    func applyLocalization() {
        if isViewLoaded {
            setupLocalization()
        }
    }
}
