import Foundation
import RobinHood
import IrohaCrypto
import SoraKeystore
import SoraFoundation

struct AccountListViewFactory {
    static func createView() -> AccountListViewProtocol? {
        let interactor = createInteractor()
        let wireframe = AccountListWireframe()

        let presenter = AccountListPresenter(
            interactor: interactor,
            wireframe: wireframe,
            viewModelFactory: AccountListViewModelFactory(),
            logger: Logger.shared
        )

        let view = AccountListViewController(presenter: presenter)

        presenter.view = view
        interactor.presenter = presenter

        return view
    }

    private static func createInteractor() -> AccountListInteractor {
        let accountRepository: CoreDataRepository<DbAccount, CDAccountItem> =
            UserDataStorageFacade.shared.createRepository()

        let txRepository: CoreDataRepository<DbTransaction, CDTransactionItem> =
            UserDataStorageFacade.shared.createRepository()

        let transactionService = TransactionService(
            operationFactory: NodeOperationFactory(url: PrivateUrl.main),
            repository: AnyDataProviderRepository(txRepository),
            operationQueue: OperationQueueFacade.defaultQueue
        )

        let txProviderFactory = StreamableProviderFactory(
            storageFacade: UserDataStorageFacade.shared,
            logger: Logger.shared
        )

        return AccountListInteractor(
            transactionService: transactionService,
            keypairFactory: GostKeyFactory(),
            keystore: Keychain(),
            txProvider: txProviderFactory.createAllTransactionProvider(),
            accountRepository: AnyDataProviderRepository(accountRepository),
            operationQueue: OperationQueueFacade.defaultQueue
        )
    }
}
