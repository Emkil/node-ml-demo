import UIKit

final class AccountListWireframe: AccountListWireframeProtocol {
    func showContracts(from view: AccountListViewProtocol?, accountId: AccountId) {
        let contractsAction = AlertPresentableAction(title: R.string.localizable.contractListTitle()) { [weak self] in
            self?.presentContracts(from: view, accountId: accountId)
        }

        let copyAction = AlertPresentableAction(title: R.string.localizable.commonCopyId()) {
            UIPasteboard.general.string = accountId.value.toHex(includePrefix: true)
        }

        let alertViewModel = AlertPresentableViewModel(
            title: R.string.localizable.commonChooseAction(),
            message: "",
            actions: [contractsAction, copyAction],
            closeAction: R.string.localizable.commonCancel()
        )

        present(viewModel: alertViewModel, style: .actionSheet, from: view)
    }

    private func presentContracts(from view: AccountListViewProtocol?, accountId: AccountId) {
        guard let contractListView = ContractListViewFactory.createView(for: accountId) else {
            return
        }

        view?.controller.navigationController?.pushViewController(
            contractListView.controller,
            animated: true
        )
    }
}
