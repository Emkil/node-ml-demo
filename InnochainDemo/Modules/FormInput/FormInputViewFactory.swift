import Foundation
import SoraFoundation

struct FormInputViewFactory {
    static func createContractCreateView(
        for customer: DbAccount?,
        delegate: FormInputDelegate,
        context: Any?
    ) -> FormInputViewProtocol? {
        let title = R.string.localizable.fuelCreateTitle()

        let viewModels: [InputViewModelProtocol] = [
            InputViewModel(
                inputHandler: InputHandler(predicate: NSPredicate.notEmpty),
                title: R.string.localizable.contractInputName(),
                placeholder: "",
                autocapitalization: .none
            ),
            InputViewModel(
                inputHandler: InputHandler(value: customer?.identifier ?? "", predicate: NSPredicate.account),
                title: R.string.localizable.fuelCustomerInputTitle(),
                placeholder: R.string.localizable.commonAccountHint(),
                autocapitalization: .none
            ),
            InputViewModel(
                inputHandler: InputHandler(value: customer?.name ?? "", predicate: NSPredicate.notEmpty),
                title: R.string.localizable.fuelCustomerNameTitle(),
                placeholder: "",
                autocapitalization: .none
            ),
            InputViewModel(
                inputHandler: InputHandler(predicate: NSPredicate.account),
                title: R.string.localizable.fuelSupplierInputTitle(),
                placeholder: R.string.localizable.commonAccountHint(),
                autocapitalization: .none
            ),
            InputViewModel(
                inputHandler: InputHandler(predicate: NSPredicate.notEmpty),
                title: R.string.localizable.fuelSupplierNameTitle(),
                placeholder: "",
                autocapitalization: .none
            ),
            InputViewModel(
                inputHandler: InputHandler(predicate: NSPredicate.notEmpty),
                title: R.string.localizable.fuelAggreementInputTitle(),
                placeholder: "",
                autocapitalization: .none
            ),
            InputViewModel(
                inputHandler: InputHandler(predicate: NSPredicate.account),
                title: R.string.localizable.fuelBankInputTitle(),
                placeholder: R.string.localizable.commonAccountHint(),
                autocapitalization: .none
            )
        ]

        return createView(with: title, viewModels: viewModels, delegate: delegate, context: context)
    }

    static func createContractAddView(
        delegate: FormInputDelegate,
        context: Any?
    ) -> FormInputViewProtocol? {
        let title = R.string.localizable.fuelCreateTitle()

        let viewModels: [InputViewModelProtocol] = [
            InputViewModel(
                inputHandler: InputHandler(predicate: NSPredicate.notEmpty),
                title: R.string.localizable.contractInputName(),
                placeholder: "",
                autocapitalization: .none
            ),
            InputViewModel(
                inputHandler: InputHandler(predicate: NSPredicate.account),
                title: R.string.localizable.fuelContractInputTitle(),
                placeholder: R.string.localizable.commonAccountHint(),
                autocapitalization: .none
            )
        ]

        return createView(with: title, viewModels: viewModels, delegate: delegate, context: context)
    }

    static func createAddPriceView(
        delegate: FormInputDelegate,
        context: Any?
    ) -> FormInputViewProtocol? {
        let title = R.string.localizable.priceChangeNewTitle()

        let viewModels: [InputViewModelProtocol] = [
            InputViewModel(
                inputHandler: InputHandler(predicate: NSPredicate.price),
                title: R.string.localizable.priceChangeInputTitle(),
                placeholder: "",
                autocapitalization: .none
            )
        ]

        return createView(with: title, viewModels: viewModels, delegate: delegate, context: context)
    }

    static func createNewTaskForm(delegate: FormInputDelegate, context: Any?) -> FormInputViewProtocol? {
        let title = R.string.localizable.taskNewTitle()

        let viewModels: [InputViewModelProtocol] = [
            InputViewModel(
                inputHandler: InputHandler(predicate: NSPredicate.account),
                title: R.string.localizable.taskNewCaptainIdInput(),
                placeholder: "",
                autocapitalization: .none
            ),
            InputViewModel(
                inputHandler: InputHandler(predicate: NSPredicate.notEmpty),
                title: R.string.localizable.taskNewCaptainNameInput(),
                placeholder: "",
                autocapitalization: .none
            ),
            InputViewModel(
                inputHandler: InputHandler(predicate: NSPredicate.account),
                title: R.string.localizable.taskNewWorkerIdInput(),
                placeholder: "",
                autocapitalization: .none
            ),
            InputViewModel(
                inputHandler: InputHandler(predicate: NSPredicate.notEmpty),
                title: R.string.localizable.taskNewWorkerNameInput(),
                placeholder: "",
                autocapitalization: .none
            ),
            InputViewModel(
                inputHandler: InputHandler(predicate: NSPredicate.quantity),
                title: R.string.localizable.taskNewFuelAmountInput(),
                placeholder: "",
                autocapitalization: .none
            )
        ]

        return createView(with: title, viewModels: viewModels, delegate: delegate, context: context)
    }

    private static func createView(
        with title: String,
        viewModels: [InputViewModelProtocol],
        delegate: FormInputDelegate,
        context: Any?
    ) -> FormInputViewProtocol? {
        let wireframe = FormInputWireframe()

        let presenter = FormInputPresenter(
            wireframe: wireframe,
            inputViewModels: viewModels,
            delegate: delegate,
            context: context
        )

        let view = FormInputViewController(presenter: presenter)

        view.controller.title = title

        presenter.view = view

        return view
    }
}
