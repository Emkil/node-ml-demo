import Foundation
import SoraFoundation

protocol FormInputDelegate: AnyObject {
    func didReceive(inputViewModels: [InputViewModelProtocol], context: Any?)
}
