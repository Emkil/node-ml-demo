import UIKit
import SoraFoundation
import SoraUI

final class FormInputViewLayout: UIView {
    let contentView: ScrollableContainerView = ScrollableContainerView()

    private(set) var inputViews: [AnimatedTextField]?

    override init(frame: CGRect) {
        super.init(frame: frame)

        backgroundColor = .white

        setupLayout()
    }

    @available(*, unavailable)
    required init?(coder _: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }

    func bind(viewModels: [InputViewModelProtocol]) {
        contentView.stackView.arrangedSubviews.forEach { $0.removeFromSuperview() }

        let textViews: [AnimatedTextField] = viewModels.map { viewModel in
            let inputView = AnimatedTextField()
            inputView.title = viewModel.title
            inputView.text = viewModel.inputHandler.value
            return inputView
        }

        for textView in textViews {
            let border = BorderedContainerView()
            border.borderType = .bottom
            border.strokeWidth = 1.0
            border.strokeColor = .lightGray
            contentView.stackView.addArrangedSubview(border)

            border.snp.makeConstraints { make in
                make.width.equalTo(self).offset(-32.0)
                make.height.equalTo(48.0)
            }

            border.addSubview(textView)
            textView.snp.makeConstraints { make in
                make.edges.equalToSuperview()
            }
        }

        inputViews = textViews
    }

    private func setupLayout() {
        addSubview(contentView)
        contentView.snp.makeConstraints { make in
            make.leading.trailing.equalToSuperview()
            make.top.bottom.equalTo(safeAreaLayoutGuide)
        }
    }
}
