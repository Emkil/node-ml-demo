import SoraFoundation

protocol FormInputViewProtocol: ControllerBackedProtocol {
    func didReceiveInput(viewModels: [InputViewModelProtocol])
    func didReceiveError(at index: Int)
}

protocol FormInputPresenterProtocol: class {
    func setup()
    func submit()
    func cancel()
}

protocol FormInputWireframeProtocol: class {
    func complete(on view: FormInputViewProtocol?)
}
