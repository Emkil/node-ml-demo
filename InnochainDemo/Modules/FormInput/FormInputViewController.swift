import UIKit
import SoraFoundation
import SoraUI

final class FormInputViewController: UIViewController, ViewHolder {
    typealias RootViewType = FormInputViewLayout

    let presenter: FormInputPresenterProtocol

    var keyboardHandler: KeyboardHandler?

    var inputViewModels: [InputViewModelProtocol] = []

    init(presenter: FormInputPresenterProtocol) {
        self.presenter = presenter
        super.init(nibName: nil, bundle: nil)
    }

    @available(*, unavailable)
    required init?(coder _: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }

    override func loadView() {
        view = FormInputViewLayout()
    }

    override func viewDidLoad() {
        super.viewDidLoad()

        setupNavigationBar()

        presenter.setup()
    }

    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)

        if keyboardHandler == nil {
            setupKeyboardHandler()
        }
    }

    override func viewDidDisappear(_ animated: Bool) {
        super.viewDidDisappear(animated)

        clearKeyboardHandler()
    }

    private func setupNavigationBar() {
        let cancelBarItem = UIBarButtonItem(
            title: R.string.localizable.commonCancel(),
            style: .plain,
            target: self,
            action: #selector(actionCancel)
        )

        let submitBarItem = UIBarButtonItem(
            title: R.string.localizable.commonDone(),
            style: .done,
            target: self,
            action: #selector(actionSubmit)
        )

        navigationItem.leftBarButtonItem = cancelBarItem
        navigationItem.rightBarButtonItem = submitBarItem
    }

    private func setupInputViews() {
        guard let inputViews = rootView.inputViews else {
            return
        }

        inputViews.enumerated().forEach { (index, inputView) in
            inputView.delegate = self
            inputView.addTarget(self, action: #selector(actionEditingChanged(_:)), for: .editingChanged)

            if index == inputViews.count - 1 {
                inputView.textField.returnKeyType = .done
            } else {
                inputView.textField.returnKeyType = .next
            }
        }
    }

    @objc private func actionCancel() {
        presenter.cancel()
    }

    @objc private func actionSubmit() {
        presenter.submit()
    }

    @objc private func actionEditingChanged(_ sender: AnimatedTextField) {
        if let index = rootView.inputViews?.firstIndex(of: sender) {
            sender.text = inputViewModels[index].inputHandler.value
        }
    }
}

extension FormInputViewController: KeyboardAdoptable {
    func updateWhileKeyboardFrameChanging(_ frame: CGRect) {
        let localKeyboardFrame = view.convert(frame, from: nil)
        let bottomInset = view.bounds.height - localKeyboardFrame.minY
        let scrollViewOffset = view.bounds.height - rootView.contentView.frame.maxY

        var contentInsets = rootView.contentView.scrollView.contentInset
        contentInsets.bottom = max(0.0, bottomInset - scrollViewOffset)
        rootView.contentView.scrollView.contentInset = contentInsets

        if contentInsets.bottom > 0.0,
           let firstResponderView = rootView.inputViews?.first(where: { $0.isFirstResponder }) {
            let fieldFrame = rootView.contentView.scrollView.convert(
                firstResponderView.frame,
                from: firstResponderView.superview
            )

            rootView.contentView.scrollView.scrollRectToVisible(fieldFrame, animated: true)
        }
    }
}

extension FormInputViewController: FormInputViewProtocol {
    func didReceiveInput(viewModels: [InputViewModelProtocol]) {
        self.inputViewModels = viewModels
        rootView.bind(viewModels: viewModels)

        setupInputViews()
    }

    func didReceiveError(at index: Int) {
        if let inputViews = rootView.inputViews, index >= 0, index < inputViews.count {
            ShakeAnimator().animate(view: inputViews[index], completionBlock: nil)
        }
    }
}

extension FormInputViewController: AnimatedTextFieldDelegate {
    func animatedTextField(
        _ textField: AnimatedTextField,
        shouldChangeCharactersIn range: NSRange,
        replacementString string: String
    ) -> Bool {
        guard let index = rootView.inputViews?.firstIndex(of: textField) else {
            return true
        }

        let shouldApply = inputViewModels[index].inputHandler.didReceiveReplacement(string, for: range)

        if !shouldApply, textField.text != inputViewModels[index].inputHandler.value {
            textField.text = inputViewModels[index].inputHandler.value
        }

        return shouldApply
    }

    func animatedTextFieldShouldReturn(_ textField: AnimatedTextField) -> Bool {
        guard let inputViews = rootView.inputViews, let index = inputViews.firstIndex(of: textField) else {
            return true
        }

        if index == inputViews.count - 1 {
            textField.resignFirstResponder()
        } else {
            inputViews[index + 1].becomeFirstResponder()
        }

        return false
    }
}
