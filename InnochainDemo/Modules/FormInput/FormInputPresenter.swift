import Foundation
import SoraFoundation

final class FormInputPresenter {
    weak var view: FormInputViewProtocol?
    let wireframe: FormInputWireframeProtocol
    let context: Any?

    let inputViewModels: [InputViewModelProtocol]

    weak var delegate: FormInputDelegate?

    init(
        wireframe: FormInputWireframeProtocol,
        inputViewModels: [InputViewModelProtocol],
        delegate: FormInputDelegate,
        context: Any?
    ) {
        self.wireframe = wireframe
        self.inputViewModels = inputViewModels
        self.delegate = delegate
        self.context = context
    }
}

extension FormInputPresenter: FormInputPresenterProtocol {
    func setup() {
        view?.didReceiveInput(viewModels: inputViewModels)
    }

    func submit() {
        if let errorIndex = inputViewModels.firstIndex(where: { !$0.inputHandler.completed }) {
            view?.didReceiveError(at: errorIndex)
            return
        }

        delegate?.didReceive(inputViewModels: inputViewModels, context: context)
        wireframe.complete(on: view)
    }

    func cancel() {
        wireframe.complete(on: view)
    }
}
