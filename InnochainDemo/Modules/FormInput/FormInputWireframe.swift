import Foundation

final class FormInputWireframe: FormInputWireframeProtocol {
    func complete(on view: FormInputViewProtocol?) {
        view?.controller.dismiss(animated: true, completion: nil)
    }
}
