import UIKit
import SoraUI

final class FearlessLoadingViewFactory: LoadingViewFactoryProtocol {
    static func createLoadingView() -> LoadingView {
        let icon = R.image.iconLoadingIndicator()?.tinted(with: UIColor.white)
        let loadingView = LoadingView(
            frame: UIScreen.main.bounds,
            indicatorImage: icon ?? UIImage()
        )
        loadingView.backgroundColor = UIColor.black.withAlphaComponent(0.5)
        loadingView.contentBackgroundColor = UIColor.clear
        loadingView.contentSize = CGSize(width: 120.0, height: 120.0)
        loadingView.animationDuration = 1.0
        return loadingView
    }
}
